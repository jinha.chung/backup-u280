/**
* Copyright (C) 2019-2021 Xilinx, Inc
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/

#include <ap_int.h>

typedef ap_int<64> intV_t;
typedef ap_int<512> floatV_t; // 512bit = 64B = 16 dim (minimum)

// source from:
// https://github.com/Xilinx/Vitis_Accel_Examples/blob/b41830f3dfc2ad85020c0cf3bdb7d27d774b90fc/host/hbm_bandwidth_pseudo_random/src/krnl_vaddmul.cpp
unsigned int rand(unsigned int seed, int load) {
    static ap_uint<32> lfsr;
    if (load == 1) {
        // init
        lfsr = seed;
    }
    bool b_32 = lfsr.get_bit(32 - 32);
    bool b_22 = lfsr.get_bit(32 - 22);
    bool b_2 = lfsr.get_bit(32 - 2);
    bool b_1 = lfsr.get_bit(32 - 1);
    bool new_bit = b_32 ^ b_22 ^ b_2 ^ b_1;
    lfsr = lfsr >> 1;
    lfsr.set_bit(31, new_bit);

    return lfsr.to_uint();
}


/*
// version 1, with no loop-integration
// here, the size of perm should be a large enough constant value
// R = (rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)
extern "C" {
void bandwidth(intV_t *in_col, intV_t *out_c, int num_neighbor, int rc, int R) {
sample_node:
    int x;
    intV_t perm[1024];
    if (rc <= num_neighbor) {
        for (x = 0; x < rc; ++x) {
#pragma HLS PIPELINE II = 1
            perm[x] = x;
        }
    }
    else {
        // we are NOT using the Robert Floyd in-place smapling algorithm for FPGA
        // this allows for duplicates, but it shouldn't be common
        for (x = 0; x < num_neighbor; ++x) {
//#pragma HLS PIPELINE II = 1
            // x + 1 so that if x == 0, @load in rand() will be 1 to set to seed
            perm[x] = rand(16807 + rc + R, x + 1) % rc;
        }
    }
gather_node:
    if (rc <= num_neighbor) {
        for (x = 0; x < rc; ++x) {
#pragma HLS PIPELINE II = 1
            out_c[x] = in_col[perm[x] + R];
        }
    }
    else {
        for (x = 0; x < num_neighbor; ++x) {
#pragma HLS PIPELINE II = 1
            out_c[x] = in_col[perm[x] + R];
        }
    }
}
}
*/

/*
// version 2, with loop-integration
// here, the size of perm should be a large enough constant value
// R = (rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)
extern "C" {
void bandwidth(intV_t *in_col, intV_t *out_c, int num_neighbor, int rc, int R, int seed) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = num_neighbor bundle = control
#pragma HLS INTERFACE s_axilite port = rc bundle = control
#pragma HLS INTERFACE s_axilite port = R bundle = control
#pragma HLS INTERFACE s_axilite port = seed bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_and_sample_node:
    int x;
    if (rc <= num_neighbor) {
        for (x = 0; x < rc; ++x) {
#pragma HLS PIPELINE II = 1
            out_c[x] = in_col[x + R];
        }
    }
    else {
        // we are NOT using the Robert Floyd in-place smapling algorithm for FPGA
        // this allows for duplicates, but it shouldn't be common
        for (x = 0; x < num_neighbor; ++x) {
//#pragma HLS PIPELINE II = 1
            // x + 1 so that if x == 0, @load in rand() will be 1 to set to seed
            out_c[x] = in_col[(rand(3055317720 + seed, x + 1) % rc) + R];
        }
    }
}
}
*/

/*
// version 3, with kernel call in bulks
extern "C" {
void bandwidth(intV_t *in_col, intV_t *in_rc_buffer, intV_t *in_data_offset_buffer, intV_t *out_c, int num_bulk, int num_neighbor, int seed) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_rc_buffer offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_data_offset_buffer offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_rc_buffer bundle = control
#pragma HLS INTERFACE s_axilite port = in_data_offset_buffer bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = num_bulk bundle = control
#pragma HLS INTERFACE s_axilite port = num_neighbor bundle = control
#pragma HLS INTERFACE s_axilite port = seed bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_and_sample_node:
    for (int bulk_id = 0; bulk_id < num_bulk; ++bulk_id) {
#pragma HLS PIPELINE II = 1
        int x;
        if (in_rc_buffer[bulk_id] <= num_neighbor) {
            for (x = 0; x < in_rc_buffer[bulk_id]; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[x + in_data_offset_buffer[bulk_id]];
            }
        }
        else {
            for (x = 0; x < num_neighbor; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[(rand(3055317720 + seed, x + 1) % in_rc_buffer[bulk_id]) + in_data_offset_buffer[bulk_id]];
            }
        }
    }
}
}
*/

/*
// version 4, with kernel call in bulks, variable names redefined
extern "C" {
void bandwidth(intV_t *in_col, intV_t *in_rc, intV_t *in_data_offset, intV_t *out_c, int num_bulk, int num_neighbor, int seed) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_rc offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_data_offset offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_rc bundle = control
#pragma HLS INTERFACE s_axilite port = in_data_offset bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = num_bulk bundle = control
#pragma HLS INTERFACE s_axilite port = num_neighbor bundle = control
#pragma HLS INTERFACE s_axilite port = seed bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_and_sample_node:
    for (int bulk_id = 0; bulk_id < num_bulk; ++bulk_id) {
#pragma HLS PIPELINE II = 1
        int x;
        if (in_rc[bulk_id] <= num_neighbor) {
            for (x = 0; x < in_rc[bulk_id]; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[x + in_data_offset[bulk_id]];
            }
        }
        else {
            for (x = 0; x < num_neighbor; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[(rand(3055317720 + seed, x + 1) % in_rc[bulk_id]) + in_data_offset[bulk_id]];
            }
        }
    }
}
}
*/

/*
// version 5, with kernel call in bulks, in an attempt to change the pipelining part
extern "C" {
void bandwidth(intV_t *in_col, intV_t *in_rc, intV_t *in_data_offset, intV_t *out_c, int num_bulk, int num_neighbor, int seed) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_rc offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_data_offset offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_rc bundle = control
#pragma HLS INTERFACE s_axilite port = in_data_offset bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = num_bulk bundle = control
#pragma HLS INTERFACE s_axilite port = num_neighbor bundle = control
#pragma HLS INTERFACE s_axilite port = seed bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_and_sample_node:
    for (int bulk_id = 0; bulk_id < num_bulk; ++bulk_id) {
#pragma HLS PIPELINE
        int x;
        if (in_rc[bulk_id] <= num_neighbor) {
            for (x = 0; x < in_rc[bulk_id]; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[x + in_data_offset[bulk_id]];
            }
        }
        else {
            for (x = 0; x < num_neighbor; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[(rand(3055317720 + seed, x + 1) % in_rc[bulk_id]) + in_data_offset[bulk_id]];
            }
        }
    }
}
}
*/

/*
// version 6, with kernel call in bulks, without using the pseudorandom rand()
extern "C" {
void bandwidth(intV_t *in_col, intV_t *in_rc, intV_t *in_data_offset, intV_t *out_c, int num_bulk, int num_neighbor, int seed) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_rc offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_data_offset offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_rc bundle = control
#pragma HLS INTERFACE s_axilite port = in_data_offset bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = num_bulk bundle = control
#pragma HLS INTERFACE s_axilite port = num_neighbor bundle = control
#pragma HLS INTERFACE s_axilite port = seed bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_and_sample_node:
    for (int bulk_id = 0; bulk_id < num_bulk; ++bulk_id) {
#pragma HLS PIPELINE
        int x;
        if (in_rc[bulk_id] <= num_neighbor) {
            for (x = 0; x < in_rc[bulk_id]; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[x + in_data_offset[bulk_id]];
            }
        }
        else {
            for (x = 0; x < num_neighbor; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[0 + in_data_offset[bulk_id]];
            }
        }
    }
}
}
*/


// version 7, with kernel call in bulks, without pipelining (for rand() correctness checking)
extern "C" {
void bandwidth(intV_t *in_col, intV_t *in_rc, intV_t *in_data_offset, intV_t *out_c, int num_bulk, int num_neighbor, int seed) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_rc offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_data_offset offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_rc bundle = control
#pragma HLS INTERFACE s_axilite port = in_data_offset bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = num_bulk bundle = control
#pragma HLS INTERFACE s_axilite port = num_neighbor bundle = control
#pragma HLS INTERFACE s_axilite port = seed bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_and_sample_node:
    for (int bulk_id = 0; bulk_id < num_bulk; ++bulk_id) {
        int x;
        if (in_rc[bulk_id] <= num_neighbor) {
            for (x = 0; x < in_rc[bulk_id]; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[x + in_data_offset[bulk_id]];
            }
        }
        else {
            for (x = 0; x < num_neighbor; ++x) {
                out_c[bulk_id * num_neighbor + x] = in_col[(rand(3055317720 + seed, x + 1) % in_rc[bulk_id]) + in_data_offset[bulk_id]];
            }
        }
    }
}
}
