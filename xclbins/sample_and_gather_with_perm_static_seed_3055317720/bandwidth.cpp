/**
* Copyright (C) 2019-2021 Xilinx, Inc
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/

#include <ap_int.h>

typedef ap_int<64> intV_t;
typedef ap_int<512> floatV_t; // 512bit = 64B = 16 dim (minimum)

/* old kernel
extern "C" {
    // vector Addition Kernel Implementation using dataflow
    // needs to do: c = ((int64_t*)col_buffer)[p];
void bandwidth(intV_t *in_col, intV_t *in_perm, intV_t *out_c, int idx_size) {
//void bandwidth((unsigned)int64_t *in_col, (unsigned)int64_t *in_perm, (unsigned)int64_t *out_c, int idx_size) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_perm offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_perm bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = idx_size bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_node:
  for (int i = 0; i < idx_size; ++i) {
#pragma HLS PIPELINE II = 1
    out_c[i] = in_col[in_perm[i]];
  }
}
}
*/

// source from:
// https://github.com/Xilinx/Vitis_Accel_Examples/blob/b41830f3dfc2ad85020c0cf3bdb7d27d774b90fc/host/hbm_bandwidth_pseudo_random/src/krnl_vaddmul.cpp
unsigned int rand(unsigned int seed, int load) {
    static ap_uint<32> lfsr;
    if (load == 1) {
        // init
        lfsr = seed;
    }
    bool b_32 = lfsr.get_bit(32 - 32);
    bool b_22 = lfsr.get_bit(32 - 22);
    bool b_2 = lfsr.get_bit(32 - 2);
    bool b_1 = lfsr.get_bit(32 - 1);
    bool new_bit = b_32 ^ b_22 ^ b_2 ^ b_1;
    lfsr = lfsr >> 1;
    lfsr.set_bit(31, new_bit);

    return lfsr.to_uint();
}


/*
// version 1, with no loop-integration
// here, the size of perm should be a large enough constant value
// R = (rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)
extern "C" {
void bandwidth(intV_t *in_col, intV_t *out_c, int num_neighbor, int rc, int R) {
sample_node:
    int x;
    intV_t perm[1024];
    if (rc <= num_neighbor) {
        for (x = 0; x < rc; ++x) {
#pragma HLS PIPELINE II = 1
            perm[x] = x;
        }
    }
    else {
        // we are NOT using the Robert Floyd in-place smapling algorithm for FPGA
        // this allows for duplicates, but it shouldn't be common
        for (x = 0; x < num_neighbor; ++x) {
//#pragma HLS PIPELINE II = 1
            // x + 1 so that if x == 0, @load in rand() will be 1 to set to seed
            perm[x] = rand(16807 + rc + R, x + 1) % rc;
        }
    }
gather_node:
    if (rc <= num_neighbor) {
        for (x = 0; x < rc; ++x) {
#pragma HLS PIPELINE II = 1
            out_c[x] = in_col[perm[x] + R];
        }
    }
    else {
        for (x = 0; x < num_neighbor; ++x) {
#pragma HLS PIPELINE II = 1
            out_c[x] = in_col[perm[x] + R];
        }
    }
}
}
*/

// version 2, with loop-integration
// here, the size of perm should be a large enough constant value
// R = (rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)
extern "C" {
void bandwidth(intV_t *in_col, intV_t *out_perm, intV_t *out_c, int num_neighbor, int rc, int R) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_perm offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = out_perm bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = num_neighbor bundle = control
#pragma HLS INTERFACE s_axilite port = rc bundle = control
#pragma HLS INTERFACE s_axilite port = R bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_and_sample_node:
    int x;
    //intV_t perm[1024];
    if (rc <= num_neighbor) {
        for (x = 0; x < rc; ++x) {
#pragma HLS PIPELINE II = 1
            //perm[x] = x;
            out_perm[x] = x + R;
            out_c[x] = in_col[x + R];
        }
    }
    else {
        // we are NOT using the Robert Floyd in-place smapling algorithm for FPGA
        // this allows for duplicates, but it shouldn't be common
        for (x = 0; x < num_neighbor; ++x) {
//#pragma HLS PIPELINE II = 1
            // x + 1 so that if x == 0, @load in rand() will be 1 to set to seed
            //perm[x] = rand(16807 + rc + R, x + 1) % rc;
            out_perm[x] = (rand(3055317720 + rc + R, x + 1) % rc) + R;
            //out_c[x] = in_col[(rand(16807 + rc + R, x + 1) % rc) + R];
            out_c[x] = in_col[out_perm[x]];
        }
    }
}
}

