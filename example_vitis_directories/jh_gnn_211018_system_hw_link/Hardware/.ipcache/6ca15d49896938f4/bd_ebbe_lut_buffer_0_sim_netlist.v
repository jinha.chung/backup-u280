// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.1 (lin64) Build 3247384 Thu Jun 10 19:36:07 MDT 2021
// Date        : Wed Oct 20 10:13:50 2021
// Host        : FPGA2-18 running 64-bit Ubuntu 18.04 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ bd_ebbe_lut_buffer_0_sim_netlist.v
// Design      : bd_ebbe_lut_buffer_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcu280-fsvh2892-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "bd_ebbe_lut_buffer_0,lut_buffer_v2_0_0_lut_buffer,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "lut_buffer_v2_0_0_lut_buffer,Vivado 2021.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (tdi_i,
    tms_i,
    tck_i,
    drck_i,
    sel_i,
    shift_i,
    update_i,
    capture_i,
    runtest_i,
    reset_i,
    bscanid_en_i,
    tdo_o,
    tdi_o,
    tms_o,
    tck_o,
    drck_o,
    sel_o,
    shift_o,
    update_o,
    capture_o,
    runtest_o,
    reset_o,
    bscanid_en_o,
    tdo_i);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TDI" *) input tdi_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TMS" *) input tms_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TCK" *) input tck_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan DRCK" *) input drck_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan SEL" *) input sel_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan SHIFT" *) input shift_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan UPDATE" *) input update_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan CAPTURE" *) input capture_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan RUNTEST" *) input runtest_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan RESET" *) input reset_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan BSCANID_EN" *) input bscanid_en_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TDO" *) output tdo_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TDI" *) output tdi_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TMS" *) output tms_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TCK" *) output tck_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan DRCK" *) output drck_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan SEL" *) output sel_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan SHIFT" *) output shift_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan UPDATE" *) output update_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan CAPTURE" *) output capture_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan RUNTEST" *) output runtest_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan RESET" *) output reset_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan BSCANID_EN" *) output bscanid_en_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TDO" *) input tdo_i;

  wire bscanid_en_i;
  wire bscanid_en_o;
  wire capture_i;
  wire capture_o;
  wire drck_i;
  wire drck_o;
  wire reset_i;
  wire reset_o;
  wire runtest_i;
  wire runtest_o;
  wire sel_i;
  wire sel_o;
  wire shift_i;
  wire shift_o;
  wire tck_i;
  wire tck_o;
  wire tdi_i;
  wire tdi_o;
  wire tdo_i;
  wire tdo_o;
  wire tms_i;
  wire tms_o;
  wire update_i;
  wire update_o;
  wire [31:0]NLW_inst_bscanid_o_UNCONNECTED;

  (* C_EN_BSCANID_VEC = "0" *) 
  (* DONT_TOUCH *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lut_buffer_v2_0_0_lut_buffer inst
       (.bscanid_en_i(bscanid_en_i),
        .bscanid_en_o(bscanid_en_o),
        .bscanid_i({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .bscanid_o(NLW_inst_bscanid_o_UNCONNECTED[31:0]),
        .capture_i(capture_i),
        .capture_o(capture_o),
        .drck_i(drck_i),
        .drck_o(drck_o),
        .reset_i(reset_i),
        .reset_o(reset_o),
        .runtest_i(runtest_i),
        .runtest_o(runtest_o),
        .sel_i(sel_i),
        .sel_o(sel_o),
        .shift_i(shift_i),
        .shift_o(shift_o),
        .tck_i(tck_i),
        .tck_o(tck_o),
        .tdi_i(tdi_i),
        .tdi_o(tdi_o),
        .tdo_i(tdo_i),
        .tdo_o(tdo_o),
        .tms_i(tms_i),
        .tms_o(tms_o),
        .update_i(update_i),
        .update_o(update_o));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
bBTLKNRCXWhOAx1+YMj4Xx/Jr4A/ssh77gsESoWMEwxUHKGn57FvXJP9nsHr13plL6UGvrcIELJE
/ww/BCXU+VyrblusNZ/2CXIXW+XX2yuz6acXk0wWG7AwULz5IHKXwbV08ymDvQRCCUj0tn3C0Vt0
Y/OZIJxb+/1lRFjRgBc=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KQkJRK64UH/2tN0ZhuVJQ2ppl5WuTRzUAGyzMePOe4p2tJxRMqY6a9ifZXRB91bG0Q/l56ak6+x2
0/xi5LRq2YztvoZfw8XE8XReQpAtBUN9F5P4HcTGG8NsxuJtDQR0VaOuaTViizAucEqIjhdas3OS
Mucq4tSWQ9lhG+heMF4Pv0l/LVbloP3qTfUh6KuG0nXeJzh1Q1Hw6aEhywj+1etgBMhfXc0JIa+3
UkV7I5qsVNYopXhcC6Mm5U4baKeK3HspCYF4KiTakmWQ6kOnpMIUiVVSd3mfOZylYqUh3CMaSqx7
fdQ6ZWME7T2tRnbjgdZgAFcyMMWZhiEi5cTIKw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Y3b+GG3CqfotOb5bwZWphegg+kNXDYHW1UUrUr3gK3Yrh3Gv9F2kDlRT/YfAMgruk/1PSQjZUado
WbPUUFCkNR02++pNfDkKtI9tkjJOuQ/wxT5acGqqWUCrTEZt5jmWxhWFpzm6ZDnKaRZzcngpnNT3
x1Wd0bVJA+my70JOVHM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TqLpjfOnPLEi2zB+g1VqYC6ZrKLLpmng/mLDRhpKb/WR81gwTPjTiiuVTcX9sqLNtBeqtH/oOmIl
VCu1YkmmtbWkMGvseencNSpX7UCmrzTs/aPnHkfRYiIaiOPhlugsqjmDiTKgA2Tq/tT5PjXq/zW+
XUFrq3xJ4Rhtz1HgrAFai+X08+ewo2Qt3CRHJjV6dlyF9nMMROE1fjTfCcWVpo/78oEGtX5Bjk1D
MrB0tivvgVSYxdKLcpJVgj6PLBAITw+62Fm3SMXUTLFdxC43ZMyszgtK6sEu7aZmfVM2w1BdVc6o
FaYItBtJ6Pc68XF/TZkulB555IBq7C5sGCpBog==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SOvC56WAvslkDg8n0L6SYQdMmY9MTJeJdttZ0jlTtj+O8yqTMbotqztuypMWMmWLavEcgOOV7gEw
J4o8h8Ue3caRdIm21sbI8c+3GnfaXvk1lsaoM7f8U0T/Umubono0IetBO0J1lgs++rmf+p0gDJNZ
kT0atXyPLLLLIqTjHTvKhiAzd/S1VSq0ZYAQ8iM7BhA7EfDwLhAaKpcWlo7fqx79QdkJwrAkHZDi
UK57tdApeh7KUsS6YfVMszwLzMiLa075FthbuSkcS/F946cb1MWJyWl4CG2+jJS3YtrsahNsN1k6
tmHHWSKPlRdih7HOePQeVlmQpDxev57XOFOBlQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Tiw8f73kpeAklLTOD0oMGVhruDKflKu5jKJMhGzWQqBPBMzZHur2DxyYsQeBfsUU2kE+Ron6WE5V
+n5E4edfcSVPRIDodFRbT8RIf6E5Jnz9yed75HAm3ROYlIYdAVpCGpVkzuGLVHBSNmpgMmqghk6q
FsYu9vRDTHf966FJhBlvqa9PYtdzJVJCdkJxikwYdkDn6uMKwO3Ki7GSr5V5LnPd4IGr3ypEoMNV
ZQlpnbfpmpCJAHlATHUNDBaTz1Uj7EdxGbok3xKGIEU83ohI2Anx0uowQglOkCQpCkNB506CdwQZ
i7pA1HbIGUaD12Sjd4HLNEt9E354+jAbE2anHw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IKg4Ui6aztSoWjSvG54rTKBtyz55ZlHwZ/Peyb7r0ywMcoqiFpR5T2wweCE67b3SQG4Y5T+xGaIn
A80CE08e9uoyGnJTaucfR8URF2Zspp+fVAM+MVC9k7Am8cKnQCj7tDVli2/+AfluaNXP3xMGF6gp
l0j1NSppOvrLjJOPUojm+p5Dtosd6NVHG9yKKmLDMgs4SJyQCnH8WpDZTbetSStccve0Wh+8BPjX
kn24RtUcfWpdMc3/p7sFRAS/xZ9yqr3PamZjkQLbtqiAdGuufgDu7QXkadWIBf+qMutx3PaF9PnQ
Kwla72sRF9OL5CIvalqMWy3UHXCoIU8O+PNFSA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
NH8E2j5Z9kVzx30F7yEjdpqHZQZfyinmIkLBE0sElyXKo7RkW7O2w3ANqxMN8dDJjDvo+Afyn09W
FtKBXG1hQ7IJjMSIVyrhqHzVNTSx7/HQjfn5xwWzK1B3NTo9zynU5CicBoNICCUUA085VteOIFSD
lX2NhqMnyMvcq+ZGW1Pl9GC1xLXcDdqRuOwol93J2KcfyKs3L1+Ces2K54MnLHSmh/wH7oz0+irc
edD4XZubqP3RoNejV7n4JZfr283agIrxralDLNpfiAZAEZNzeHX2YnFwXNRZ8++5IUwJsj57Bxjv
S3De0VfC3v1o/AHkmMwkZrlhvccAiolopdOyH6e3uCkocnoNPOPVPltgSXIqd//u1IYkxqlWVJBx
QOeZU2q4MaUZj8iP9KOE9QFDHelN5efjw0ecidRfLTyxcoEeT24Z1pHWSTHPfu6BmjUia/eUWJ1S
mNMuwQjSuOwuhY1oUMt022dENh40auRUKOsKpoct7toIpl0CJ/okPmI3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CcHFq/bSOCYX/lphfljbwbptit8aJqo4TfpjkNOMqLa8QH6TqEITgYKyih5HbuAs2zCLNkbLClLt
GcQFw/GJ6L9TvstcUfWXNWgsrZiIWPJh4t+SWR6DAr07Js3NqwU35oA+bggBNB6xWUysztACo6cB
/S9O5c0bo+WojPtcFcLs0zU+mLwZFflmBJaGSYKiNYmK1awlI2sl9DicuLw8G5Bkt+CXMLSyMGe3
O/QtauigzQ4hSFn+HJ5EJ6MR5MeGdXQESBs9Z6+de3V+ezfUQ/VaBudlFlU3gvC6qS2Pjrp/1Xwp
EDzasAhIIVJwfXKzJQMuhX3RcINDzx0ePJj3LA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2960)
`pragma protect data_block
jqUhUUc3YOPyLafsDyOQuUFdi182buUy4rXjPQW4e6Hc9RvumDIYDeS5EoyqQx9kIqNRKiLWhz6J
HsgZJeWVvclSgfOi+VoCKNYaLzNUZkKPsgceQi53pVHMSCNLSVxCrRUrgjbtiNBHTsK4NGz77tEa
+Y8gv3fVPzOhNUZjSAaplJ68UXNmTa2YJDLQbH9PyC2z1mRk07jbladudkW5HdwLB7DnlHKk+veE
HfV59DXJS2pUGXx8jd1HinzZ4rItvjVfqfMdPI2Fg4zmwIAuSpi9QWl6xQut1TyLCIsBuboKRVT6
h3HeOtZMnK1496zJXPE5ksXcqXiLIbIHQ/aJ6C+x3iDwd15d6s6qGFywrmacbifHAz7gbEbfHw5J
/ibAOWXRKhYM7k4u5PBBMBp3XjxFMbd9ZYREE9+ClqVBTooEGmNmQqeTE4CAB3xAv6C5e0/IPGnB
5jE5j4SDtZgQlGHlzjelci0hqDNjvcdOSUfXk86kHq/RVDynLXQXbZLKqEeNbf/U+m2FE43G5J1p
IkR9fM0Ah2k/qJYEhP1WNq9jlKaduogd1fmpSaCXj4qBBGyCbzAqUsz2vG3KLTkHcD/lGyIYLJWZ
0ElDUQOhw/Mn7RDGDxzI2g22dQSArALUdp9AIiQ0NVz/m08Cji81JVG3plCCIKbXatzyEHoPcG8c
Dohjm97R9X5JUf4vETlSn6UIt11VUIWhhH+sU5Q2Iss34lkWEEWxhhZ7TlU52Q3bhvWS5EeSuVIF
g9C9C6SLZ8f9FdiZGW8qQJGGUO2x31mRiPX/r2SwTWSM02DL5uyFUCQ0MTLuKFRkeRY9RCieknRb
IhJueUvqoIaEz7eApQZjMVnlvnoC6aMEjs3xVRW44J6OsO9D33KwBJJdLVeG2oxXBbHgxy/e0Jjp
FN8S98yPA/6hTWvlYZpJy6eVU5Q71NMFR1iz9yXux31jy/o3PdasqajzzveQibsEmRAyit8pUt4a
rcyBrkmXN95LgRT/99Ov9xO9Nq4lU/c/rfMXQD8acfJOGvX5PcpJjicJG5nIN6DlZY/ulkczpf+l
evAwsWJwe4IYv9C8JzZKTzgzhwBVhYyrOOS907NV57UWMFkl4f+8P8KTnfYrxRKlftRMd3FrvFTY
/ZEJ2jDz7H+t/fD5YNqYhEZl7fQWi5K8HApBe9Lug+q2B0PoqMkp0g1e82Vbm+vCQJBXm3TO5VlP
8h4pMEOgJn8l5EuweZ+DdM1L6TolBQ18dIUGZ+LB5aTeS40ddQW7XKKEfHkuzXn6Jn74xIhgGuu+
rwQCyDLk/lM35+x7lwZ37FlEnuBg2j/VBlMHJXI+Sp4PLrJ0oOBk18tRneSwdqrYBYwIVOSJYZK0
QqyXBWxZrYy+/hSZGmsL9yk08nYUiO1ig1JJf4EH/lu97VKHlrPfEpjd2qbwXFIu91f6QtQSRqJw
YsRsLVdQtp5bIWWz7hdvH7Ut/CJKkOBETQ7hrAc1LzllTn8ik00nkonwHJc/xZUF1BIJcJZubS5q
8c4+0fbOkjoi/xGRWnbgdn6pmDUq6vzpR4aTL9IG73l2s0p6M+cjcwC9MGPKxle0CvGoUPa8MX/v
mR5FEAyz2AREK1rDUlg/FW9enWDIlCsDyakiPJtCu9hw7ND/LgUbz+JUCgEhy9kFn0Srrs7mOtIB
whI0llMgwQB47WV6S8XHX9lvNQ/f5+UgRUq8t0TM+WhbEPNRNDBOwCYCzMl64wUcAXGqArmvGUZo
jgE4O003L+YXYkouRQEbzm6V7Udpw5XUvbhlhASyyfzdOwyA7GaNOx3Rx719MBv2ZeMgbxfAPpm0
yE9RAFYHYn2L25y/4fE1284y/Z5oY6CXBhtLEaW0hAsFrFZAqYjrNNs6D+t+249S568C7mRsE0lM
2sYO6XjQicOgkIJZPY11jMmCI8pNmwLnXc9V9WDBYTeSreunh764NTi9vhPIAZLvZpq4KhkFChX0
99aMRIj87OdeqqqeS7NHnwHxB4UdU4d72XFHwsnjWHjZxcv34vJCrEbS5u79QIhtuHbeIRrm8sKh
9mC8qSGTSJ9AB57LjfbuVT5n8qoB6eyo/bgJaa5hpb6pyATQnVKXoYg0a+tkR+WHNM00cMmZBYr5
MeUuY4lr+wAI1CdENm1mF4X7vHOUk0yNjp4vBmLmFZAqFO2BbTBcXbSKXu8gq1Y7IDJD6x2DY0kx
FmnX5iQHuo2MpkoZbRauIULZfxN4OBuoY/Y3EgG9xAuf18tziKhsPcWtgrnEgcspmGhC2Zs8OcHi
10O9Ym3gF3oV7UCvW3jOQbqgdh9o6SGxTcEvns1JfkI3fkOLSY2j8aAkrrSJ7azw7x3x+AHK0mN8
JOmlY9j3m/p0d4JrZKs/cnlHJdOriqPGfn2ZJzv1FPQ7GofpAE3OJPPftzmqxQQ4tGS5oVtULYK7
tFFJ3YG8jOyjEuwE6X4BsHlRGti4t3WD8PONHTrHkjq1WyA+UJ1PB79Xt1qvijigGB47J7/4MhRO
1GgQWOixYsN1I7O8q/vIgs6y4GExRjMtSZ4R9p3eg4FFk5leUqOdZAw1fFa26FCdMuBJWsspW8nJ
cVbxw1EGu5LzBpImQTG85VgWCTa+bGyWWs8abMWBY58vE1xN9gFBOC8wHIladMagzKUDaBXPsluz
IoKMUZ8FCi+cO1/sOvnU1Xh4qaENiYha+HnE9TIjp+CI1+FlXI/k0WDDx2qkVYCHs+YTXQTNXICa
DzQq0k+IUbd830GtsffqMPub2vCxy6+c0sMpXYeSoxTnf9jLQvvKjWNpnqZbicqYUvDvd4m8r3pJ
TOopXPr/0wOsuUdajOWC5vVN/08SL8U9gW/cYMQlYLAT5BSZPRzaj9nm+WzBOiHFSMgkbQsDCPUW
Uebr1bpDXqQkESXUVIDAiSvM5jVepBjWWRZmBIL3nlGYCxUevgJ4qhAa/a2Oo5DqPct6t/SbFo+4
UhGzrgyJ/GHYKqs5kcUiZP1Xe7PDu+DayqD6iSmeX9fdPi5PvuMO4q3wnhe4XhNj9gCYXjOpghYK
g++K3GNaQMaEGzeQDjYFoIShpM/kWABt34yB45KGun8APJYNR0vAzYD8g1OW2VAYTxMez88bdLjU
wmvU34184Nfl1Nc2sY6fyTaFg+4dmCq7K9Tbe0A5+Kg3gMFwpZ7H88yiZ/T9P1s966LuLKSAyvXY
7PoXRye39hXaUNFRVStcJfrBVolBTNPhCbUnQ9GCIySL2WwnY5R7EsjBZGiac2LRYRuqJ1ZAQ//J
hJr9qiiLHSElLVcLIARYwQMmwrcV5IwfRJSwn9eg1ZQob4Kycd/HkEBYaBI8UsjUbcl+IDlhmX5m
+E+RrtARina3TtwcalglmzdijmMc8azImpLzh2f9Muzcq71zAIqtxNC1UaxCAVGnpkrEmSllgpb4
wzrqfam2Zgg+SalLZef9+u9Ote+6UG7g1gePXQ0/562Gnm8gZBGOCzFlcFP1rGvCrPjClwu5q05p
DVwXiX+U6K6EjQB53JOfZSPRO7Pbv4vQNOHmPycdHBT5e+DPqTWpAEkbK3VvPof5q0AV0kwMxLqo
79+Z6Yy9mz+2QElOWJ+zRHXdBY8NT8A+TyvkFa1ou49mPwixzE4OJpRqkgX8Q9Zh3G5qytpLFF8G
Z660ovxhUFa8CCUVQfDQBn4XOI+wUI/T2nvJrwX9j8UclpXPNlbWuQ4VAOT2NZL2VVO4MAyQgyJg
HOjCHJ96GNaS4WYCoNV+xZ6gGNBR6i4GamrfMLzPDYZ/MetPD4h6Fy5xpyB+Lb98NsEuOcYAz23+
/H8tpELW6DnyVLZNGe4ZTayeh8qheRl3rEpPie4nW81SeiglKZUcHpwKVRWB9DfZUqazw96ZNJPK
QMV4zhSlEc7iSL/V42OxyT5+SwKmldH51xCuXKXLbCd11vnDo8w8rChrSOG7fkVRqPTIhls=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
