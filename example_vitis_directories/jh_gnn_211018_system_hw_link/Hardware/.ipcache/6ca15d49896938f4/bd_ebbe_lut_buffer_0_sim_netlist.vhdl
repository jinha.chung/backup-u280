-- Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2021.1 (lin64) Build 3247384 Thu Jun 10 19:36:07 MDT 2021
-- Date        : Wed Oct 20 10:13:51 2021
-- Host        : FPGA2-18 running 64-bit Ubuntu 18.04 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ bd_ebbe_lut_buffer_0_sim_netlist.vhdl
-- Design      : bd_ebbe_lut_buffer_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcu280-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2021.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
bBTLKNRCXWhOAx1+YMj4Xx/Jr4A/ssh77gsESoWMEwxUHKGn57FvXJP9nsHr13plL6UGvrcIELJE
/ww/BCXU+VyrblusNZ/2CXIXW+XX2yuz6acXk0wWG7AwULz5IHKXwbV08ymDvQRCCUj0tn3C0Vt0
Y/OZIJxb+/1lRFjRgBc=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KQkJRK64UH/2tN0ZhuVJQ2ppl5WuTRzUAGyzMePOe4p2tJxRMqY6a9ifZXRB91bG0Q/l56ak6+x2
0/xi5LRq2YztvoZfw8XE8XReQpAtBUN9F5P4HcTGG8NsxuJtDQR0VaOuaTViizAucEqIjhdas3OS
Mucq4tSWQ9lhG+heMF4Pv0l/LVbloP3qTfUh6KuG0nXeJzh1Q1Hw6aEhywj+1etgBMhfXc0JIa+3
UkV7I5qsVNYopXhcC6Mm5U4baKeK3HspCYF4KiTakmWQ6kOnpMIUiVVSd3mfOZylYqUh3CMaSqx7
fdQ6ZWME7T2tRnbjgdZgAFcyMMWZhiEi5cTIKw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Y3b+GG3CqfotOb5bwZWphegg+kNXDYHW1UUrUr3gK3Yrh3Gv9F2kDlRT/YfAMgruk/1PSQjZUado
WbPUUFCkNR02++pNfDkKtI9tkjJOuQ/wxT5acGqqWUCrTEZt5jmWxhWFpzm6ZDnKaRZzcngpnNT3
x1Wd0bVJA+my70JOVHM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TqLpjfOnPLEi2zB+g1VqYC6ZrKLLpmng/mLDRhpKb/WR81gwTPjTiiuVTcX9sqLNtBeqtH/oOmIl
VCu1YkmmtbWkMGvseencNSpX7UCmrzTs/aPnHkfRYiIaiOPhlugsqjmDiTKgA2Tq/tT5PjXq/zW+
XUFrq3xJ4Rhtz1HgrAFai+X08+ewo2Qt3CRHJjV6dlyF9nMMROE1fjTfCcWVpo/78oEGtX5Bjk1D
MrB0tivvgVSYxdKLcpJVgj6PLBAITw+62Fm3SMXUTLFdxC43ZMyszgtK6sEu7aZmfVM2w1BdVc6o
FaYItBtJ6Pc68XF/TZkulB555IBq7C5sGCpBog==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SOvC56WAvslkDg8n0L6SYQdMmY9MTJeJdttZ0jlTtj+O8yqTMbotqztuypMWMmWLavEcgOOV7gEw
J4o8h8Ue3caRdIm21sbI8c+3GnfaXvk1lsaoM7f8U0T/Umubono0IetBO0J1lgs++rmf+p0gDJNZ
kT0atXyPLLLLIqTjHTvKhiAzd/S1VSq0ZYAQ8iM7BhA7EfDwLhAaKpcWlo7fqx79QdkJwrAkHZDi
UK57tdApeh7KUsS6YfVMszwLzMiLa075FthbuSkcS/F946cb1MWJyWl4CG2+jJS3YtrsahNsN1k6
tmHHWSKPlRdih7HOePQeVlmQpDxev57XOFOBlQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Tiw8f73kpeAklLTOD0oMGVhruDKflKu5jKJMhGzWQqBPBMzZHur2DxyYsQeBfsUU2kE+Ron6WE5V
+n5E4edfcSVPRIDodFRbT8RIf6E5Jnz9yed75HAm3ROYlIYdAVpCGpVkzuGLVHBSNmpgMmqghk6q
FsYu9vRDTHf966FJhBlvqa9PYtdzJVJCdkJxikwYdkDn6uMKwO3Ki7GSr5V5LnPd4IGr3ypEoMNV
ZQlpnbfpmpCJAHlATHUNDBaTz1Uj7EdxGbok3xKGIEU83ohI2Anx0uowQglOkCQpCkNB506CdwQZ
i7pA1HbIGUaD12Sjd4HLNEt9E354+jAbE2anHw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IKg4Ui6aztSoWjSvG54rTKBtyz55ZlHwZ/Peyb7r0ywMcoqiFpR5T2wweCE67b3SQG4Y5T+xGaIn
A80CE08e9uoyGnJTaucfR8URF2Zspp+fVAM+MVC9k7Am8cKnQCj7tDVli2/+AfluaNXP3xMGF6gp
l0j1NSppOvrLjJOPUojm+p5Dtosd6NVHG9yKKmLDMgs4SJyQCnH8WpDZTbetSStccve0Wh+8BPjX
kn24RtUcfWpdMc3/p7sFRAS/xZ9yqr3PamZjkQLbtqiAdGuufgDu7QXkadWIBf+qMutx3PaF9PnQ
Kwla72sRF9OL5CIvalqMWy3UHXCoIU8O+PNFSA==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
NH8E2j5Z9kVzx30F7yEjdpqHZQZfyinmIkLBE0sElyXKo7RkW7O2w3ANqxMN8dDJjDvo+Afyn09W
FtKBXG1hQ7IJjMSIVyrhqHzVNTSx7/HQjfn5xwWzK1B3NTo9zynU5CicBoNICCUUA085VteOIFSD
lX2NhqMnyMvcq+ZGW1Pl9GC1xLXcDdqRuOwol93J2KcfyKs3L1+Ces2K54MnLHSmh/wH7oz0+irc
edD4XZubqP3RoNejV7n4JZfr283agIrxralDLNpfiAZAEZNzeHX2YnFwXNRZ8++5IUwJsj57Bxjv
S3De0VfC3v1o/AHkmMwkZrlhvccAiolopdOyH6e3uCkocnoNPOPVPltgSXIqd//u1IYkxqlWVJBx
QOeZU2q4MaUZj8iP9KOE9QFDHelN5efjw0ecidRfLTyxcoEeT24Z1pHWSTHPfu6BmjUia/eUWJ1S
mNMuwQjSuOwuhY1oUMt022dENh40auRUKOsKpoct7toIpl0CJ/okPmI3

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CcHFq/bSOCYX/lphfljbwbptit8aJqo4TfpjkNOMqLa8QH6TqEITgYKyih5HbuAs2zCLNkbLClLt
GcQFw/GJ6L9TvstcUfWXNWgsrZiIWPJh4t+SWR6DAr07Js3NqwU35oA+bggBNB6xWUysztACo6cB
/S9O5c0bo+WojPtcFcLs0zU+mLwZFflmBJaGSYKiNYmK1awlI2sl9DicuLw8G5Bkt+CXMLSyMGe3
O/QtauigzQ4hSFn+HJ5EJ6MR5MeGdXQESBs9Z6+de3V+ezfUQ/VaBudlFlU3gvC6qS2Pjrp/1Xwp
EDzasAhIIVJwfXKzJQMuhX3RcINDzx0ePJj3LA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4224)
`protect data_block
QPDwu616nW2D7N5ew47+fbYNEyMFMIyu4i2M+CrcydRkgbz9teXANlp6JqUkDESTgcNLYA3sOvzo
eqAlkIDsCpAfZk0oXQCsZe+IkiY1YW2I3P52R7a76mJbhuIVitHFaQ32UcGtQWDJBjNEDG8ScNgn
LqAoj6PAi32sb+mYPu3iDCaq+Jkr8k/ZCc/ykVKFaMLQ0sDL87vfvbd1MKodO2wEt3Jmmvq+Bdxx
SDN3cVRRSnxAcSdw1t8uMC6KsMkvPxb9H9q2HgS8NH0FDjXMh5+WkZf41zCGf8oMEhndM1GV/1Sx
MkVi+h+Eezs3x49GR9EWlEk12IzZDOEpFxKvRR2SG/XS7HzV3fJGOSMwBHAcwRmBB6+VNDKU39vx
1d22MnPcv+U6gGdeeVb6IgtcZMO/ccYIdpAP3zhuy4fzgYdKzzqw3b09rI0WPwL5fi5K3hKPowKK
KgtAlEibfyFLEFCerO9Glxt5WkM2gIyGpKFr+mWCB4+CXPMATUeVWx8ltwQAh+02U3hHlxwUy+EW
0t5saZrPeJB5orwQxIC3lndi3o1MDQay48MQUemNaeRiN+2Td5ZTu7vVWzKMUgRXzSqsRbq+4pQh
M0lOwcA8sPCCDqOhyzPVLr6a2MXZ2rplxRkqvYkFc2DNZ2H6IfH0Lq+2xiwQpkMQV9/3301YzNmO
+ghj3b+5hfOEOrePbp8/gXXqkjYiNzqaoQ3XaKxP3dii/fRV5s2jkXLnQD+0k8JzxUMwJJ87pRmg
hD49AN0iql8cduT+3bsUzZUMQ/ZAmazj1oVpY7Ce0g/Ns2SKd51bL1LJFHSpyMc1vsCpHTikbckr
eMgj66rwBz8yDvVpVNbrYatK9yfoQle+5brYHvwsVI4PoD90gYkNDW1BuvCMmMgdItTKG0zTBtWG
2HcEkLumUQwBhIVYXwASb/K5C+9QO56buovxhyCGaQ1hvIflInGvTX+f1GMWStfQuXKTDQVFYis2
k7lwgcb54IRj0F8oO+SdTJAVHC1m3qKF5KgL7pvX/UTXa++hhQ8UmRvxqZhak0H0juWVEQibue6v
5J9YLI3pjcrdCASlVpjkZyKJATXN/d3Qdi2aCe8TZ8hBNF5Q71LUDApY3tno0HFw6nzS5m8ZzrHw
+Ns2mo0bdr801DCXv64kj4nWTxoWn7SbH3T5ar46E+5r48qNKiOv8tps6Nw/visEclsOK7/S2uAc
iUFUeGvKJUKFzv4uQBkx6F1s/vBBVS3fXVhxpa2TPGAVfv0tORRGP6l9ISOHtSs2dV8o/Dc/jMhr
aoTXdzadCmTVTPei7nYyQbFYqCVWbH8z+kTJOPeelERlUEKJpt3AhK0WAnh+eAofGQ4xI3XeiFuW
rJ13/AnXY8gYVwRpzNXum3h5L40xcnOfhq1nWYTcJ4qW+abm0mhhumSDZBhyiivuG02w2E77S7AM
SZt1ROcZEa2x7EbwGxPJyNn2vZJ6l23g6ypjW8AIba6Ys7WIUQVXgab1U+cKxOIpbzSKvTw7iXTr
VItRjKLk1bK//0k/Dmp4D0mFY8mnfqsLHrt85hswXqtemPvtPEjSal5drpi7NcBZooxGO91lIhy9
AjKVNvGmJyln9e1Nrf4cuJr7Sbwt33HYPh4f4MMuzkXVMzA3r7ksoV+FrSIRutdFGh83XnaepivE
SWcFZLJSHLVl5Rts+jPSeRyzf/O9TkT/jWcvBCrzzqQLRMGuL6FXFsKDOcYBdrJvsTle1v0oR6FD
1KoLqtAhiS3izUNeGmU2816fxLq8JxJmzvNqJoRaY7HH3RIqjzPSucZkMiRuLqDevmVsrWM37wyf
7PIIApQuYbRjgpUqXPk0G1J88vZl4o0b6ip4pJYDYDn6+TCDLyrYuFcBYhZUU49oncB+bt20kxq/
Qvb+eElMn3IQMpge0NGbP5MyPkuTMOUMY0C4WO2KcstaDJL1SXN4COIMDxYiPjJx/u+Wg6cXAh7a
bOtFm0GDiALHt0T7gv9t/B7wXA/nB9gHIePagoRsFb5NCWRD0h6RAeJSVT+46XxizKOGS5fba7Cz
QMjDwQD2fs66B8p8/AinPjwSXErcJU0Z4Bnsi7lxAqKyJTQZR4pWSSL0Yzwl3UuqTSGQo1+rb0Bv
LilrFaXs9b9UUi8VS8PsiN9sS1ufcZC/5P5ZKbvG2DAGUXWse0iNpwXlPNnyFo4T0paEYIoahXc2
wLCZe9saZb9fNrAKgq65qk9owVyyRwiPMOcKltlnOG8dizUGJJdkuZs+yxgQ5gDFWcrngrP4rj7W
MXy/Q50D/np2YCxZu9nuYw0NPxNAqZBx5kWTtVjBInmNohG6P/WezVbDauyZ7PhspyE2d1mZy4dk
Fun4MPmoXo9a5VZ+CyiU5hVMSCNWvtIa/i6dALtoGUeP/pOSG77nwYTTc63HZ+yLJPVdZNDZI83q
LD0xGK2BdYUunAF6eUriwiNlO45d6PvseLTDJsB7xy3X+urCC67TQekoPJYNfxSA+ojGB/amdgVN
ZFd8lKxzGS0T1DIOVUEgfy2wOcZDZ4GzKI29vHZGifvw67fa1U+5xPj6YAIezv/F77q9E8PWwIBr
OhvXqNTATMbOBT2B0GHrPlSG1EmaJYKpdLXHcC8E5MBmzzOrjdYRN8CAH5H4oTCAuPPGOHxdlW7L
eTcq4C6cvG9mRKwzjUfrOYKfZ3l57skof02Iq7UNKCYGaWwgkRhhhubQGacfzo5lQJ2awDZ2OJ3W
wurnJuL76V3FySCeRRF+XTy9jJ0edq5VGKweeAlASgR9XJfwMFLUkSVYjhWt/JSYwd3+YNNK/i2y
GCrtuCN9yGoaX9AdLaBlsw8t5fTmqD65cuQb7Eu0w12UY6DRGvYyywPYBPCZwqK6mKx1bAagzLln
g8ZADVGAFom1rVMGrnlCUn/w47xZtlr2Q/kXP6cIoms0sG5nCI843qAjy/9v7GCvekklGoxzHjPI
vNrot4Qa7YsMgeL4ci7hTT/4jUfKABvC9GPbNxuw1srmUSFCUC0dXmmzwgBJMK6PZW0PK34WvzgU
4eejcSMj9pP+ZYFV5aOwLKaTaX6cqN/PqrN0uQAzCA+gt/c02flCZxDaTTt4tbM2FeXcooWJVHx9
LYEcyUKDKtHrV4W1eZnl3V4krAY1D57XL+hFV9gZTakJGjElxOvlXamcHpx1r2XacH7CzDl8B1Gf
0arV7QsQSiaFdfvx2nPU5WquipvHBgsnqurwu9Tu+2nQS1hHaOuM1gMVQ6Qhu5SG7SoNCT1CNIGv
xEKtHiFrqwzR3sY1NyXQM5+RAPg+L3vJ2k1wFNEdkvmFQRaOevqRZss/Fk6shKmvDZ1U8RLwDzX/
4wFBRVpaovvucfR/QLqluX4ML1Qem5QKamcVzCOVQaSTeCAzcuX8PYbFW6FW+Yjzh3ivXVorzVwX
Hqu/1/l2leyvi5ePyCyqmGNVMrpU/uTAf+IY5bk62UgCYM/wS9fzuYSmd1m13P49eINWEuWq3aCm
zeQ8UlXGRcguu5l5/f5a69RDhLmZQrbKddCNkKZzU3mHUhMIJ1wXhuh04h1YbPQd24ylkrfX2fjN
jpJvG+Buxn0QlFLm+aEh4aLjUYerfiOxPyhLfX/wY60qekl3pQGzJmvXgKnpjmaySyk1eT00u84w
YJOIv2U/WHfGeUnND1XRNRsM9C0jUM2RfBzytCBaX8U4yNHwtmB2QG729qhOXbZFfx4NeFk0QDkW
8zlAbexRANeLUhHutccb6unQzoGjx5lnm7aEaOye9A9V0nf9XlAk+4Cv+XjDAMOLbaekDEm0DT1V
ZMQKE+TCq6kl62B0abx0KuxLxKrqDmFJw030C3twx9pWjFePfS4kgIhYrq+Jq5NZQ0OJwzqpguRJ
pMtAY2FgIbrbJtuwC/27IkawdaDFyYqIHtdtDQcPbeNbyYOoO3Y8NiXGKovcU1/eZ0rGBAm93POz
1APT7xUFjuJ0EdBY9pysjrCuQarOEMtg2hlcLmF1DgLRJC9Elg2bwDwgFGPDQfjK/Xac7OiK99wZ
04Gy225UzBtOJAFIBX9gSjZOCvlzG9H1bi0xIbXEel9uF4WbwgTNH3HBWKU/nKvbMPZvizbeUlzi
3XPQdVxxVfyZ8YRY9jvLb+MSho3fDA6bCn1zNBockBK8JhLogoxYNARI6RP6vyBLYX5iJEOGq4aH
iIgfzpqrOnPR24z6CwtKYfKzUtIRSvLAKQIRvIUAgcUOmv2MtReiiiQq5qewJ4M8Y0jS3bISRnkJ
30HKwPqY8khzKF1HJqQU8ngjNrgQPq1wkeg5Vd1dgc9A4n6pZqiX++ayN9PVXenQZ3FftqmDHAIe
MlQCNkxQRhZ7c5kyr2tnBYl4BwwyTXhW9v4P2F65Nc+f6/DQ0BwzFzaQ2r8wqwos8W4vEWhi8Rjg
y6Wm4Ex49cUtwYncMm0gAfK6B8sL8uSBBef4MMz4sGbnRAXgcHF0V+8KLUus0SNc46YhyS+QTkZC
tMUN/LKMKD69TAoYoA8EBZdIy7RtjIwB0uDgz0wW8U8iaBt6+eyLSCJhaC9dX2Ijt+cKoIqRgLrc
CK5PtXXakGDedsnM9mzyXimZ1WUCX3+KRf8DpubQu5PvZksYUzWzMbyF0NiUMz6GFz1TJilvm0Vi
lVtkdYc1ue2Ish+kZYb6RzI5lTe4UOpsDeXbl3G5Ssc6FHz07HWVqG43Nm1s5eUfvMUm7UfcJojH
PWFHXpBUI9eicrD8iks2Nya+A91hFw7XaZZyBodLB1fxG8+eTePQXChkR4UHCWiMX9olBHmhduxO
0UFAQcAwKHdJvjssM/EA/OEHiDWZMevis3aWR7N/DZ+kHyE2Bi/o5ABVd26ITgkubTn1W/4qDcat
ChdzsSzgJ/Zp/xSyBsft4TI/kfNzBe/ty6f9+f7iMbqdeLmNAGCq1Hmw0Ymedwd4cWe7H7Op9HOg
kOKb+I/QtHkNuIGkRMW5HtpmfecsR0BM4KsanhJy75udTYO0mXrnmMkKQJwQzbGduTsPsJW1jGMd
3hMrt2AUnk1zI4cTAF7rBQARWX6HqVHgamWkpZQvDQYFvSx+8DhAEs6e6tyPGUK5gOsXVs8hroW/
IzEPef6ieGNwZd07oQQqdqb7D9TYPI6e3ET+qdqGLQWjpNAsIJYlV81lxpNf1X9IXKUMNqTLrqIC
3Qrue0iP7A/a+6uBsiz0zao5cBxcEiiR9zq8Vb0xZv0D2lB3Oxny3rI1NH7rPSrmYazqDV0LaaKM
N7usM4HWkqZN13/z1mITBI0ueuho2omFRPI6YZfix4MbMGIIqboaNDFkxF2Z1zOQuvMjrWhkhr4Z
zzAzGLpg5gDVUpY2lB0cIY1p/lp9p5GenFfmo78Cv6ls8AHT4IH4miR3VKtYf3aDMNdNHtL/3av2
25mrkv/NsH6T6eUQMHjvgY24gTDWBI2zuYo7I0NiWNvs0o1daXDnWFGg4ccoLfmQ0TjPEM+VTMtu
MldgKfpNwRxGLLhxPndo//p+dgHStDhv/bg5N7jR2OR4zLSEV+Mqax9/oZgnivtbKCmqs0eH3B7m
VxewQXaiCPFJ3nS2Atfu+X2hciHx5L+2VmeUnjwQIIsjlqfFtrPDBsWzS2WJQDpaHdJ+WS/PO8FP
LHPGUWEw
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    tdi_i : in STD_LOGIC;
    tms_i : in STD_LOGIC;
    tck_i : in STD_LOGIC;
    drck_i : in STD_LOGIC;
    sel_i : in STD_LOGIC;
    shift_i : in STD_LOGIC;
    update_i : in STD_LOGIC;
    capture_i : in STD_LOGIC;
    runtest_i : in STD_LOGIC;
    reset_i : in STD_LOGIC;
    bscanid_en_i : in STD_LOGIC;
    tdo_o : out STD_LOGIC;
    tdi_o : out STD_LOGIC;
    tms_o : out STD_LOGIC;
    tck_o : out STD_LOGIC;
    drck_o : out STD_LOGIC;
    sel_o : out STD_LOGIC;
    shift_o : out STD_LOGIC;
    update_o : out STD_LOGIC;
    capture_o : out STD_LOGIC;
    runtest_o : out STD_LOGIC;
    reset_o : out STD_LOGIC;
    bscanid_en_o : out STD_LOGIC;
    tdo_i : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "bd_ebbe_lut_buffer_0,lut_buffer_v2_0_0_lut_buffer,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "lut_buffer_v2_0_0_lut_buffer,Vivado 2021.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal NLW_inst_bscanid_o_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute C_EN_BSCANID_VEC : integer;
  attribute C_EN_BSCANID_VEC of inst : label is 0;
  attribute DONT_TOUCH : boolean;
  attribute DONT_TOUCH of inst : label is std.standard.true;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of inst : label is "soft";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of inst : label is "true";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of bscanid_en_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan BSCANID_EN";
  attribute X_INTERFACE_INFO of bscanid_en_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan BSCANID_EN";
  attribute X_INTERFACE_INFO of capture_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan CAPTURE";
  attribute X_INTERFACE_INFO of capture_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan CAPTURE";
  attribute X_INTERFACE_INFO of drck_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan DRCK";
  attribute X_INTERFACE_INFO of drck_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan DRCK";
  attribute X_INTERFACE_INFO of reset_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan RESET";
  attribute X_INTERFACE_INFO of reset_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan RESET";
  attribute X_INTERFACE_INFO of runtest_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan RUNTEST";
  attribute X_INTERFACE_INFO of runtest_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan RUNTEST";
  attribute X_INTERFACE_INFO of sel_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan SEL";
  attribute X_INTERFACE_INFO of sel_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan SEL";
  attribute X_INTERFACE_INFO of shift_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan SHIFT";
  attribute X_INTERFACE_INFO of shift_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan SHIFT";
  attribute X_INTERFACE_INFO of tck_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan TCK";
  attribute X_INTERFACE_INFO of tck_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan TCK";
  attribute X_INTERFACE_INFO of tdi_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan TDI";
  attribute X_INTERFACE_INFO of tdi_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan TDI";
  attribute X_INTERFACE_INFO of tdo_i : signal is "xilinx.com:interface:bscan:1.0 m_bscan TDO";
  attribute X_INTERFACE_INFO of tdo_o : signal is "xilinx.com:interface:bscan:1.0 s_bscan TDO";
  attribute X_INTERFACE_INFO of tms_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan TMS";
  attribute X_INTERFACE_INFO of tms_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan TMS";
  attribute X_INTERFACE_INFO of update_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan UPDATE";
  attribute X_INTERFACE_INFO of update_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan UPDATE";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lut_buffer_v2_0_0_lut_buffer
     port map (
      bscanid_en_i => bscanid_en_i,
      bscanid_en_o => bscanid_en_o,
      bscanid_i(31 downto 0) => B"00000000000000000000000000000000",
      bscanid_o(31 downto 0) => NLW_inst_bscanid_o_UNCONNECTED(31 downto 0),
      capture_i => capture_i,
      capture_o => capture_o,
      drck_i => drck_i,
      drck_o => drck_o,
      reset_i => reset_i,
      reset_o => reset_o,
      runtest_i => runtest_i,
      runtest_o => runtest_o,
      sel_i => sel_i,
      sel_o => sel_o,
      shift_i => shift_i,
      shift_o => shift_o,
      tck_i => tck_i,
      tck_o => tck_o,
      tdi_i => tdi_i,
      tdi_o => tdi_o,
      tdo_i => tdo_i,
      tdo_o => tdo_o,
      tms_i => tms_i,
      tms_o => tms_o,
      update_i => update_i,
      update_o => update_o
    );
end STRUCTURE;
