
c
IRunning '/tools/Xilinx/Vitis_HLS/2021.1/bin/unwrapped/lnx64.o/vitis_hls'
*HLSZ200-10h px 

pFor user 'root' on host 'FPGA2-18.04' (Linux_x86_64 version 4.15.0-161-generic) on Wed Nov 03 18:15:42 KST 2021
*HLSZ200-10h px 
1
On os Ubuntu 18.04 LTS
*HLSZ200-10h px 
z
`In directory '/home/via/gnn_workspace/jh_gnn_211018_kernels/Hardware/build/bandwidth/bandwidth'
*HLSZ200-10h px 
?
$Sourcing Tcl script 'bandwidth.tcl'
*HLSZ200-150h px 
^
Running: %s
2001510*hls2+
open_project bandwidth 2default:defaultZ200-1510h px 

{Creating and opening project '/home/via/gnn_workspace/jh_gnn_211018_kernels/Hardware/build/bandwidth/bandwidth/bandwidth'.
*HLSZ200-10h px 
Y
Running: %s
2001510*hls2&
set_top bandwidth 2default:defaultZ200-1510h px 
Ò
Running: %s
2001510*hls2
add_files /home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp -cflags  -I /home/via/gnn_workspace/jh_gnn_211018_kernels/src  2default:defaultZ200-1510h px 
~
dAdding design file '/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp' to the project
*HLSZ200-10h px 
q
Running: %s
2001510*hls2>
*open_solution -flow_target vitis solution 2default:defaultZ200-1510h px 
 
Creating and opening solution '/home/via/gnn_workspace/jh_gnn_211018_kernels/Hardware/build/bandwidth/bandwidth/bandwidth/solution'.
*HLSZ200-10h px 
×
Using %sflow_target '%s'
2001505*hls2
 2default:default2
vitis2default:defaultZ200-1505h pxeFor help on HLS 200-1505 see www.xilinx.com/cgi-bin/docs/rdoc?v=2021.1;t=hls+guidance;d=200-1505.html 
°
Setting %s configuration: %s
200435*hls26
"'open_solution -flow_target vitis'2default:default26
"config_interface -m_axi_latency=642default:defaultZ200-435h px 
¼
Setting %s configuration: %s
200435*hls26
"'open_solution -flow_target vitis'2default:default2B
.config_interface -m_axi_alignment_byte_size=642default:defaultZ200-435h px 
¼
Setting %s configuration: %s
200435*hls26
"'open_solution -flow_target vitis'2default:default2B
.config_interface -m_axi_max_widen_bitwidth=5122default:defaultZ200-435h px 
®
Setting %s configuration: %s
200435*hls26
"'open_solution -flow_target vitis'2default:default24
 config_rtl -register_reset_num=32default:defaultZ200-435h px 
e
Running: %s
2001510*hls22
set_part xcu280-fsvh2892-2L-e 2default:defaultZ200-1510h px 
l
Setting target device to '%s'2001611*hls2(
xcu280-fsvh2892-2L-e2default:defaultZ200-1611h px 
x
Running: %s
2001510*hls2E
1create_clock -period 300.000000MHz -name default 2default:defaultZ200-1510h px 
P
5Setting up clock 'default' with a period of 3.333ns.
*SYNZ201-201h px 
l
Running: %s
2001510*hls29
%config_dataflow -strict_mode warning 2default:defaultZ200-1510h px 
v
Running: %s
2001510*hls2C
/config_export -disable_deadlock_detection=true 2default:defaultZ200-1510h px 
m
Running: %s
2001510*hls2:
&config_rtl -m_axi_conservative_mode=1 2default:defaultZ200-1510h px 
þ
cThe '%s' command is deprecated and will be removed in a future release. Use %s as its replacement.
200483*hls27
#config_rtl -m_axi_conservative_mode2default:default2=
)config_interface -m_axi_conservative_mode2default:defaultZ200-483h px 
f
Running: %s
2001510*hls23
config_interface -m_axi_addr64 2default:defaultZ200-1510h px 
p
Running: %s
2001510*hls2=
)config_interface -m_axi_auto_max_ports=0 2default:defaultZ200-1510h px 
z
Running: %s
2001510*hls2G
3config_export -format ip_catalog -ipname bandwidth 2default:defaultZ200-1510h px 
U
Running: %s
2001510*hls2"
csynth_design 2default:defaultZ200-1510h px 
Á
¥Finished File checks and directory preparation: CPU user time: 0 seconds. CPU system time: 0 seconds. Elapsed time: 0 seconds; current allocated memory: 250.176 MB.
*HLSZ200-111h px 
w
]Analyzing design file '/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp' ... 
*HLSZ200-10h px 
«
Ignore interface attribute or pragma which is not used in top function: /home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:239:9
*HLSZ207-5528h px 

junused parameter 'print': /tools/Xilinx/Vitis_HLS/2021.1/common/technology/autopilot/ap_int_base.h:792:16
*HLSZ207-5301h px 
Ë
¯Finished Source Code Analysis and Preprocessing: CPU user time: 3.82 seconds. CPU system time: 0.45 seconds. Elapsed time: 3.55 seconds; current allocated memory: 251.840 MB.
*HLSZ200-111h px 
m
/Using interface defaults for '%s' flow target.
200777*hls2
Vitis2default:defaultZ200-777h px 
Q
6Initial Interval estimation mode is set into default.
*HLSZ214-279h px 
J
/Auto array partition mode is set into default.
*HLSZ214-284h px 
§
Aggregating maxi variable 'out_c' with non-compact mode in 64-bits (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:227:0)
*HLSZ214-241h px 
°
Aggregating maxi variable 'in_data_offset' with non-compact mode in 64-bits (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:227:0)
*HLSZ214-241h px 
§
Aggregating maxi variable 'in_rc' with non-compact mode in 64-bits (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:227:0)
*HLSZ214-241h px 
¨
Aggregating maxi variable 'in_col' with non-compact mode in 64-bits (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:227:0)
*HLSZ214-241h px 
J
/Starting automatic array partition analysis...
*HLSZ214-270h px 
Ç
«Multiple burst reads of variable length and bit width 64 in loop 'VITIS_LOOP_245_1'(/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:245:31) has been inferred on port 'gmem'. These bursts requests might be further partitioned into multiple requests during RTL generation, based on max_read_burst_length or max_write_burst_length settings. (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:245:31)
*HLSZ214-115h px 
È
¬Multiple burst writes of variable length and bit width 64 in loop 'VITIS_LOOP_245_1'(/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:245:31) has been inferred on port 'gmem'. These bursts requests might be further partitioned into multiple requests during RTL generation, based on max_read_burst_length or max_write_burst_length settings. (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:245:31)
*HLSZ214-115h px 
È
¬Multiple burst writes of variable length and bit width 64 in loop 'VITIS_LOOP_250_2'(/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:250:31) has been inferred on port 'gmem'. These bursts requests might be further partitioned into multiple requests during RTL generation, based on max_read_burst_length or max_write_burst_length settings. (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:250:31)
*HLSZ214-115h px 

wInlining function '_llvm.fpga.pack.none.i64.s_struct.ssdm_int.1s' into '_llvm.fpga.unpack.none.s_struct.ap_ints.i64.1'
*HLSZ214-131h px 
¸
Inlining function '_llvm.fpga.unpack.none.s_struct.ap_ints.i64.1' into 'bandwidth' (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:246:51)
*HLSZ214-131h px 
n
SInlining function '_llvm.fpga.unpack.none.s_struct.ap_ints.i64.1' into 'bandwidth'
*HLSZ214-131h px 
Æ
ªFinished Compiling Optimization and Transform: CPU user time: 3 seconds. CPU system time: 0.25 seconds. Elapsed time: 3.25 seconds; current allocated memory: 253.194 MB.
*HLSZ200-111h px 
¬
Finished Checking Pragmas: CPU user time: 0 seconds. CPU system time: 0 seconds. Elapsed time: 0 seconds; current allocated memory: 253.195 MB.
*HLSZ200-111h px 
<
"Starting code transformations ...
*HLSZ200-10h px 
µ
Finished Standard Transforms: CPU user time: 0.02 seconds. CPU system time: 0 seconds. Elapsed time: 0.01 seconds; current allocated memory: 256.414 MB.
*HLSZ200-111h px 
8
Checking synthesizability ...
*HLSZ200-10h px 
¾
¢Finished Checking Synthesizability: CPU user time: 0.03 seconds. CPU system time: 0.01 seconds. Elapsed time: 0.03 seconds; current allocated memory: 258.943 MB.
*HLSZ200-111h px 
É
«Unrolling all sub-loops inside loop 'gather_and_sample_node' (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:241) in function 'bandwidth' for pipelining.
*XFORMZ203-502h px 
ã
SCannot unroll loop %s: cannot completely unroll a loop with a variable trip count.
200936*hls2
p'VITIS_LOOP_245_1' (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:243) in function 'bandwidth'2default:defaultZ200-936h pxcFor help on HLS 200-936 see www.xilinx.com/cgi-bin/docs/rdoc?v=2021.1;t=hls+guidance;d=200-936.html 
ã
SCannot unroll loop %s: cannot completely unroll a loop with a variable trip count.
200936*hls2
p'VITIS_LOOP_250_2' (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:243) in function 'bandwidth'2default:defaultZ200-936h pxcFor help on HLS 200-936 see www.xilinx.com/cgi-bin/docs/rdoc?v=2021.1;t=hls+guidance;d=200-936.html 
È
¬Finished Loop, function and other optimizations: CPU user time: 0.06 seconds. CPU system time: 0 seconds. Elapsed time: 0.06 seconds; current allocated memory: 283.147 MB.
*HLSZ200-111h px 
à
Cannot flatten loop %s %s.
200960*hls2
y'gather_and_sample_node' (/home/via/gnn_workspace/jh_gnn_211018_kernels/src/bandwidth.cpp:241:14) in function 'bandwidth'2default:default2*
more than one sub loop2default:defaultZ200-960h pxcFor help on HLS 200-960 see www.xilinx.com/cgi-bin/docs/rdoc?v=2021.1;t=hls+guidance;d=200-960.html 
¸
Finished Architecture Synthesis: CPU user time: 0.04 seconds. CPU system time: 0 seconds. Elapsed time: 0.05 seconds; current allocated memory: 277.805 MB.
*HLSZ200-111h px 
:
 Starting hardware synthesis ...
*HLSZ200-10h px 
7
Synthesizing 'bandwidth' ...
*HLSZ200-10h px 
[
A----------------------------------------------------------------
*HLSZ200-10h px 
>
$-- Implementing module 'bandwidth' 
*HLSZ200-42h px 
[
A----------------------------------------------------------------
*HLSZ200-10h px 
4
Starting scheduling ...
*SCHEDZ204-11h px 

Unable to satisfy pipeline directive for loop 'gather_and_sample_node': contains subloop(s) that are not unrolled or flattened.
*SCHEDZ204-65h px 
1
Finished scheduling.
*SCHEDZ204-11h px 
¯
Finished Scheduling: CPU user time: 0.68 seconds. CPU system time: 0.01 seconds. Elapsed time: 0.69 seconds; current allocated memory: 279.266 MB.
*HLSZ200-111h px 
G
+Starting micro-architecture generation ...
*BINDZ205-100h px 
C
'Performing variable lifetime analysis.
*BINDZ205-101h px 
8
Exploring resource sharing.
*BINDZ205-101h px 
(
Binding ...
*BINDZ205-101h px 
D
(Finished micro-architecture generation.
*BINDZ205-100h px 
¨
Finished Binding: CPU user time: 1.7 seconds. CPU system time: 0 seconds. Elapsed time: 1.71 seconds; current allocated memory: 281.900 MB.
*HLSZ200-111h px 
[
A----------------------------------------------------------------
*HLSZ200-10h px 
D
*-- Generating RTL for module 'bandwidth' 
*HLSZ200-10h px 
[
A----------------------------------------------------------------
*HLSZ200-10h px 
f
IDesign contains AXI ports. Reset is fixed to synchronous and active low.
*RTGENZ206-101h px 
Y
<Setting interface mode on port 'bandwidth/gmem' to 'm_axi'.
*RTGENZ206-500h px 
i
LSetting interface mode on port 'bandwidth/in_col' to 's_axilite & ap_none'.
*RTGENZ206-500h px 
h
KSetting interface mode on port 'bandwidth/in_rc' to 's_axilite & ap_none'.
*RTGENZ206-500h px 
q
TSetting interface mode on port 'bandwidth/in_data_offset' to 's_axilite & ap_none'.
*RTGENZ206-500h px 
h
KSetting interface mode on port 'bandwidth/out_c' to 's_axilite & ap_none'.
*RTGENZ206-500h px 
k
NSetting interface mode on port 'bandwidth/num_bulk' to 's_axilite & ap_none'.
*RTGENZ206-500h px 
o
RSetting interface mode on port 'bandwidth/num_neighbor' to 's_axilite & ap_none'.
*RTGENZ206-500h px 
g
JSetting interface mode on port 'bandwidth/seed' to 's_axilite & ap_none'.
*RTGENZ206-500h px 
l
OSetting interface mode on function 'bandwidth' to 's_axilite & ap_ctrl_chain'.
*RTGENZ206-500h px 

yBundling port 'in_col', 'in_rc', 'in_data_offset', 'out_c', 'num_bulk', 'num_neighbor', 'seed' to AXI-Lite port control.
*RTGENZ206-100h px 
J
-Finished creating RTL model for 'bandwidth'.
*RTGENZ206-100h px 
´
Finished Creating RTL model: CPU user time: 0.41 seconds. CPU system time: 0 seconds. Elapsed time: 0.41 seconds; current allocated memory: 288.214 MB.
*HLSZ200-111h px 
¾
¢Finished Generating all RTL models: CPU user time: 0.67 seconds. CPU system time: 0.03 seconds. Elapsed time: 0.69 seconds; current allocated memory: 295.583 MB.
*HLSZ200-111h px 
¬
_Design has MAXI bursts%s, see Vitis HLS GUI synthesis summary report for detailed information.
2001603*hls2&
 and missed bursts2default:defaultZ200-1603h px 
·
Finished Updating report files: CPU user time: 0.47 seconds. CPU system time: 0 seconds. Elapsed time: 0.47 seconds; current allocated memory: 300.626 MB.
*HLSZ200-111h px 
?
#Generating VHDL RTL for bandwidth.
*VHDLZ208-304h px 
B
&Generating Verilog RTL for bandwidth.
*VLOGZ209-307h px 
|
**** Loop Constraint Status: %s200790*hls28
$All loop constraints were satisfied.2default:defaultZ200-790h px 
Z
**** Estimated Fmax: %s MHz200789*hls2
411.002default:defaultZ200-789h px 
»
Finished Command csynth_design CPU user time: 10.92 seconds. CPU system time: 0.75 seconds. Elapsed time: 10.95 seconds; current allocated memory: 300.611 MB.
*HLSZ200-111h px 
U
Running: %s
2001510*hls2"
export_design 2default:defaultZ200-1510h px 
8
Exporting RTL as a Vivado IP.
*IMPLZ213-8h px 

í
****** Vivado v2021.1 (64-bit)
  **** SW Build 3247384 on Thu Jun 10 19:36:07 MDT 2021
  **** IP Build 3246043 on Fri Jun 11 00:30:35 MDT 2021
    ** Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.

source run_ippack.tcl -notrace
*commonh px 
G
2INFO: [IP_Flow 19-234] Refreshing IP repositories
*commonh px 
O
:INFO: [IP_Flow 19-1704] No user IP repositories specified
*commonh px 
p
[INFO: [IP_Flow 19-2313] Loaded Vivado IP repository '/tools/Xilinx/Vivado/2021.1/data/ip'.
*commonh px 
Y
DINFO: [Common 17-206] Exiting Vivado at Wed Nov  3 18:16:03 2021...
*commonh px 
t
Generated output file %s
200802*hls26
"bandwidth/solution/impl/export.zip2default:defaultZ200-802h px 
»
Finished Command export_design CPU user time: 10.67 seconds. CPU system time: 0.74 seconds. Elapsed time: 12.28 seconds; current allocated memory: 303.219 MB.
*HLSZ200-111h px 
6
HLS completed successfully
*HLSZ200-150h px 
«
Total CPU user time: 23.46 seconds. Total CPU system time: 1.89 seconds. Total elapsed time: 24.65 seconds; peak allocated memory: 300.626 MB.
*HLSZ200-112h px 

Exiting %s at %s...
206*common2
	vitis_hls2default:default2,
Wed Nov  3 18:16:06 20212default:defaultZ17-206h px 


End Record