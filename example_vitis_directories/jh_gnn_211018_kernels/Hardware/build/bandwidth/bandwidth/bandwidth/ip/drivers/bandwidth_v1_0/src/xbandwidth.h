// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2021.1 (64-bit)
// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XBANDWIDTH_H
#define XBANDWIDTH_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xbandwidth_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u32 Control_BaseAddress;
} XBandwidth_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u32 IsReady;
} XBandwidth;

typedef u32 word_type;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XBandwidth_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XBandwidth_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XBandwidth_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XBandwidth_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XBandwidth_Initialize(XBandwidth *InstancePtr, u16 DeviceId);
XBandwidth_Config* XBandwidth_LookupConfig(u16 DeviceId);
int XBandwidth_CfgInitialize(XBandwidth *InstancePtr, XBandwidth_Config *ConfigPtr);
#else
int XBandwidth_Initialize(XBandwidth *InstancePtr, const char* InstanceName);
int XBandwidth_Release(XBandwidth *InstancePtr);
#endif

void XBandwidth_Start(XBandwidth *InstancePtr);
u32 XBandwidth_IsDone(XBandwidth *InstancePtr);
u32 XBandwidth_IsIdle(XBandwidth *InstancePtr);
u32 XBandwidth_IsReady(XBandwidth *InstancePtr);
void XBandwidth_Continue(XBandwidth *InstancePtr);
void XBandwidth_EnableAutoRestart(XBandwidth *InstancePtr);
void XBandwidth_DisableAutoRestart(XBandwidth *InstancePtr);

void XBandwidth_Set_in_col(XBandwidth *InstancePtr, u64 Data);
u64 XBandwidth_Get_in_col(XBandwidth *InstancePtr);
void XBandwidth_Set_in_rc(XBandwidth *InstancePtr, u64 Data);
u64 XBandwidth_Get_in_rc(XBandwidth *InstancePtr);
void XBandwidth_Set_in_data_offset(XBandwidth *InstancePtr, u64 Data);
u64 XBandwidth_Get_in_data_offset(XBandwidth *InstancePtr);
void XBandwidth_Set_out_c(XBandwidth *InstancePtr, u64 Data);
u64 XBandwidth_Get_out_c(XBandwidth *InstancePtr);
void XBandwidth_Set_num_bulk(XBandwidth *InstancePtr, u32 Data);
u32 XBandwidth_Get_num_bulk(XBandwidth *InstancePtr);
void XBandwidth_Set_num_neighbor(XBandwidth *InstancePtr, u32 Data);
u32 XBandwidth_Get_num_neighbor(XBandwidth *InstancePtr);
void XBandwidth_Set_seed(XBandwidth *InstancePtr, u32 Data);
u32 XBandwidth_Get_seed(XBandwidth *InstancePtr);

void XBandwidth_InterruptGlobalEnable(XBandwidth *InstancePtr);
void XBandwidth_InterruptGlobalDisable(XBandwidth *InstancePtr);
void XBandwidth_InterruptEnable(XBandwidth *InstancePtr, u32 Mask);
void XBandwidth_InterruptDisable(XBandwidth *InstancePtr, u32 Mask);
void XBandwidth_InterruptClear(XBandwidth *InstancePtr, u32 Mask);
u32 XBandwidth_InterruptGetEnabled(XBandwidth *InstancePtr);
u32 XBandwidth_InterruptGetStatus(XBandwidth *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
