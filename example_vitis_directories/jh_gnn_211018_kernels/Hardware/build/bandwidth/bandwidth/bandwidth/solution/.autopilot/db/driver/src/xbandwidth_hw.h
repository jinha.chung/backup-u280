// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2021.1 (64-bit)
// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// control
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read/COR)
//        bit 4  - ap_continue (Read/Write/SC)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - enable ap_done interrupt (Read/Write)
//        bit 1  - enable ap_ready interrupt (Read/Write)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - ap_done (COR/TOW)
//        bit 1  - ap_ready (COR/TOW)
//        others - reserved
// 0x10 : Data signal of in_col
//        bit 31~0 - in_col[31:0] (Read/Write)
// 0x14 : Data signal of in_col
//        bit 31~0 - in_col[63:32] (Read/Write)
// 0x18 : reserved
// 0x1c : Data signal of in_rc
//        bit 31~0 - in_rc[31:0] (Read/Write)
// 0x20 : Data signal of in_rc
//        bit 31~0 - in_rc[63:32] (Read/Write)
// 0x24 : reserved
// 0x28 : Data signal of in_data_offset
//        bit 31~0 - in_data_offset[31:0] (Read/Write)
// 0x2c : Data signal of in_data_offset
//        bit 31~0 - in_data_offset[63:32] (Read/Write)
// 0x30 : reserved
// 0x34 : Data signal of out_c
//        bit 31~0 - out_c[31:0] (Read/Write)
// 0x38 : Data signal of out_c
//        bit 31~0 - out_c[63:32] (Read/Write)
// 0x3c : reserved
// 0x40 : Data signal of num_bulk
//        bit 31~0 - num_bulk[31:0] (Read/Write)
// 0x44 : reserved
// 0x48 : Data signal of num_neighbor
//        bit 31~0 - num_neighbor[31:0] (Read/Write)
// 0x4c : reserved
// 0x50 : Data signal of seed
//        bit 31~0 - seed[31:0] (Read/Write)
// 0x54 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XBANDWIDTH_CONTROL_ADDR_AP_CTRL             0x00
#define XBANDWIDTH_CONTROL_ADDR_GIE                 0x04
#define XBANDWIDTH_CONTROL_ADDR_IER                 0x08
#define XBANDWIDTH_CONTROL_ADDR_ISR                 0x0c
#define XBANDWIDTH_CONTROL_ADDR_IN_COL_DATA         0x10
#define XBANDWIDTH_CONTROL_BITS_IN_COL_DATA         64
#define XBANDWIDTH_CONTROL_ADDR_IN_RC_DATA          0x1c
#define XBANDWIDTH_CONTROL_BITS_IN_RC_DATA          64
#define XBANDWIDTH_CONTROL_ADDR_IN_DATA_OFFSET_DATA 0x28
#define XBANDWIDTH_CONTROL_BITS_IN_DATA_OFFSET_DATA 64
#define XBANDWIDTH_CONTROL_ADDR_OUT_C_DATA          0x34
#define XBANDWIDTH_CONTROL_BITS_OUT_C_DATA          64
#define XBANDWIDTH_CONTROL_ADDR_NUM_BULK_DATA       0x40
#define XBANDWIDTH_CONTROL_BITS_NUM_BULK_DATA       32
#define XBANDWIDTH_CONTROL_ADDR_NUM_NEIGHBOR_DATA   0x48
#define XBANDWIDTH_CONTROL_BITS_NUM_NEIGHBOR_DATA   32
#define XBANDWIDTH_CONTROL_ADDR_SEED_DATA           0x50
#define XBANDWIDTH_CONTROL_BITS_SEED_DATA           32

