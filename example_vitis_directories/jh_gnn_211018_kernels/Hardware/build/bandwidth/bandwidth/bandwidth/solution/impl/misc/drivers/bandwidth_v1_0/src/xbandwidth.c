// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2021.1 (64-bit)
// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xbandwidth.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XBandwidth_CfgInitialize(XBandwidth *InstancePtr, XBandwidth_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XBandwidth_Start(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL) & 0x80;
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL, Data | 0x01);
}

u32 XBandwidth_IsDone(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XBandwidth_IsIdle(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XBandwidth_IsReady(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XBandwidth_Continue(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL) & 0x80;
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL, Data | 0x10);
}

void XBandwidth_EnableAutoRestart(XBandwidth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL, 0x80);
}

void XBandwidth_DisableAutoRestart(XBandwidth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_AP_CTRL, 0);
}

void XBandwidth_Set_in_col(XBandwidth *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_COL_DATA, (u32)(Data));
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_COL_DATA + 4, (u32)(Data >> 32));
}

u64 XBandwidth_Get_in_col(XBandwidth *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_COL_DATA);
    Data += (u64)XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_COL_DATA + 4) << 32;
    return Data;
}

void XBandwidth_Set_in_rc(XBandwidth *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_RC_DATA, (u32)(Data));
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_RC_DATA + 4, (u32)(Data >> 32));
}

u64 XBandwidth_Get_in_rc(XBandwidth *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_RC_DATA);
    Data += (u64)XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_RC_DATA + 4) << 32;
    return Data;
}

void XBandwidth_Set_in_data_offset(XBandwidth *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_DATA_OFFSET_DATA, (u32)(Data));
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_DATA_OFFSET_DATA + 4, (u32)(Data >> 32));
}

u64 XBandwidth_Get_in_data_offset(XBandwidth *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_DATA_OFFSET_DATA);
    Data += (u64)XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IN_DATA_OFFSET_DATA + 4) << 32;
    return Data;
}

void XBandwidth_Set_out_c(XBandwidth *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_OUT_C_DATA, (u32)(Data));
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_OUT_C_DATA + 4, (u32)(Data >> 32));
}

u64 XBandwidth_Get_out_c(XBandwidth *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_OUT_C_DATA);
    Data += (u64)XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_OUT_C_DATA + 4) << 32;
    return Data;
}

void XBandwidth_Set_num_bulk(XBandwidth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_NUM_BULK_DATA, Data);
}

u32 XBandwidth_Get_num_bulk(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_NUM_BULK_DATA);
    return Data;
}

void XBandwidth_Set_num_neighbor(XBandwidth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_NUM_NEIGHBOR_DATA, Data);
}

u32 XBandwidth_Get_num_neighbor(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_NUM_NEIGHBOR_DATA);
    return Data;
}

void XBandwidth_Set_seed(XBandwidth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_SEED_DATA, Data);
}

u32 XBandwidth_Get_seed(XBandwidth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_SEED_DATA);
    return Data;
}

void XBandwidth_InterruptGlobalEnable(XBandwidth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_GIE, 1);
}

void XBandwidth_InterruptGlobalDisable(XBandwidth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_GIE, 0);
}

void XBandwidth_InterruptEnable(XBandwidth *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IER);
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IER, Register | Mask);
}

void XBandwidth_InterruptDisable(XBandwidth *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IER);
    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IER, Register & (~Mask));
}

void XBandwidth_InterruptClear(XBandwidth *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBandwidth_WriteReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_ISR, Mask);
}

u32 XBandwidth_InterruptGetEnabled(XBandwidth *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_IER);
}

u32 XBandwidth_InterruptGetStatus(XBandwidth *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XBandwidth_ReadReg(InstancePtr->Control_BaseAddress, XBANDWIDTH_CONTROL_ADDR_ISR);
}

