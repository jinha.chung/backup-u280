// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2021.1 (64-bit)
// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xbandwidth.h"

extern XBandwidth_Config XBandwidth_ConfigTable[];

XBandwidth_Config *XBandwidth_LookupConfig(u16 DeviceId) {
	XBandwidth_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XBANDWIDTH_NUM_INSTANCES; Index++) {
		if (XBandwidth_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XBandwidth_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XBandwidth_Initialize(XBandwidth *InstancePtr, u16 DeviceId) {
	XBandwidth_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XBandwidth_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XBandwidth_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

