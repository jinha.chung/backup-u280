#include <systemc>
#include <vector>
#include <iostream>
#include "hls_stream.h"
#include "ap_int.h"
#include "ap_fixed.h"
using namespace std;
using namespace sc_dt;
class AESL_RUNTIME_BC {
  public:
    AESL_RUNTIME_BC(const char* name) {
      file_token.open( name);
      if (!file_token.good()) {
        cout << "Failed to open tv file " << name << endl;
        exit (1);
      }
      file_token >> mName;//[[[runtime]]]
    }
    ~AESL_RUNTIME_BC() {
      file_token.close();
    }
    int read_size () {
      int size = 0;
      file_token >> mName;//[[transaction]]
      file_token >> mName;//transaction number
      file_token >> mName;//pop_size
      size = atoi(mName.c_str());
      file_token >> mName;//[[/transaction]]
      return size;
    }
  public:
    fstream file_token;
    string mName;
};
extern "C" void bandwidth(long long*, int, int, int, int, int, int, int);
extern "C" void apatb_bandwidth_hw(volatile void * __xlx_apatb_param_in_col, volatile void * __xlx_apatb_param_in_rc, volatile void * __xlx_apatb_param_in_data_offset, volatile void * __xlx_apatb_param_out_c, int __xlx_apatb_param_num_bulk, int __xlx_apatb_param_num_neighbor, int __xlx_apatb_param_seed) {
  // Collect __xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec
  vector<sc_bv<64> >__xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec;
  for (int j = 0, e = 1; j != e; ++j) {
    sc_bv<64> _xlx_tmp_sc;
    _xlx_tmp_sc.range(7, 0) = ((char*)__xlx_apatb_param_in_col)[j*8+0];
    _xlx_tmp_sc.range(15, 8) = ((char*)__xlx_apatb_param_in_col)[j*8+1];
    _xlx_tmp_sc.range(23, 16) = ((char*)__xlx_apatb_param_in_col)[j*8+2];
    _xlx_tmp_sc.range(31, 24) = ((char*)__xlx_apatb_param_in_col)[j*8+3];
    _xlx_tmp_sc.range(39, 32) = ((char*)__xlx_apatb_param_in_col)[j*8+4];
    _xlx_tmp_sc.range(47, 40) = ((char*)__xlx_apatb_param_in_col)[j*8+5];
    _xlx_tmp_sc.range(55, 48) = ((char*)__xlx_apatb_param_in_col)[j*8+6];
    _xlx_tmp_sc.range(63, 56) = ((char*)__xlx_apatb_param_in_col)[j*8+7];
    __xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec.push_back(_xlx_tmp_sc);
  }
  int __xlx_size_param_in_col = 1;
  int __xlx_offset_param_in_col = 0;
  int __xlx_offset_byte_param_in_col = 0*8;
  for (int j = 0, e = 1; j != e; ++j) {
    sc_bv<64> _xlx_tmp_sc;
    _xlx_tmp_sc.range(7, 0) = ((char*)__xlx_apatb_param_in_rc)[j*8+0];
    _xlx_tmp_sc.range(15, 8) = ((char*)__xlx_apatb_param_in_rc)[j*8+1];
    _xlx_tmp_sc.range(23, 16) = ((char*)__xlx_apatb_param_in_rc)[j*8+2];
    _xlx_tmp_sc.range(31, 24) = ((char*)__xlx_apatb_param_in_rc)[j*8+3];
    _xlx_tmp_sc.range(39, 32) = ((char*)__xlx_apatb_param_in_rc)[j*8+4];
    _xlx_tmp_sc.range(47, 40) = ((char*)__xlx_apatb_param_in_rc)[j*8+5];
    _xlx_tmp_sc.range(55, 48) = ((char*)__xlx_apatb_param_in_rc)[j*8+6];
    _xlx_tmp_sc.range(63, 56) = ((char*)__xlx_apatb_param_in_rc)[j*8+7];
    __xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec.push_back(_xlx_tmp_sc);
  }
  int __xlx_size_param_in_rc = 1;
  int __xlx_offset_param_in_rc = 1;
  int __xlx_offset_byte_param_in_rc = 1*8;
  for (int j = 0, e = 1; j != e; ++j) {
    sc_bv<64> _xlx_tmp_sc;
    _xlx_tmp_sc.range(7, 0) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+0];
    _xlx_tmp_sc.range(15, 8) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+1];
    _xlx_tmp_sc.range(23, 16) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+2];
    _xlx_tmp_sc.range(31, 24) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+3];
    _xlx_tmp_sc.range(39, 32) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+4];
    _xlx_tmp_sc.range(47, 40) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+5];
    _xlx_tmp_sc.range(55, 48) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+6];
    _xlx_tmp_sc.range(63, 56) = ((char*)__xlx_apatb_param_in_data_offset)[j*8+7];
    __xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec.push_back(_xlx_tmp_sc);
  }
  int __xlx_size_param_in_data_offset = 1;
  int __xlx_offset_param_in_data_offset = 2;
  int __xlx_offset_byte_param_in_data_offset = 2*8;
  for (int j = 0, e = 1; j != e; ++j) {
    sc_bv<64> _xlx_tmp_sc;
    _xlx_tmp_sc.range(7, 0) = ((char*)__xlx_apatb_param_out_c)[j*8+0];
    _xlx_tmp_sc.range(15, 8) = ((char*)__xlx_apatb_param_out_c)[j*8+1];
    _xlx_tmp_sc.range(23, 16) = ((char*)__xlx_apatb_param_out_c)[j*8+2];
    _xlx_tmp_sc.range(31, 24) = ((char*)__xlx_apatb_param_out_c)[j*8+3];
    _xlx_tmp_sc.range(39, 32) = ((char*)__xlx_apatb_param_out_c)[j*8+4];
    _xlx_tmp_sc.range(47, 40) = ((char*)__xlx_apatb_param_out_c)[j*8+5];
    _xlx_tmp_sc.range(55, 48) = ((char*)__xlx_apatb_param_out_c)[j*8+6];
    _xlx_tmp_sc.range(63, 56) = ((char*)__xlx_apatb_param_out_c)[j*8+7];
    __xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec.push_back(_xlx_tmp_sc);
  }
  int __xlx_size_param_out_c = 1;
  int __xlx_offset_param_out_c = 3;
  int __xlx_offset_byte_param_out_c = 3*8;
  long long* __xlx_in_col_in_rc_in_data_offset_out_c__input_buffer= new long long[__xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec.size()];
  for (int i = 0; i < __xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec.size(); ++i) {
    __xlx_in_col_in_rc_in_data_offset_out_c__input_buffer[i] = __xlx_in_col_in_rc_in_data_offset_out_c__tmp_vec[i].range(63, 0).to_uint64();
  }
  // DUT call
  bandwidth(__xlx_in_col_in_rc_in_data_offset_out_c__input_buffer, __xlx_offset_byte_param_in_col, __xlx_offset_byte_param_in_rc, __xlx_offset_byte_param_in_data_offset, __xlx_offset_byte_param_out_c, __xlx_apatb_param_num_bulk, __xlx_apatb_param_num_neighbor, __xlx_apatb_param_seed);
// print __xlx_apatb_param_in_col
  sc_bv<64>*__xlx_in_col_output_buffer = new sc_bv<64>[__xlx_size_param_in_col];
  for (int i = 0; i < __xlx_size_param_in_col; ++i) {
    __xlx_in_col_output_buffer[i] = __xlx_in_col_in_rc_in_data_offset_out_c__input_buffer[i+__xlx_offset_param_in_col];
  }
  for (int i = 0; i < __xlx_size_param_in_col; ++i) {
    ((char*)__xlx_apatb_param_in_col)[i*8+0] = __xlx_in_col_output_buffer[i].range(7, 0).to_uint();
    ((char*)__xlx_apatb_param_in_col)[i*8+1] = __xlx_in_col_output_buffer[i].range(15, 8).to_uint();
    ((char*)__xlx_apatb_param_in_col)[i*8+2] = __xlx_in_col_output_buffer[i].range(23, 16).to_uint();
    ((char*)__xlx_apatb_param_in_col)[i*8+3] = __xlx_in_col_output_buffer[i].range(31, 24).to_uint();
    ((char*)__xlx_apatb_param_in_col)[i*8+4] = __xlx_in_col_output_buffer[i].range(39, 32).to_uint();
    ((char*)__xlx_apatb_param_in_col)[i*8+5] = __xlx_in_col_output_buffer[i].range(47, 40).to_uint();
    ((char*)__xlx_apatb_param_in_col)[i*8+6] = __xlx_in_col_output_buffer[i].range(55, 48).to_uint();
    ((char*)__xlx_apatb_param_in_col)[i*8+7] = __xlx_in_col_output_buffer[i].range(63, 56).to_uint();
  }
// print __xlx_apatb_param_in_rc
  sc_bv<64>*__xlx_in_rc_output_buffer = new sc_bv<64>[__xlx_size_param_in_rc];
  for (int i = 0; i < __xlx_size_param_in_rc; ++i) {
    __xlx_in_rc_output_buffer[i] = __xlx_in_col_in_rc_in_data_offset_out_c__input_buffer[i+__xlx_offset_param_in_rc];
  }
  for (int i = 0; i < __xlx_size_param_in_rc; ++i) {
    ((char*)__xlx_apatb_param_in_rc)[i*8+0] = __xlx_in_rc_output_buffer[i].range(7, 0).to_uint();
    ((char*)__xlx_apatb_param_in_rc)[i*8+1] = __xlx_in_rc_output_buffer[i].range(15, 8).to_uint();
    ((char*)__xlx_apatb_param_in_rc)[i*8+2] = __xlx_in_rc_output_buffer[i].range(23, 16).to_uint();
    ((char*)__xlx_apatb_param_in_rc)[i*8+3] = __xlx_in_rc_output_buffer[i].range(31, 24).to_uint();
    ((char*)__xlx_apatb_param_in_rc)[i*8+4] = __xlx_in_rc_output_buffer[i].range(39, 32).to_uint();
    ((char*)__xlx_apatb_param_in_rc)[i*8+5] = __xlx_in_rc_output_buffer[i].range(47, 40).to_uint();
    ((char*)__xlx_apatb_param_in_rc)[i*8+6] = __xlx_in_rc_output_buffer[i].range(55, 48).to_uint();
    ((char*)__xlx_apatb_param_in_rc)[i*8+7] = __xlx_in_rc_output_buffer[i].range(63, 56).to_uint();
  }
// print __xlx_apatb_param_in_data_offset
  sc_bv<64>*__xlx_in_data_offset_output_buffer = new sc_bv<64>[__xlx_size_param_in_data_offset];
  for (int i = 0; i < __xlx_size_param_in_data_offset; ++i) {
    __xlx_in_data_offset_output_buffer[i] = __xlx_in_col_in_rc_in_data_offset_out_c__input_buffer[i+__xlx_offset_param_in_data_offset];
  }
  for (int i = 0; i < __xlx_size_param_in_data_offset; ++i) {
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+0] = __xlx_in_data_offset_output_buffer[i].range(7, 0).to_uint();
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+1] = __xlx_in_data_offset_output_buffer[i].range(15, 8).to_uint();
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+2] = __xlx_in_data_offset_output_buffer[i].range(23, 16).to_uint();
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+3] = __xlx_in_data_offset_output_buffer[i].range(31, 24).to_uint();
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+4] = __xlx_in_data_offset_output_buffer[i].range(39, 32).to_uint();
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+5] = __xlx_in_data_offset_output_buffer[i].range(47, 40).to_uint();
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+6] = __xlx_in_data_offset_output_buffer[i].range(55, 48).to_uint();
    ((char*)__xlx_apatb_param_in_data_offset)[i*8+7] = __xlx_in_data_offset_output_buffer[i].range(63, 56).to_uint();
  }
// print __xlx_apatb_param_out_c
  sc_bv<64>*__xlx_out_c_output_buffer = new sc_bv<64>[__xlx_size_param_out_c];
  for (int i = 0; i < __xlx_size_param_out_c; ++i) {
    __xlx_out_c_output_buffer[i] = __xlx_in_col_in_rc_in_data_offset_out_c__input_buffer[i+__xlx_offset_param_out_c];
  }
  for (int i = 0; i < __xlx_size_param_out_c; ++i) {
    ((char*)__xlx_apatb_param_out_c)[i*8+0] = __xlx_out_c_output_buffer[i].range(7, 0).to_uint();
    ((char*)__xlx_apatb_param_out_c)[i*8+1] = __xlx_out_c_output_buffer[i].range(15, 8).to_uint();
    ((char*)__xlx_apatb_param_out_c)[i*8+2] = __xlx_out_c_output_buffer[i].range(23, 16).to_uint();
    ((char*)__xlx_apatb_param_out_c)[i*8+3] = __xlx_out_c_output_buffer[i].range(31, 24).to_uint();
    ((char*)__xlx_apatb_param_out_c)[i*8+4] = __xlx_out_c_output_buffer[i].range(39, 32).to_uint();
    ((char*)__xlx_apatb_param_out_c)[i*8+5] = __xlx_out_c_output_buffer[i].range(47, 40).to_uint();
    ((char*)__xlx_apatb_param_out_c)[i*8+6] = __xlx_out_c_output_buffer[i].range(55, 48).to_uint();
    ((char*)__xlx_apatb_param_out_c)[i*8+7] = __xlx_out_c_output_buffer[i].range(63, 56).to_uint();
  }
}
