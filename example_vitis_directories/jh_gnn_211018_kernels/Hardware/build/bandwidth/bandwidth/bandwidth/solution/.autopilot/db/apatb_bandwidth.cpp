#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;

// wrapc file define:
#define AUTOTB_TVIN_gmem "../tv/cdatafile/c.bandwidth.autotvin_gmem.dat"
#define AUTOTB_TVOUT_gmem "../tv/cdatafile/c.bandwidth.autotvout_gmem.dat"
// wrapc file define:
#define AUTOTB_TVIN_in_col "../tv/cdatafile/c.bandwidth.autotvin_in_col.dat"
#define AUTOTB_TVOUT_in_col "../tv/cdatafile/c.bandwidth.autotvout_in_col.dat"
// wrapc file define:
#define AUTOTB_TVIN_in_rc "../tv/cdatafile/c.bandwidth.autotvin_in_rc.dat"
#define AUTOTB_TVOUT_in_rc "../tv/cdatafile/c.bandwidth.autotvout_in_rc.dat"
// wrapc file define:
#define AUTOTB_TVIN_in_data_offset "../tv/cdatafile/c.bandwidth.autotvin_in_data_offset.dat"
#define AUTOTB_TVOUT_in_data_offset "../tv/cdatafile/c.bandwidth.autotvout_in_data_offset.dat"
// wrapc file define:
#define AUTOTB_TVIN_out_c "../tv/cdatafile/c.bandwidth.autotvin_out_c.dat"
#define AUTOTB_TVOUT_out_c "../tv/cdatafile/c.bandwidth.autotvout_out_c.dat"
// wrapc file define:
#define AUTOTB_TVIN_num_bulk "../tv/cdatafile/c.bandwidth.autotvin_num_bulk.dat"
#define AUTOTB_TVOUT_num_bulk "../tv/cdatafile/c.bandwidth.autotvout_num_bulk.dat"
// wrapc file define:
#define AUTOTB_TVIN_num_neighbor "../tv/cdatafile/c.bandwidth.autotvin_num_neighbor.dat"
#define AUTOTB_TVOUT_num_neighbor "../tv/cdatafile/c.bandwidth.autotvout_num_neighbor.dat"
// wrapc file define:
#define AUTOTB_TVIN_seed "../tv/cdatafile/c.bandwidth.autotvin_seed.dat"
#define AUTOTB_TVOUT_seed "../tv/cdatafile/c.bandwidth.autotvout_seed.dat"

#define INTER_TCL "../tv/cdatafile/ref.tcl"

// tvout file define:
#define AUTOTB_TVOUT_PC_gmem "../tv/rtldatafile/rtl.bandwidth.autotvout_gmem.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_in_col "../tv/rtldatafile/rtl.bandwidth.autotvout_in_col.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_in_rc "../tv/rtldatafile/rtl.bandwidth.autotvout_in_rc.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_in_data_offset "../tv/rtldatafile/rtl.bandwidth.autotvout_in_data_offset.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_out_c "../tv/rtldatafile/rtl.bandwidth.autotvout_out_c.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_num_bulk "../tv/rtldatafile/rtl.bandwidth.autotvout_num_bulk.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_num_neighbor "../tv/rtldatafile/rtl.bandwidth.autotvout_num_neighbor.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_seed "../tv/rtldatafile/rtl.bandwidth.autotvout_seed.dat"

class INTER_TCL_FILE {
  public:
INTER_TCL_FILE(const char* name) {
  mName = name; 
  gmem_depth = 0;
  in_col_depth = 0;
  in_rc_depth = 0;
  in_data_offset_depth = 0;
  out_c_depth = 0;
  num_bulk_depth = 0;
  num_neighbor_depth = 0;
  seed_depth = 0;
  trans_num =0;
}
~INTER_TCL_FILE() {
  mFile.open(mName);
  if (!mFile.good()) {
    cout << "Failed to open file ref.tcl" << endl;
    exit (1); 
  }
  string total_list = get_depth_list();
  mFile << "set depth_list {\n";
  mFile << total_list;
  mFile << "}\n";
  mFile << "set trans_num "<<trans_num<<endl;
  mFile.close();
}
string get_depth_list () {
  stringstream total_list;
  total_list << "{gmem " << gmem_depth << "}\n";
  total_list << "{in_col " << in_col_depth << "}\n";
  total_list << "{in_rc " << in_rc_depth << "}\n";
  total_list << "{in_data_offset " << in_data_offset_depth << "}\n";
  total_list << "{out_c " << out_c_depth << "}\n";
  total_list << "{num_bulk " << num_bulk_depth << "}\n";
  total_list << "{num_neighbor " << num_neighbor_depth << "}\n";
  total_list << "{seed " << seed_depth << "}\n";
  return total_list.str();
}
void set_num (int num , int* class_num) {
  (*class_num) = (*class_num) > num ? (*class_num) : num;
}
void set_string(std::string list, std::string* class_list) {
  (*class_list) = list;
}
  public:
    int gmem_depth;
    int in_col_depth;
    int in_rc_depth;
    int in_data_offset_depth;
    int out_c_depth;
    int num_bulk_depth;
    int num_neighbor_depth;
    int seed_depth;
    int trans_num;
  private:
    ofstream mFile;
    const char* mName;
};

static void RTLOutputCheckAndReplacement(std::string &AESL_token, std::string PortName) {
  bool no_x = false;
  bool err = false;

  no_x = false;
  // search and replace 'X' with '0' from the 3rd char of token
  while (!no_x) {
    size_t x_found = AESL_token.find('X', 0);
    if (x_found != string::npos) {
      if (!err) { 
        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port" 
             << PortName << ", possible cause: There are uninitialized variables in the C design."
             << endl; 
        err = true;
      }
      AESL_token.replace(x_found, 1, "0");
    } else
      no_x = true;
  }
  no_x = false;
  // search and replace 'x' with '0' from the 3rd char of token
  while (!no_x) {
    size_t x_found = AESL_token.find('x', 2);
    if (x_found != string::npos) {
      if (!err) { 
        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'x' on port" 
             << PortName << ", possible cause: There are uninitialized variables in the C design."
             << endl; 
        err = true;
      }
      AESL_token.replace(x_found, 1, "0");
    } else
      no_x = true;
  }
}
extern "C" void bandwidth_hw_stub_wrapper(volatile void *, volatile void *, volatile void *, volatile void *, int, int, int);

extern "C" void apatb_bandwidth_hw(volatile void * __xlx_apatb_param_in_col, volatile void * __xlx_apatb_param_in_rc, volatile void * __xlx_apatb_param_in_data_offset, volatile void * __xlx_apatb_param_out_c, int __xlx_apatb_param_num_bulk, int __xlx_apatb_param_num_neighbor, int __xlx_apatb_param_seed) {
  refine_signal_handler();
  fstream wrapc_switch_file_token;
  wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
  int AESL_i;
  if (wrapc_switch_file_token.good())
  {

    CodeState = ENTER_WRAPC_PC;
    static unsigned AESL_transaction_pc = 0;
    string AESL_token;
    string AESL_num;{
      static ifstream rtl_tv_out_file;
      if (!rtl_tv_out_file.is_open()) {
        rtl_tv_out_file.open(AUTOTB_TVOUT_PC_gmem);
        if (rtl_tv_out_file.good()) {
          rtl_tv_out_file >> AESL_token;
          if (AESL_token != "[[[runtime]]]")
            exit(1);
        }
      }
  
      if (rtl_tv_out_file.good()) {
        rtl_tv_out_file >> AESL_token; 
        rtl_tv_out_file >> AESL_num;  // transaction number
        if (AESL_token != "[[transaction]]") {
          cerr << "Unexpected token: " << AESL_token << endl;
          exit(1);
        }
        if (atoi(AESL_num.c_str()) == AESL_transaction_pc) {
          std::vector<sc_bv<64> > gmem_pc_buffer(4);
          int i = 0;

          rtl_tv_out_file >> AESL_token; //data
          while (AESL_token != "[[/transaction]]"){

            RTLOutputCheckAndReplacement(AESL_token, "gmem");
  
            // push token into output port buffer
            if (AESL_token != "") {
              gmem_pc_buffer[i] = AESL_token.c_str();;
              i++;
            }
  
            rtl_tv_out_file >> AESL_token; //data or [[/transaction]]
            if (AESL_token == "[[[/runtime]]]" || rtl_tv_out_file.eof())
              exit(1);
          }
          if (i > 0) {{
            int i = 0;
            for (int j = 0, e = 1; j < e; j += 1, ++i) {((char*)__xlx_apatb_param_in_col)[j*8+0] = gmem_pc_buffer[i].range(7, 0).to_int64();
((char*)__xlx_apatb_param_in_col)[j*8+1] = gmem_pc_buffer[i].range(15, 8).to_int64();
((char*)__xlx_apatb_param_in_col)[j*8+2] = gmem_pc_buffer[i].range(23, 16).to_int64();
((char*)__xlx_apatb_param_in_col)[j*8+3] = gmem_pc_buffer[i].range(31, 24).to_int64();
((char*)__xlx_apatb_param_in_col)[j*8+4] = gmem_pc_buffer[i].range(39, 32).to_int64();
((char*)__xlx_apatb_param_in_col)[j*8+5] = gmem_pc_buffer[i].range(47, 40).to_int64();
((char*)__xlx_apatb_param_in_col)[j*8+6] = gmem_pc_buffer[i].range(55, 48).to_int64();
((char*)__xlx_apatb_param_in_col)[j*8+7] = gmem_pc_buffer[i].range(63, 56).to_int64();
}
            for (int j = 0, e = 1; j < e; j += 1, ++i) {((char*)__xlx_apatb_param_in_rc)[j*8+0] = gmem_pc_buffer[i].range(7, 0).to_int64();
((char*)__xlx_apatb_param_in_rc)[j*8+1] = gmem_pc_buffer[i].range(15, 8).to_int64();
((char*)__xlx_apatb_param_in_rc)[j*8+2] = gmem_pc_buffer[i].range(23, 16).to_int64();
((char*)__xlx_apatb_param_in_rc)[j*8+3] = gmem_pc_buffer[i].range(31, 24).to_int64();
((char*)__xlx_apatb_param_in_rc)[j*8+4] = gmem_pc_buffer[i].range(39, 32).to_int64();
((char*)__xlx_apatb_param_in_rc)[j*8+5] = gmem_pc_buffer[i].range(47, 40).to_int64();
((char*)__xlx_apatb_param_in_rc)[j*8+6] = gmem_pc_buffer[i].range(55, 48).to_int64();
((char*)__xlx_apatb_param_in_rc)[j*8+7] = gmem_pc_buffer[i].range(63, 56).to_int64();
}
            for (int j = 0, e = 1; j < e; j += 1, ++i) {((char*)__xlx_apatb_param_in_data_offset)[j*8+0] = gmem_pc_buffer[i].range(7, 0).to_int64();
((char*)__xlx_apatb_param_in_data_offset)[j*8+1] = gmem_pc_buffer[i].range(15, 8).to_int64();
((char*)__xlx_apatb_param_in_data_offset)[j*8+2] = gmem_pc_buffer[i].range(23, 16).to_int64();
((char*)__xlx_apatb_param_in_data_offset)[j*8+3] = gmem_pc_buffer[i].range(31, 24).to_int64();
((char*)__xlx_apatb_param_in_data_offset)[j*8+4] = gmem_pc_buffer[i].range(39, 32).to_int64();
((char*)__xlx_apatb_param_in_data_offset)[j*8+5] = gmem_pc_buffer[i].range(47, 40).to_int64();
((char*)__xlx_apatb_param_in_data_offset)[j*8+6] = gmem_pc_buffer[i].range(55, 48).to_int64();
((char*)__xlx_apatb_param_in_data_offset)[j*8+7] = gmem_pc_buffer[i].range(63, 56).to_int64();
}
            for (int j = 0, e = 1; j < e; j += 1, ++i) {((char*)__xlx_apatb_param_out_c)[j*8+0] = gmem_pc_buffer[i].range(7, 0).to_int64();
((char*)__xlx_apatb_param_out_c)[j*8+1] = gmem_pc_buffer[i].range(15, 8).to_int64();
((char*)__xlx_apatb_param_out_c)[j*8+2] = gmem_pc_buffer[i].range(23, 16).to_int64();
((char*)__xlx_apatb_param_out_c)[j*8+3] = gmem_pc_buffer[i].range(31, 24).to_int64();
((char*)__xlx_apatb_param_out_c)[j*8+4] = gmem_pc_buffer[i].range(39, 32).to_int64();
((char*)__xlx_apatb_param_out_c)[j*8+5] = gmem_pc_buffer[i].range(47, 40).to_int64();
((char*)__xlx_apatb_param_out_c)[j*8+6] = gmem_pc_buffer[i].range(55, 48).to_int64();
((char*)__xlx_apatb_param_out_c)[j*8+7] = gmem_pc_buffer[i].range(63, 56).to_int64();
}}}
        } // end transaction
      } // end file is good
    } // end post check logic bolck
  
    AESL_transaction_pc++;
    return ;
  }
static unsigned AESL_transaction;
static AESL_FILE_HANDLER aesl_fh;
static INTER_TCL_FILE tcl_file(INTER_TCL);
std::vector<char> __xlx_sprintf_buffer(1024);
CodeState = ENTER_WRAPC;
//gmem
aesl_fh.touch(AUTOTB_TVIN_gmem);
aesl_fh.touch(AUTOTB_TVOUT_gmem);
//in_col
aesl_fh.touch(AUTOTB_TVIN_in_col);
aesl_fh.touch(AUTOTB_TVOUT_in_col);
//in_rc
aesl_fh.touch(AUTOTB_TVIN_in_rc);
aesl_fh.touch(AUTOTB_TVOUT_in_rc);
//in_data_offset
aesl_fh.touch(AUTOTB_TVIN_in_data_offset);
aesl_fh.touch(AUTOTB_TVOUT_in_data_offset);
//out_c
aesl_fh.touch(AUTOTB_TVIN_out_c);
aesl_fh.touch(AUTOTB_TVOUT_out_c);
//num_bulk
aesl_fh.touch(AUTOTB_TVIN_num_bulk);
aesl_fh.touch(AUTOTB_TVOUT_num_bulk);
//num_neighbor
aesl_fh.touch(AUTOTB_TVIN_num_neighbor);
aesl_fh.touch(AUTOTB_TVOUT_num_neighbor);
//seed
aesl_fh.touch(AUTOTB_TVIN_seed);
aesl_fh.touch(AUTOTB_TVOUT_seed);
CodeState = DUMP_INPUTS;
unsigned __xlx_offset_byte_param_in_col = 0;
unsigned __xlx_offset_byte_param_in_rc = 0;
unsigned __xlx_offset_byte_param_in_data_offset = 0;
unsigned __xlx_offset_byte_param_out_c = 0;
// print gmem Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_gmem, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_in_col = 0*8;
  if (__xlx_apatb_param_in_col) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_in_col)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_gmem, s.append("\n")); 
      }
  }
  __xlx_offset_byte_param_in_rc = 1*8;
  if (__xlx_apatb_param_in_rc) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_in_rc)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_gmem, s.append("\n")); 
      }
  }
  __xlx_offset_byte_param_in_data_offset = 2*8;
  if (__xlx_apatb_param_in_data_offset) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_in_data_offset)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_gmem, s.append("\n")); 
      }
  }
  __xlx_offset_byte_param_out_c = 3*8;
  if (__xlx_apatb_param_out_c) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_out_c)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_gmem, s.append("\n")); 
      }
  }
}
  tcl_file.set_num(4, &tcl_file.gmem_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_gmem, __xlx_sprintf_buffer.data());
}
// print in_col Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_in_col, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_in_col;

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_in_col, s.append("\n")); 
  }
  tcl_file.set_num(1, &tcl_file.in_col_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_in_col, __xlx_sprintf_buffer.data());
}
// print in_rc Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_in_rc, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_in_rc;

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_in_rc, s.append("\n")); 
  }
  tcl_file.set_num(1, &tcl_file.in_rc_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_in_rc, __xlx_sprintf_buffer.data());
}
// print in_data_offset Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_in_data_offset, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_in_data_offset;

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_in_data_offset, s.append("\n")); 
  }
  tcl_file.set_num(1, &tcl_file.in_data_offset_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_in_data_offset, __xlx_sprintf_buffer.data());
}
// print out_c Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_out_c, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_out_c;

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_out_c, s.append("\n")); 
  }
  tcl_file.set_num(1, &tcl_file.out_c_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_out_c, __xlx_sprintf_buffer.data());
}
// print num_bulk Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_num_bulk, __xlx_sprintf_buffer.data());
  {
    sc_bv<32> __xlx_tmp_lv = *((int*)&__xlx_apatb_param_num_bulk);

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_num_bulk, s.append("\n")); 
  }
  tcl_file.set_num(1, &tcl_file.num_bulk_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_num_bulk, __xlx_sprintf_buffer.data());
}
// print num_neighbor Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_num_neighbor, __xlx_sprintf_buffer.data());
  {
    sc_bv<32> __xlx_tmp_lv = *((int*)&__xlx_apatb_param_num_neighbor);

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_num_neighbor, s.append("\n")); 
  }
  tcl_file.set_num(1, &tcl_file.num_neighbor_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_num_neighbor, __xlx_sprintf_buffer.data());
}
// print seed Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_seed, __xlx_sprintf_buffer.data());
  {
    sc_bv<32> __xlx_tmp_lv = *((int*)&__xlx_apatb_param_seed);

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVIN_seed, s.append("\n")); 
  }
  tcl_file.set_num(1, &tcl_file.seed_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_seed, __xlx_sprintf_buffer.data());
}
CodeState = CALL_C_DUT;
bandwidth_hw_stub_wrapper(__xlx_apatb_param_in_col, __xlx_apatb_param_in_rc, __xlx_apatb_param_in_data_offset, __xlx_apatb_param_out_c, __xlx_apatb_param_num_bulk, __xlx_apatb_param_num_neighbor, __xlx_apatb_param_seed);
CodeState = DUMP_OUTPUTS;
// print gmem Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVOUT_gmem, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_in_col = 0*8;
  if (__xlx_apatb_param_in_col) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_in_col)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVOUT_gmem, s.append("\n")); 
      }
  }
  __xlx_offset_byte_param_in_rc = 1*8;
  if (__xlx_apatb_param_in_rc) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_in_rc)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVOUT_gmem, s.append("\n")); 
      }
  }
  __xlx_offset_byte_param_in_data_offset = 2*8;
  if (__xlx_apatb_param_in_data_offset) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_in_data_offset)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVOUT_gmem, s.append("\n")); 
      }
  }
  __xlx_offset_byte_param_out_c = 3*8;
  if (__xlx_apatb_param_out_c) {
    for (int j = 0  - 0, e = 1 - 0; j != e; ++j) {
sc_bv<64> __xlx_tmp_lv = ((long long*)__xlx_apatb_param_out_c)[j];

    std::string s(__xlx_tmp_lv.to_string(SC_HEX));
    aesl_fh.write(AUTOTB_TVOUT_gmem, s.append("\n")); 
      }
  }
}
  tcl_file.set_num(4, &tcl_file.gmem_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVOUT_gmem, __xlx_sprintf_buffer.data());
}
CodeState = DELETE_CHAR_BUFFERS;
AESL_transaction++;
tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);
}
