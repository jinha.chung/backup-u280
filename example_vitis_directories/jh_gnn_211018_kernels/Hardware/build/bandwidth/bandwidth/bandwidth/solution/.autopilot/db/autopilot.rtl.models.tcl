set SynModuleInfo {
  {SRCNAME bandwidth MODELNAME bandwidth RTLNAME bandwidth IS_TOP 1
    SUBMODULES {
      {MODELNAME bandwidth_control_s_axi RTLNAME bandwidth_control_s_axi BINDTYPE interface TYPE interface_s_axilite}
      {MODELNAME bandwidth_gmem_m_axi RTLNAME bandwidth_gmem_m_axi BINDTYPE interface TYPE interface_m_axi}
    }
  }
}
