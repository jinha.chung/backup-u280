; ModuleID = '/home/via/gnn_workspace/jh_gnn_211018_kernels/Hardware/build/bandwidth/bandwidth/bandwidth/solution/.autopilot/db/a.g.ld.5.gdce.bc'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-i128:128-i256:256-i512:512-i1024:1024-i2048:2048-i4096:4096-n8:16:32:64-S128-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024"
target triple = "fpga64-xilinx-none"

%struct.ap_int = type { %struct.ap_int_base.0 }
%struct.ap_int_base.0 = type { %struct.ssdm_int.1 }
%struct.ssdm_int.1 = type { i64 }

; Function Attrs: noinline
define void @apatb_bandwidth_ir(%struct.ap_int* %in_col, %struct.ap_int* %in_rc, %struct.ap_int* %in_data_offset, %struct.ap_int* %out_c, i32 %num_bulk, i32 %num_neighbor, i32 %seed) local_unnamed_addr #0 {
entry:
  %in_col_copy1 = alloca i64, align 512
  %in_rc_copy2 = alloca i64, align 512
  %in_data_offset_copy3 = alloca i64, align 512
  %out_c_copy4 = alloca i64, align 512
  call fastcc void @copy_in(%struct.ap_int* %in_col, i64* nonnull align 512 %in_col_copy1, %struct.ap_int* %in_rc, i64* nonnull align 512 %in_rc_copy2, %struct.ap_int* %in_data_offset, i64* nonnull align 512 %in_data_offset_copy3, %struct.ap_int* %out_c, i64* nonnull align 512 %out_c_copy4)
  call void @apatb_bandwidth_hw(i64* %in_col_copy1, i64* %in_rc_copy2, i64* %in_data_offset_copy3, i64* %out_c_copy4, i32 %num_bulk, i32 %num_neighbor, i32 %seed)
  call fastcc void @copy_out(%struct.ap_int* %in_col, i64* nonnull align 512 %in_col_copy1, %struct.ap_int* %in_rc, i64* nonnull align 512 %in_rc_copy2, %struct.ap_int* %in_data_offset, i64* nonnull align 512 %in_data_offset_copy3, %struct.ap_int* %out_c, i64* nonnull align 512 %out_c_copy4)
  ret void
}

; Function Attrs: argmemonly noinline
define internal fastcc void @copy_in(%struct.ap_int* readonly, i64* noalias align 512, %struct.ap_int* readonly, i64* noalias align 512, %struct.ap_int* readonly, i64* noalias align 512, %struct.ap_int* readonly, i64* noalias align 512) unnamed_addr #1 {
entry:
  call fastcc void @onebyonecpy_hls.p0struct.ap_int(i64* align 512 %1, %struct.ap_int* %0)
  call fastcc void @onebyonecpy_hls.p0struct.ap_int(i64* align 512 %3, %struct.ap_int* %2)
  call fastcc void @onebyonecpy_hls.p0struct.ap_int(i64* align 512 %5, %struct.ap_int* %4)
  call fastcc void @onebyonecpy_hls.p0struct.ap_int(i64* align 512 %7, %struct.ap_int* %6)
  ret void
}

; Function Attrs: argmemonly noinline
define internal fastcc void @copy_out(%struct.ap_int*, i64* noalias readonly align 512, %struct.ap_int*, i64* noalias readonly align 512, %struct.ap_int*, i64* noalias readonly align 512, %struct.ap_int*, i64* noalias readonly align 512) unnamed_addr #2 {
entry:
  call fastcc void @onebyonecpy_hls.p0struct.ap_int.9(%struct.ap_int* %0, i64* align 512 %1)
  call fastcc void @onebyonecpy_hls.p0struct.ap_int.9(%struct.ap_int* %2, i64* align 512 %3)
  call fastcc void @onebyonecpy_hls.p0struct.ap_int.9(%struct.ap_int* %4, i64* align 512 %5)
  call fastcc void @onebyonecpy_hls.p0struct.ap_int.9(%struct.ap_int* %6, i64* align 512 %7)
  ret void
}

; Function Attrs: argmemonly noinline
define internal fastcc void @onebyonecpy_hls.p0struct.ap_int(i64* noalias align 512, %struct.ap_int* noalias readonly) unnamed_addr #3 {
entry:
  %2 = icmp eq i64* %0, null
  %3 = icmp eq %struct.ap_int* %1, null
  %4 = or i1 %2, %3
  br i1 %4, label %ret, label %copy

copy:                                             ; preds = %entry
  %5 = getelementptr inbounds %struct.ap_int, %struct.ap_int* %1, i64 0, i32 0, i32 0, i32 0
  %6 = load i64, i64* %5, align 8
  store i64 %6, i64* %0, align 512
  br label %ret

ret:                                              ; preds = %copy, %entry
  ret void
}

; Function Attrs: argmemonly noinline
define internal fastcc void @onebyonecpy_hls.p0struct.ap_int.9(%struct.ap_int* noalias, i64* noalias readonly align 512) unnamed_addr #3 {
entry:
  %2 = icmp eq %struct.ap_int* %0, null
  %3 = icmp eq i64* %1, null
  %4 = or i1 %2, %3
  br i1 %4, label %ret, label %copy

copy:                                             ; preds = %entry
  %5 = getelementptr inbounds %struct.ap_int, %struct.ap_int* %0, i64 0, i32 0, i32 0, i32 0
  %6 = load i64, i64* %1, align 512
  store i64 %6, i64* %5, align 8
  br label %ret

ret:                                              ; preds = %copy, %entry
  ret void
}

declare void @apatb_bandwidth_hw(i64*, i64*, i64*, i64*, i32, i32, i32)

define void @bandwidth_hw_stub_wrapper(i64*, i64*, i64*, i64*, i32, i32, i32) #4 {
entry:
  %7 = alloca %struct.ap_int
  %8 = alloca %struct.ap_int
  %9 = alloca %struct.ap_int
  %10 = alloca %struct.ap_int
  call void @copy_out(%struct.ap_int* %7, i64* %0, %struct.ap_int* %8, i64* %1, %struct.ap_int* %9, i64* %2, %struct.ap_int* %10, i64* %3)
  call void @bandwidth_hw_stub(%struct.ap_int* %7, %struct.ap_int* %8, %struct.ap_int* %9, %struct.ap_int* %10, i32 %4, i32 %5, i32 %6)
  call void @copy_in(%struct.ap_int* %7, i64* %0, %struct.ap_int* %8, i64* %1, %struct.ap_int* %9, i64* %2, %struct.ap_int* %10, i64* %3)
  ret void
}

declare void @bandwidth_hw_stub(%struct.ap_int*, %struct.ap_int*, %struct.ap_int*, %struct.ap_int*, i32, i32, i32)

attributes #0 = { noinline "fpga.wrapper.func"="wrapper" }
attributes #1 = { argmemonly noinline "fpga.wrapper.func"="copyin" }
attributes #2 = { argmemonly noinline "fpga.wrapper.func"="copyout" }
attributes #3 = { argmemonly noinline "fpga.wrapper.func"="onebyonecpy_hls" }
attributes #4 = { "fpga.wrapper.func"="stub" }

!llvm.dbg.cu = !{}
!llvm.ident = !{!0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0}
!llvm.module.flags = !{!1, !2, !3}
!blackbox_cfg = !{!4}

!0 = !{!"clang version 7.0.0 "}
!1 = !{i32 2, !"Dwarf Version", i32 4}
!2 = !{i32 2, !"Debug Info Version", i32 3}
!3 = !{i32 1, !"wchar_size", i32 4}
!4 = !{}
