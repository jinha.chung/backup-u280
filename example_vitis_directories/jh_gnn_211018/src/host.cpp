/**
* Copyright (C) 2019-2021 Xilinx, Inc
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/

// OpenCL utility layer include
#include <fcntl.h>
#include <fstream>
#include <iomanip>
#include <iosfwd>
#include <iostream>
#include <unistd.h>
#include <vector>

// MODIFICATIONS - includes for my code
#include <xrt/xrt_device.h>
#include <experimental/xrt_xclbin.h>
#include <xrt/xrt_bo.h>
#include <xrt/xrt_kernel.h>
#include <experimental/xrt_ip.h>

size_t max_buffer = 16 * 1024 * 1024;
size_t min_buffer = 4 * 1024;
size_t max_size = 128 * 1024 * 1024; // 128MB

// https://github.com/Xilinx/Vitis_Accel_Examples/blob/master/host_xrt/p2p_simple_xrt/src/host.cpp
void test_adder() {
    std::cout << "[TEST_ADDER] Starting. Entering test_adder()" << std::endl;

    // 1. get device and kernel ready
    std::cout << "[TEST_ADDER] Getting device and kernel ready" << std::endl;
    auto device = xrt::device(0);
    std::cout << "[TEST_ADDER] Checkpoint - xrt::device() complete" << std::endl;
    auto uuid = device.load_xclbin("/home/via/gnn_workspace/jh_gnn_211018/Hardware/my_xclbins/sample_and_gather_with_perm_checking_3055317720.xclbin");
    std::cout << "[TEST_ADDER] Checkpoint - device.load_xclbin() complete" << std::endl;
    auto krnl = xrt::kernel(device, uuid, "bandwidth");
    
    // 2. generate inputs
    std::cout << "[TEST_ADDER] Generating host-side inputs" << std::endl;
    uint64_t BUFFER_SIZE = 1024;
    // open SSD
    //std::string filename = "/mnt/jinha/ssd.txt";
    std::string filename = "/mnt/jinha/abcd.txt";
    int fd = open(filename.c_str(), O_RDWR | O_DIRECT);
    if (fd < 0) {
        std::cerr << "[TEST_ADDER] ERROR: open " << filename << " failed" << std::endl;
        exit(-1);
    }

    // 3. generate buffers for FPGA
    std::cout << "[TEST_ADDER] Generating FPGA-side buffers" << std::endl;
    auto input_col_buf = xrt::bo(device, BUFFER_SIZE * sizeof(int64_t), xrt::bo::flags::p2p, krnl.group_id(0));
    auto output_perm_buf = xrt::bo(device, BUFFER_SIZE * sizeof(int64_t), krnl.group_id(1));
    auto output_c_buf = xrt::bo(device, BUFFER_SIZE * sizeof(int64_t), krnl.group_id(2));
    // map buffer objects
    auto input_col_buf_map = input_col_buf.map<int64_t *>();
    auto output_perm_buf_map = output_perm_buf.map<int64_t *>();
    auto output_c_buf_map = output_c_buf.map<int64_t *>();
    // initialize values
    // read from SSD
    //if (pread(fd, (void *)input_col_buf_map, BUFFER_SIZE * sizeof(int64_t *), 0) <= 0) {
    if (pread(fd, (void *)input_col_buf_map, BUFFER_SIZE * sizeof(int64_t), 0) <= 0) {
        std::cerr << "[TEST_ADDER] ERROR: pread" << std::endl;
        exit(-1);
    }

    // 4. run kernel
    std::cout << "[TEST_ADDER] Running kernel" << std::endl;
    // krnl(in_col, out_perm, out_c, num_neighbor, rc, R)
    int num_neighbor = 32;
    int rc = 128;
    auto run = krnl(input_col_buf, output_perm_buf, output_c_buf, num_neighbor, rc, 0);
    run.wait();

    // sync
    output_c_buf.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
    output_perm_buf.sync(XCL_BO_SYNC_BO_FROM_DEVICE);

    // 5. check output
    std::cout << "[TEST_ADDER] Checking outputs" << std::endl;
    uint64_t total_num_wrong = 0;
    for (uint64_t i = 0; i < num_neighbor; ++i) {
        if (input_col_buf_map[output_perm_buf_map[i]] != output_c_buf_map[i]) {
            std::cout << "[TEST_ADDER] WRONG @ INDEX " << i << ": "
                      << "should be " << input_col_buf_map[output_perm_buf_map[i]]
                      << ", but got " << output_c_buf_map[i] << " instead"
                      << std::endl;
            ++total_num_wrong;
        }
        if (1) {
        //else if (i % 10 == 0) {
            std::cout << "[TEST_ADDER] INFO @ INDEX " << i << ": "
                      << "output_perm[" << i << "] = " << output_perm_buf_map[i]
                      << ", input_col[" << output_perm_buf_map[i] << "] = "
                      << ((uint64_t *)input_col_buf_map)[((uint64_t *)output_perm_buf_map)[i]]
                      << ", and output_c[" << i << "] = " << ((uint64_t *)output_c_buf_map)[i] << " as well"
		      << std::endl;
        }
    }
    std::cout << "[TEST_ADDER] Total of " << total_num_wrong << " wrong answers" << std::endl;

    // 6. finished
    std::cout << "[TEST_ADDER] Finished. Leaving test_adder()" << std::endl;
}

int main(int argc, char** argv) {

    test_adder();

    return 0;
}

