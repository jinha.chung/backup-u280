#!/bin/bash

dir=w
i=0
SSD=$(lsblk | grep 3.5T | awk '{ print $1 }') 
while [ $? -eq 0 ]; do
	echo iteration $i
	((i=i+1))
	if [ "$dir" == "r" ]; then
		dir=w
	else
		dir=r
	fi
	./bytecopy_async.exe -p /dev/$SSD -$dir -k ./bytecopy.xclbin
done
