1. To run Hello world Kernel:
   ./validate.exe verify.xclbin


2. To run Bandwidth Kernel:
   ./kernel_bw.exe bandwidth.xclbin


3. To run regular/sync bytecopy kernel:
   ./run_bytecopy.sh

4. To run aync_bytecopy kernel:
   ./run_aync_bytecopy.sh
