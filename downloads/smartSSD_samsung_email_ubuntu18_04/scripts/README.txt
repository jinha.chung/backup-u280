 *************************************************************************
#    ____  ____
#   /   /\/   /
#  /___/  \  /
#  \   \   \/    © Copyright 2019-20 Xilinx, Inc. All rights reserved.
#   \   \        This file contains confidential and proprietary
#   /   /        information of Xilinx, Inc. and is protected under U.S.
#  /___/   /\    and international copyright and other intellectual
#  \   \  /  \   property laws.
#   \___\/\___\
#
#
# *************************************************************************
#
# Disclaimer:
#
#       This disclaimer is not a license and does not grant any rights to
#       the materials distributed herewith. Except as otherwise provided in
#       a valid license issued to you by Xilinx, and to the maximum extent
#       permitted by applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE
#       "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL
#       WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
#       INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
#       NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
#       (2) Xilinx shall not be liable (whether in contract or tort,
#       including negligence, or under any other theory of liability) for
#       any loss or damage of any kind or nature related to, arising under
#       or in connection with these materials, including for any direct, or
#       any indirect, special, incidental, or consequential loss or damage
#       (including loss of data, profits, goodwill, or any type of loss or
#       damage suffered as a result of any action brought by a third party)
#       even if such damage or loss was reasonably foreseeable or Xilinx
#       had been advised of the possibility of the same.
#
# Critical Applications:
#
#       Xilinx products are not designed or intended to be fail-safe, or
#       for use in any application requiring fail-safe performance, such as
#       life-support or safety devices or systems, Class III medical
#       devices, nuclear facilities, applications related to the deployment
#       of airbags, or any other applications that could lead to death,
#       personal injury, or severe property or environmental damage
#       (individually and collectively, "Critical Applications"). Customer
#       assumes the sole risk and liability of any use of Xilinx products
#       in Critical Applications, subject only to applicable laws and
#       regulations governing limitations on product liability.
#
# THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS
# FILE AT ALL TIMES.
#
# *************************************************************************

==========
CONTENTS:
=========
Scripts directory contains below files
1.run_all.sh                              
2.plot_graph_clk_scaling.py               
3.rwmem
4.datalog folder
5.README.txt
6.clk_thrt_set_limit.sh
7.clk_thrt_en.sh

Setup:
------
OS Release: Ubuntu 16.04.4 LTS
samsung smart ssd connected in PCIe slot.

Note:
-----
script was tested on kernel version: 4.4.0-116-generic


Pre-requisites:
---------------
1.python 3.5.2
2.matplotlib 3.0.3
3.xrt 2.3


============
How to run:
============
1. Execute the below command to visualize power throttling.
   $./run_all.sh

2. To Enable or Disable we can use clk_thrt_em.sh script, we can choose either 0 or 1 to disbale or enable clock throtlling respectively.
   $./clk_thrt_en.sh

3. To Change Power and Temperature Thresholds for clock throttling we can use clk_thrt_set_limit.sh script. In this script we can also choose clock throttling type as well.
   $./clk_thrt_set_limit.sh <temp.limit> <power_limit>
   Here we have to choose clock throlling type every time and temperature range is (0-100 degress) , Power Range is (0-25Watts)



Note:
-----
if you are Logging in to the host via ssh, use  ssh -X "root@Host_IP_Address" to display the graphs gui.


About run_all.sh:
----------------
plot live graphs for kernel clock frequency scaling.
script plots below graphs againt time
1.Raw Board power(W) vs time (1 unit = 75 milli sec)
2.FPGA Temperature(C) vs time (1 unit = 75 milli sec)
3.Effective Frequency(MHz) vs time(1 unit = 75 milli sec)
