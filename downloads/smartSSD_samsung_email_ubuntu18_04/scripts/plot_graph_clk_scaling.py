# *************************************************************************
#    ____  ____
#   /   /\/   /
#  /___/  \  /
#  \   \   \/    © Copyright 2019-20 Xilinx, Inc. All rights reserved.
#   \   \        This file contains confidential and proprietary
#   /   /        information of Xilinx, Inc. and is protected under U.S.
#  /___/   /\    and international copyright and other intellectual
#  \   \  /  \   property laws.
#   \___\/\___\
#
#
# *************************************************************************
#
# Disclaimer:
#
#       This disclaimer is not a license and does not grant any rights to
#       the materials distributed herewith. Except as otherwise provided in
#       a valid license issued to you by Xilinx, and to the maximum extent
#       permitted by applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE
#       "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL
#       WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
#       INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
#       NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
#       (2) Xilinx shall not be liable (whether in contract or tort,
#       including negligence, or under any other theory of liability) for
#       any loss or damage of any kind or nature related to, arising under
#       or in connection with these materials, including for any direct, or
#       any indirect, special, incidental, or consequential loss or damage
#       (including loss of data, profits, goodwill, or any type of loss or
#       damage suffered as a result of any action brought by a third party)
#       even if such damage or loss was reasonably foreseeable or Xilinx
#       had been advised of the possibility of the same.
#
# Critical Applications:
#
#       Xilinx products are not designed or intended to be fail-safe, or
#       for use in any application requiring fail-safe performance, such as
#       life-support or safety devices or systems, Class III medical
#       devices, nuclear facilities, applications related to the deployment
#       of airbags, or any other applications that could lead to death,
#       personal injury, or severe property or environmental damage
#       (individually and collectively, "Critical Applications"). Customer
#       assumes the sole risk and liability of any use of Xilinx products
#       in Critical Applications, subject only to applicable laws and
#       regulations governing limitations on product liability.
#
# THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS
# FILE AT ALL TIMES.
#
# *************************************************************************


"""Module to plot live graphs of FPGA clock throttling."""
from matplotlib import style
import matplotlib.pyplot as plt
import matplotlib.animation as animation


XAXIS_MAX_TIME = 100000         # unit = XAXIS_MAXTIME * 0.075 sec.
FREQUENCY_LOG = "./data_log/krnl_freq_data"
FPGA_TEMP_LOG = "./data_log/fpga_temp_data"
POWER_LOG = "./data_log/board_power_data"


class ClockThrottling:
    """Clock frequency scaling"""

    def __init__(self):
        """init method"""

        style.use('fivethirtyeight')
        self.fig = plt.figure(figsize=(17,10))

        self.ax1 = self.fig.add_subplot(2, 2, 1)
        self.ax1.set_title("Raw Board Power")

        self.ax1.set_xlabel('Time in units, 1 unit = 75milli sec')
        self.ax1.set_ylabel('Raw Power (W)')
        self.ax3 = self.fig.add_subplot(2, 1, 2)
        self.ax3.set_title("Effective Kernel Frequency")
        self.ax3.set_xlabel('Time in units, 1 unit = 75milli sec')
        self.ax3.set_ylabel('Effective Frequency (MHz)')

        self.ax4 = self.fig.add_subplot(2, 2, 2)
        self.ax4.set_title("FPGA Temperature")
        self.ax4.set_xlabel("Time in units, 1 unit = 75milli sec")
        self.ax4.set_ylabel("FPGA TEMPERATURE (C)")


    def get_power_line(self):
        """yields a line from POWER_LOG"""

        with open(POWER_LOG, "r") as handle:
            for line in handle:
                yield line


    def get_temperature_line(self):
        """yield a line from FPGA_TEMP_LOG"""
        with open(FPGA_TEMP_LOG, "r") as handle:
            for line in handle:
                yield line


    def get_frequency_line(self):
        """yield a line from FREQUENCY_LOG """
        with open(FREQUENCY_LOG, "r") as handle:
            for line in handle:
                yield line


    def get_x_coordinate(self):
        """yields a value to be used in x-axis of graph."""
        for i in range(XAXIS_MAX_TIME):
            yield i


    def freq_animate(self, i):
        """Callable to be called at each frame"""
        xs = []
        ys = []
        n = self.get_x_coordinate()
        bin_value = ""
        line_generator = self.get_frequency_line()
        while True:
            try:
                #Iterate over 8 register values at a time 
                for i in range(8):
                    line = line_generator.__next__()
                    n_val = n.__next__()
                    try:
                        _, register_value = line.split("=")
                        bin_value = bin_value + bin(int(register_value.strip("\n"), 16))[2:]
                    except:
                        pass
                power_mhz = (float(bin_value.count('1'))/256)*300
                bin_value = ""
                try:
                    ys.append(float(power_mhz))
                    xs.append(n_val)
                except:
                    pass
            except StopIteration:
                break
        self.ax3.plot(xs, ys)


    def board_temp_animate(self, j):
        """Callable to be called at each frame"""
        xs = []
        ys = []
        n = self.get_x_coordinate()
        bin_value = ""
        line_generator = self.get_temperature_line()
        while True:
            try:
                line = line_generator.__next__()
                n_val = n.__next__()
                try:
                    _, register_value = line.split("=")
                    bin_value = (int(register_value.strip(), 16))
                except:
                    pass
                try:
                    ys.append(float(bin_value))
                    xs.append(n_val)
                except:
                    pass
                bin_value = ""
            except StopIteration:
                break
        self.ax4.plot(xs, ys)


    def board_power_animate(self, j):
        """Callable to be called at each frame"""
        xs = []
        ys = []
        n = self.get_x_coordinate()
        bin_value = ""
        line_generator = self.get_power_line()
        while True:
            try:
                line = line_generator.__next__()
                n_val = n.__next__()
                try:
                    _, register_value = line.split("=")
                    bin_value = (int(register_value.strip(), 16))
                except:
                    pass
                try:
                    ys.append(float(bin_value))
                    xs.append(n_val)
                except:
                    pass
                bin_value = ""
            except StopIteration:
                break
        self.ax1.plot(xs, ys)


if __name__ == '__main__':
    clck_freq = ClockThrottling()
    ANNIMATION_1 = animation.FuncAnimation(clck_freq.fig, clck_freq.board_power_animate,
                                           interval=1000)
    ANNIMATION_2 = animation.FuncAnimation(clck_freq.fig, clck_freq.freq_animate, interval=1000)
    ANNIMATION_3 = animation.FuncAnimation(clck_freq.fig, clck_freq.board_temp_animate,
                                           interval=1000)
    plt.show()
