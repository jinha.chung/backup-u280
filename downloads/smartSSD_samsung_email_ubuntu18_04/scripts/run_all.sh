# *************************************************************************
#    ____  ____
#   /   /\/   /
#  /___/  \  /
#  \   \   \/    © Copyright 2019-20 Xilinx, Inc. All rights reserved.
#   \   \        This file contains confidential and proprietary
#   /   /        information of Xilinx, Inc. and is protected under U.S.
#  /___/   /\    and international copyright and other intellectual
#  \   \  /  \   property laws.
#   \___\/\___\
#
#
# *************************************************************************
#
# Disclaimer:
#
#       This disclaimer is not a license and does not grant any rights to
#       the materials distributed herewith. Except as otherwise provided in
#       a valid license issued to you by Xilinx, and to the maximum extent
#       permitted by applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE
#       "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL
#       WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
#       INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
#       NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
#       (2) Xilinx shall not be liable (whether in contract or tort,
#       including negligence, or under any other theory of liability) for
#       any loss or damage of any kind or nature related to, arising under
#       or in connection with these materials, including for any direct, or
#       any indirect, special, incidental, or consequential loss or damage
#       (including loss of data, profits, goodwill, or any type of loss or
#       damage suffered as a result of any action brought by a third party)
#       even if such damage or loss was reasonably foreseeable or Xilinx
#       had been advised of the possibility of the same.
#
# Critical Applications:
#
#       Xilinx products are not designed or intended to be fail-safe, or
#       for use in any application requiring fail-safe performance, such as
#       life-support or safety devices or systems, Class III medical
#       devices, nuclear facilities, applications related to the deployment
#       of airbags, or any other applications that could lead to death,
#       personal injury, or severe property or environmental damage
#       (individually and collectively, "Critical Applications"). Customer
#       assumes the sole risk and liability of any use of Xilinx products
#       in Critical Applications, subject only to applicable laws and
#       regulations governing limitations on product liability.
#
# THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS
# FILE AT ALL TIMES.
#
# *************************************************************************

#! /bin/bash
: '
	Memory at 96000000 (32-bit, non-prefetchable) [size=32M]
	Memory at 98000000 (32-bit, non-prefetchable) [size=128K]
'
rm -rf data_log/*

bus_addr=`lspci | grep -i xilinx | grep 6987 | awk '{print $1}'`
bar_addr=`lspci -v -s $bus_addr | grep size=32M | awk '{print $3}'`
hex_bar_addr="0x$bar_addr"
python3 plot_graph_clk_scaling.py &
#trap ctrl_c INT
end=$((SECONDS+5000))
echo "press ctrl+C to stop"


signal_trap()
{
echo "Caught Signal"
id=`pidof python3 plot_graph_clk_scaling.py`
if [[ -z $id ]]; then
exit
else 
    kill -9 $id
fi
exit
}

trap signal_trap SIGINT
trap signal_trap SIGTERM

while [ $SECONDS -lt $end ]; do
	
	./rwmem $(($hex_bar_addr + 0x5300C)) >> data_log/fpga_temp_data;
	./rwmem $(($hex_bar_addr + 0x5301C)) >> data_log/board_power_data;
	./rwmem $(($hex_bar_addr + 0x53100)) >> data_log/krnl_freq_data;
	./rwmem $(($hex_bar_addr + 0x53104)) >> data_log/krnl_freq_data;
	./rwmem $(($hex_bar_addr + 0x53108)) >> data_log/krnl_freq_data;
	./rwmem $(($hex_bar_addr + 0x5310C)) >> data_log/krnl_freq_data;
	./rwmem $(($hex_bar_addr + 0x53110)) >> data_log/krnl_freq_data;
	./rwmem $(($hex_bar_addr + 0x53114)) >> data_log/krnl_freq_data;
	./rwmem $(($hex_bar_addr + 0x53118)) >> data_log/krnl_freq_data;
	./rwmem $(($hex_bar_addr + 0x5311C)) >> data_log/krnl_freq_data;
	sleep 0.075
:
done



