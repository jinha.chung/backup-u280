#include "sample_cpu.h"

#include "utils.h"

int num_unroll = 16;

int col_fd = -1;
int value_fd = -1;
int embedding_fd = -1;
cl_command_queue command_queue;
cl_context context;
const char* kernel_name = "adder";
cl_program program;
cl_mem buffer_col, buffer_value, buffer_perm, buffer_output_col, buffer_output_value;
int64_t *perm_ptr, *col_ptr, *value_ptr, *output_col_ptr, *output_value_ptr;

std::vector<unsigned char> readBinary(const std::string &fileName) {
  std::ifstream file(fileName, std::ios::binary | std::ios::ate);
  if (file) {
    file.seekg(0, std::ios::end);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);
    std::vector<unsigned char> buffer(size);
    file.read((char *)buffer.data(), size);
    return buffer;
  } else {
    return std::vector<unsigned char>(0);
  }
}

void *pthread_pread(void *_arg){
  struct pread_arg *arg = (struct pread_arg *)_arg;
  pread(arg->fd, arg->buf, arg->count, arg->pos);
  // pread(arg->fd, arg->buf, 1 << 18, arg->pos);
}

void *pthread_pread_unroll(void *_arg){
  struct pread_arg *arg = (struct pread_arg *)_arg;
  for (int unroll = 0; unroll<num_unroll; unroll++){
    if (arg[unroll].fd == -1)
      break;
    pread(arg[unroll].fd, arg[unroll].buf, arg[unroll].count, arg[unroll].pos);
  }
}

// Returns `rowptr`, `col`, `n_id`, `e_id`,
std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_cpu(torch::Tensor rowptr, torch::Tensor col, torch::Tensor rowcount, torch::Tensor idx, int64_t num_neighbors, bool replace) {
  std::chrono::system_clock::time_point start, end;
  CHECK_CPU(rowptr);
  CHECK_CPU(col);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto col_data = col.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();

  auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> e_ids;
  std::vector<std::vector<double>> g_perf(3);

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }

  if (num_neighbors < 0) { // No sampling ======================================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      for (int64_t j = 0; j < rowcount_data[r]; j++) {
        e = rowptr_data[r] + j;
        c = col_data[e];

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
        e_ids.push_back(e);
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  else if (replace) { // Sample with replacement ===============================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      for (int64_t j = 0; j < num_neighbors; j++) {
        e = rowptr_data[r] + rand() % rowcount_data[r];
        c = col_data[e];

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        c = n_id_map[c];
        if (std::find(cols.begin() + offset, cols.end(), c) == cols.end()) {
          cols.push_back(c);
          e_ids.push_back(e);
        }
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }

  } else { // Sample without replacement via Robert Floyd algorithm ============

    int64_t r, c, e, rc, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];
      rc = rowcount_data[r];

      start = std::chrono::high_resolution_clock::now();
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[0].push_back((double) std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count());
      
      std::vector<int64_t> tmp(num_neighbors);
      start = std::chrono::high_resolution_clock::now();
      int cnt = 0;
      for (const int64_t &p : perm) {
        e = rowptr_data[r] + p;
        c = col_data[e];
      
        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }
        cols.push_back(n_id_map[c]);
        e_ids.push_back(e);
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[1].push_back((double) std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count());
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  // std::cout << "perm:" << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size()  << "ns" << std::endl;
  // std::cout << "col reading & n_id map:" << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size()  << "ns" << std::endl;
  // std::cout << "n_id map:" << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size() << "ns" << std::endl;
  
  int64_t n_len = n_ids.size(), e_len = cols.size();
  col = torch::from_blob(cols.data(), {e_len}, col.options()).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, col.options()).clone();
  auto e_id = torch::from_blob(e_ids.data(), {e_len}, col.options()).clone();

  return std::make_tuple(out_rowptr, col, n_id, e_id);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_stdio_cpu(torch::Tensor rowptr, torch::Tensor rowcount, torch::Tensor idx, int64_t num_neighbors, bool replace, std::string path) {
  std::chrono::system_clock::time_point start, end;
  // start = std::chrono::high_resolution_clock::now();
  
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();
  auto out_rowptr = torch::empty(idx.size(0) + 1, options); // Need to rowptr.options() and col.options()
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
  // start = std::chrono::high_resolution_clock::now();
  FILE *file_col = fopen((path+"col").c_str(), "r");
  FILE *file_val = fopen((path+"value").c_str(), "r");

  // void *temp_col_buffer, *temp_val_buffer;
  size_t sectorsize = 4096*64UL;
  // posix_memalign(&(temp_col_buffer), 512, aligned_size);
  // posix_memalign(&(temp_val_buffer), 512, aligned_size);
  int64_t numentry_per_sector = sectorsize / sizeof(int64_t);
  std::vector<int64_t> temp_col_buffer(numentry_per_sector), temp_val_buffer(numentry_per_sector);
  // int64_t* temp_col_buffer = new int64_t[numentry_per_sector];
  // int64_t* temp_val_buffer = new int64_t[numentry_per_sector];
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << "variable setup:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
  int64_t col_data_e;
  int64_t LBA_offset, data_offset;

  if (num_neighbors < 0) { // No sampling ======================================
    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      int64_t prev = -1;
      for (int64_t j = 0; j < rowcount_data[r]; j++) {
        e = rowptr_data[r] + j;
        // fetch col data
        LBA_offset = e / numentry_per_sector;
        data_offset = e % numentry_per_sector;
        
        if (LBA_offset == prev)
        {
          // get it from buffer
          col_data_e = temp_col_buffer[data_offset];
          value_e_ids.push_back(temp_val_buffer[data_offset]);
        }
        else
        {
          // col
          fseek(file_col, LBA_offset * sectorsize, SEEK_SET);
          fread(temp_col_buffer.data(), sectorsize, 1, file_col);
          col_data_e = temp_col_buffer[data_offset];
          // value
          fseek(file_val, LBA_offset * sectorsize, SEEK_SET);
          fread(temp_val_buffer.data(), sectorsize, 1, file_val);
          value_e_ids.push_back(temp_val_buffer[data_offset]);
          prev = LBA_offset;
        }
        
        c = col_data_e;

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  else if (replace) { // Sample with replacement ===============================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];
      
      int64_t prev = -1;
      for (int64_t j = 0; j < num_neighbors; j++) {
        e = rowptr_data[r] + rand() % rowcount_data[r];
        // fetch col data
        LBA_offset = e / numentry_per_sector;
        data_offset = e % numentry_per_sector;
        if (LBA_offset == prev)
        {
          // get it from buffer
          col_data_e = temp_col_buffer[data_offset];
        }
        else
        {
          // col
          fseek(file_col, LBA_offset * sectorsize, SEEK_SET);
          fread(temp_col_buffer.data(), sectorsize, 1, file_col);
          col_data_e = temp_col_buffer[data_offset];
          prev = LBA_offset;
        }
        c = col_data_e;


        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        c = n_id_map[c];
        if (std::find(cols.begin() + offset, cols.end(), c) == cols.end()) {
          cols.push_back(c);
          if (LBA_offset == prev)
          {
            value_e_ids.push_back(temp_val_buffer[data_offset]);
          }
          {
            fseek(file_val, LBA_offset * sectorsize, SEEK_SET);
            fread(temp_val_buffer.data(), sectorsize, 1, file_val);
            value_e_ids.push_back(temp_val_buffer[data_offset]);
          }
        }
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }

  } else { // Sample without replacement via Robert Floyd algorithm ============
    int64_t r, c, e, rc, offset = 0;
    size_t total_cnt_cached = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      // start = std::chrono::high_resolution_clock::now();
      r = idx_data[i];
      rc = rowcount_data[r];

      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int64_t prev = -1;
      size_t cnt_cached = 0;
      start = std::chrono::high_resolution_clock::now();
      if(fseek(file_col, rowptr_data[r] * sizeof(int64_t), SEEK_SET))
      {
        printf("fseek error\n");
        exit(1);
      }
      if(!fread(temp_col_buffer.data(), rc * sizeof(int64_t), 1, file_col))
      {
        printf("fread error\n");
        exit(1);
      }
      if(fseek(file_val, rowptr_data[r] * sizeof(int64_t), SEEK_SET))
      {
        printf("fseek error\n");
        exit(1);
      }
      if(!fread(temp_val_buffer.data(), rc * sizeof(int64_t), 1, file_val))
      {
        printf("fread error\n");
        exit(1);
      }
      end = std::chrono::high_resolution_clock::now();
      // std::cout << "col val reading:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
      for (const int64_t &p : perm) {
        // e = rowptr_data[r] + p;
        // fetch col data
        c = temp_col_buffer[p];
        value_e_ids.push_back(temp_val_buffer[p]);
        // LBA_offset = e / numentry_per_sector;
        // data_offset = e % numentry_per_sector;
        // if (LBA_offset == prev)
        // {
        //   // get it from buffer
        //   // std::cout << LBA_offset << std::endl;
        //   col_data_e = temp_col_buffer[data_offset];
        //   value_e_ids.push_back(temp_val_buffer[data_offset]);
        //   cnt_cached++;
        // }
        // else
        // {
        //   // col
        //   if(fseek(file_col, LBA_offset * sectorsize, SEEK_SET))
        //   {
        //     printf("fseek error\n");
        //     exit(1);
        //   }
        //   if(!fread(temp_col_buffer.data(), sectorsize, 1, file_col))
        //   {
        //     printf("fread error\n");
        //     exit(1);
        //   }
        //   col_data_e = temp_col_buffer[data_offset];
        //   // value
        //   if(fseek(file_val, LBA_offset * sectorsize, SEEK_SET))
        //   {
        //     printf("fseek error\n");
        //     exit(1);
        //   }

        //   if(!fread(temp_val_buffer.data(), sectorsize, 1, file_val))
        //   {
        //     printf("fread error\n");
        //     exit(1);
        //   }
        //   value_e_ids.push_back(temp_val_buffer[data_offset]);
        //   prev = LBA_offset;
        // }
        // c = col_data_e;
        // std::cout << col_data_e << std::endl;
        // end = std::chrono::high_resolution_clock::now();
        // std::cout << "col_data_e:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
        
        start = std::chrono::high_resolution_clock::now();
        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
        end = std::chrono::high_resolution_clock::now();
        // std::cout << "col_data_e else:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
      }
      // total_cnt_cached += cnt_cached;
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
    // std::cout << "how many cached?:" << (double)total_cnt_cached / (idx.size(0) * num_neighbors) << std::endl;
  }

  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  // delete[] temp_col_buffer;
  // delete[] temp_val_buffer;
  fclose(file_col);
  fclose(file_val);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << getpid() << " spdk_sample total:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
  return std::make_tuple(out_rowptr, col, n_id, val);
}

// Returns `rowptr`, `col`, `n_id`, `e_id`,
std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_mmap_cpu(torch::Tensor rowptr, torch::Tensor col, torch::Tensor value, torch::Tensor rowcount,
               torch::Tensor idx, int64_t num_neighbors, bool replace) {
  std::chrono::system_clock::time_point start, end;
  // start = std::chrono::high_resolution_clock::now();
  CHECK_CPU(rowptr);
  CHECK_CPU(col);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto col_data = col.data_ptr<int64_t>();
  auto val_data = value.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();

  auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;
  int64_t buffersize = 4096UL;
  int64_t numentry_per_buffer = buffersize / sizeof(int64_t);
  std::vector<int64_t> temp_col_buffer(numentry_per_buffer), temp_val_buffer(numentry_per_buffer);

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  // std::vector<int64_t> e_ids;
  std::vector<int64_t> value_e_ids;
  int64_t col_data_e = 0;
  int64_t LBA_offset, data_offset;

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }

  if (num_neighbors < 0) { // No sampling ======================================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      int64_t prev = -1;
      for (int64_t j = 0; j < rowcount_data[r]; j++) {
        e = rowptr_data[r] + j;
        LBA_offset = e / numentry_per_buffer;
        data_offset = e % numentry_per_buffer;
        if (LBA_offset == prev)
        {
          // get it from buffer
          col_data_e = temp_col_buffer[data_offset];
          value_e_ids.push_back(temp_val_buffer[data_offset]);
        }
        else
        {
          // col
          memcpy(temp_col_buffer.data(), col_data + LBA_offset * numentry_per_buffer, buffersize);
          col_data_e = temp_col_buffer[data_offset];
          // val
          memcpy(temp_val_buffer.data(), val_data + LBA_offset * numentry_per_buffer, buffersize);
          value_e_ids.push_back(temp_val_buffer[data_offset]);
          prev = LBA_offset;
        }
        c = col_data_e;
        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  else if (replace) { // Sample with replacement ===============================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      int64_t prev = -1;
      for (int64_t j = 0; j < num_neighbors; j++) {
        e = rowptr_data[r] + rand() % rowcount_data[r];
        LBA_offset = e / numentry_per_buffer;
        data_offset = e % numentry_per_buffer;
        if (LBA_offset == prev)
        {
          // get it from buffer
          col_data_e = temp_col_buffer[data_offset];
        }
        else
        {
          // col
          memcpy(temp_col_buffer.data(), col_data + LBA_offset * numentry_per_buffer, buffersize);
          col_data_e = temp_col_buffer[data_offset];
          prev = LBA_offset;
        }
        c = col_data_e;

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        c = n_id_map[c];
        if (std::find(cols.begin() + offset, cols.end(), c) == cols.end()) {
          cols.push_back(c);
          if (LBA_offset == prev)
          {
            // get it from buffer
            value_e_ids.push_back(temp_val_buffer[data_offset]);
          }
          else
          {
            // val
            memcpy(temp_val_buffer.data(), val_data + LBA_offset * numentry_per_buffer, buffersize);
            value_e_ids.push_back(temp_val_buffer[data_offset]);
          }
        }
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }

  } else { // Sample without replacement via Robert Floyd algorithm ============

    int64_t r, c, e, rc, offset = 0;
    size_t total_cnt_cached=0;
    size_t cnt_cached=0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];
      rc = rowcount_data[r];

      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }

      cnt_cached=0;
      for (const int64_t &p : perm) {
        e = rowptr_data[r] + p;
        c = col_data[e];
        value_e_ids.push_back(val_data[e]);

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
      }
      total_cnt_cached += cnt_cached;
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
    // std::cout << "how many cached?:" << (double)total_cnt_cached / (idx.size(0) * num_neighbors) << std::endl;
  }

  int64_t n_len = n_ids.size(), e_len = cols.size();
  col = torch::from_blob(cols.data(), {e_len}, col.options()).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, col.options()).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, value.options()).clone();
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << getpid() << " sample total:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;

  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_pio_cpu(torch::Tensor rowptr, torch::Tensor rowcount, torch::Tensor idx, int64_t num_neighbors, bool replace, std::string path) {
  std::chrono::system_clock::time_point start, end;
  // start = std::chrono::high_resolution_clock::now();
  
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();
  auto out_rowptr = torch::empty(idx.size(0) + 1, options); // Need to rowptr.options() and col.options()
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;
  std::vector<std::vector<double>> g_perf(6);

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
  // start = std::chrono::high_resolution_clock::now();
  int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
  if((fd_col < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }
  int fd_val = open((path+"value").c_str(), O_RDONLY | O_DIRECT);
  if((fd_val < 0))
  {
    std::cout << "can't open " << path+"value" << std::endl;
    exit(1);
  }
  void *temp_col_buffer, *temp_val_buffer;
  size_t aligned_size = BUFFER_SIZE;
  posix_memalign(&(temp_col_buffer), 4096, aligned_size);
  posix_memalign(&(temp_val_buffer), 4096, aligned_size);
  int64_t numentry_per_sector = aligned_size / sizeof(int64_t);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << "variable setup:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
  int64_t col_data_e;
  int64_t LBA_offset, data_offset;

  if (num_neighbors < 0) { // No sampling ======================================
    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      // torch::Tensor temp_col_buffer, temp_val_buffer;
      int64_t prev = -1;
      for (int64_t j = 0; j < rowcount_data[r]; j++) {
        e = rowptr_data[r] + j;
        // fetch col data
        LBA_offset = e / numentry_per_sector;
        data_offset = e % numentry_per_sector;
        
        if (LBA_offset == prev)
        {
          // get it from buffer
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
          value_e_ids.push_back(((int64_t*)temp_val_buffer)[data_offset]);
        }
        else
        {
          // col
          lseek(fd_col, LBA_offset * aligned_size, SEEK_SET);
          read(fd_col, temp_col_buffer, aligned_size);
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
          // value
          lseek(fd_val, LBA_offset * aligned_size, SEEK_SET);
          read(fd_val, temp_val_buffer, aligned_size);
          value_e_ids.push_back(((int64_t*)temp_val_buffer)[data_offset]);
          prev = LBA_offset;
        }
        
        c = col_data_e;

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  else if (replace) { // Sample with replacement ===============================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];
      
      // torch::Tensor temp_col_buffer, temp_val_buffer;
      int64_t prev = -1;
      for (int64_t j = 0; j < num_neighbors; j++) {
        e = rowptr_data[r] + rand() % rowcount_data[r];
        // fetch col data
        LBA_offset = e / numentry_per_sector;
        data_offset = e % numentry_per_sector;
        if (LBA_offset == prev)
        {
          // get it from buffer
          // col_data_e = temp_col_buffer[data_offset].item<int64_t>();
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
        }
        else
        {
          // col
          lseek(fd_col, LBA_offset * aligned_size, SEEK_SET);
          read(fd_col, temp_col_buffer, aligned_size);
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
          // target_LBA = col_LBA_start + LBA_offset;
          // temp_col_buffer = std::get<0>(read_tensor(target_LBA, target_LBA, 0));
          // col_data_e = temp_col_buffer[data_offset].item<int64_t>();
          prev = LBA_offset;
        }
        c = col_data_e;


        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        c = n_id_map[c];
        if (std::find(cols.begin() + offset, cols.end(), c) == cols.end()) {
          cols.push_back(c);
          if (LBA_offset == prev)
          {
            // value_e_ids.push_back(temp_val_buffer[data_offset].item<int64_t>());
            value_e_ids.push_back(((int64_t*)temp_val_buffer)[data_offset]);
          }
          {
            lseek(fd_val, LBA_offset * aligned_size, SEEK_SET);
            read(fd_val, temp_val_buffer, aligned_size);
            value_e_ids.push_back(((int64_t*)temp_val_buffer)[data_offset]);
            // target_LBA = val_LBA_start + LBA_offset;
            // temp_val_buffer = std::get<0>(read_tensor(target_LBA, target_LBA, 0));
            // value_e_ids.push_back(temp_val_buffer[data_offset].item<int64_t>());
          }
        }
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }

  } else { // Sample without replacement via Robert Floyd algorithm ============
    int64_t r, c, e, rc, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      // start = std::chrono::high_resolution_clock::now();
      r = idx_data[i];
      rc = rowcount_data[r];

      start = std::chrono::high_resolution_clock::now();
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[0].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

      LBA_offset = rowptr_data[r] * sizeof(int64_t) / 512;
      data_offset = rowptr_data[r] * sizeof(int64_t) % 512;
      int64_t transfer_size_bytes = ((data_offset + rc * sizeof(int64_t) + 512) >> 9 ) << 9;
      // size_t transfer_size_bytes = aligned_size;
      start = std::chrono::high_resolution_clock::now();
      pread(fd_col, temp_col_buffer, transfer_size_bytes, LBA_offset * 512);
      end = std::chrono::high_resolution_clock::now();
      g_perf[1].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
      g_perf[2].push_back(transfer_size_bytes * 1000000/ ((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() * 1024 * 1024 * 1024));
      
      start = std::chrono::high_resolution_clock::now();
      pread(fd_val, temp_val_buffer, transfer_size_bytes, LBA_offset * 512);
      end = std::chrono::high_resolution_clock::now();
      g_perf[3].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
      g_perf[4].push_back(transfer_size_bytes * 1000000 / ((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() * 1024 * 1024 * 1024));

      int64_t prev = -1;
      std::vector<int64_t> tmp(num_neighbors);
      start = std::chrono::high_resolution_clock::now();

      for (const int64_t &p : perm) {
        c = ((int64_t*)temp_col_buffer)[data_offset / sizeof(int64_t) + p];
        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }
        cols.push_back(n_id_map[c]);
        value_e_ids.push_back(((int64_t*)temp_val_buffer)[data_offset / sizeof(int64_t) + p]);
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[5].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }
  // std::cout << "perm:" << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size()  << "us" << std::endl;
  // std::cout << "col read time(ssd->dram):" << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size()  << "us" << std::endl;
  // std::cout << "col read BW (ssd->dram):" << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size()  << "GB/s" << std::endl;
  // std::cout << "value read time (ssd->dram):" << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size()  << "us" << std::endl;
  // std::cout << "value read BW (ssd->dram):" << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size()  << "GB/s" << std::endl;
  // std::cout << "col & value gather & n_id_map:" << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) / g_perf[5].size()  << "us" << std::endl;
  // std::cout << "n_id map:" << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size() << "ms" << std::endl;

  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();


  free(temp_col_buffer);
  free(temp_val_buffer);
  close(fd_col);
  close(fd_val);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << getpid() << " spdk_sample total:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_pio_blk_cpu(torch::Tensor rowptr, torch::Tensor rowcount, torch::Tensor idx, int64_t num_neighbors, bool replace, std::string path) {
  std::chrono::system_clock::time_point start, end;
  // start = std::chrono::high_resolution_clock::now();
  
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();
  auto out_rowptr = torch::empty(idx.size(0) + 1, options); // Need to rowptr.options() and col.options()
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;
  std::vector<std::vector<double>> g_perf(9);

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
  // start = std::chrono::high_resolution_clock::now();
  int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
  if((fd_col < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }
  int fd_val = open((path+"value").c_str(), O_RDONLY | O_DIRECT);
  if((fd_val < 0))
  {
    std::cout << "can't open " << path+"value" << std::endl;
    exit(1);
  }
  void *temp_col_buffer, *temp_val_buffer;
  size_t partial_size = BUFFER_SIZE;
  // size_t partial_size = 32 * 1024 * 1024UL;
  size_t total_size = BULK_SIZE * partial_size;
  posix_memalign(&(temp_col_buffer), 4096, total_size);
  posix_memalign(&(temp_val_buffer), 4096, total_size);
  std::vector<int64_t> perm_vector(BULK_SIZE * num_neighbors);
  int64_t* perm_ptr = perm_vector.data();

  int64_t col_data_e;
  int64_t target_LBA, LBA_offset, data_offset;

  pthread_t col_thread[BULK_SIZE], value_thread[BULK_SIZE];
  int thr_id;
  void* status;
  struct pread_arg *arg_col, *arg_value;

  arg_col = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  arg_value = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));

  int64_t r, c, e, rc, offset = 0;
  
  for (int64_t i = 0; i < idx.size(0); i+=BULK_SIZE) {
    int64_t chunk_size = BULK_SIZE;
    if(i + BULK_SIZE > idx.size(0))
      chunk_size = idx.size(0) - i;

    size_t bytes = 0;

    for(int64_t j = 0; j < chunk_size; j++){
      r = idx_data[i + j];
      rc = rowcount_data[r];
      std::vector<int64_t> chunk_aligned_pos_bytes(chunk_size);
      std::vector<int64_t> chunk_transfer_size_bytes(chunk_size);

      LBA_offset = rowptr_data[r] * sizeof(int64_t) / 512;
      data_offset = rowptr_data[r] * sizeof(int64_t) % 512;
      int64_t transfer_size_bytes = ((data_offset + rc * sizeof(int64_t) + 512) >> 9 ) << 9;
      chunk_aligned_pos_bytes[j] = LBA_offset * 512;
      chunk_transfer_size_bytes[j] = transfer_size_bytes;
      // chunk_transfer_size_bytes[j] = partial_size / 8;

      arg_col[j].fd = fd_col;
      arg_col[j].buf = (void*)((char*)temp_col_buffer + j * partial_size);
      arg_col[j].count = chunk_transfer_size_bytes[j];
      arg_col[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&col_thread[j], NULL, pthread_pread, (void *)&arg_col[j]);
      if (thr_id < 0)
        std::cout << "col thread_create error: " << j << std::endl;

      arg_value[j].fd = fd_val;
      arg_value[j].buf = (void*)((char*)temp_val_buffer + j * partial_size);
      arg_value[j].count = chunk_transfer_size_bytes[j];
      arg_value[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&value_thread[j], NULL, pthread_pread, (void *)&arg_value[j]);
      if (thr_id < 0)
        std::cout << "value thread_create error: " << j << std::endl;

      bytes += chunk_transfer_size_bytes[j];
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int n_cnt = 0;
      for (const int64_t &p : perm) {
        // std::cout << p + rowptr_data[r] << " ";
        perm_ptr[j * num_neighbors + n_cnt++] = p + data_offset / sizeof(int64_t) + j * partial_size / sizeof(int64_t);
      }
      // std::cout << std::endl;
      offset += n_cnt;
      out_rowptr_data[i + j + 1] = offset;
    }

    for(int64_t j = 0; j < chunk_size; j++)
    {
      pthread_join(col_thread[j], &status);
    }

    std::vector<int64_t> output_col, output_value;
    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        output_col.push_back(((int64_t*)temp_col_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }

    for (int64_t j = 0; j < output_col.size(); j++) {
      if (n_id_map.count(output_col[j]) == 0) {
        n_id_map[output_col[j]] = n_ids.size();
        n_ids.push_back(output_col[j]);
      }
      cols.push_back(n_id_map[output_col[j]]);
    }

    for(int64_t j = 0; j < chunk_size; j++)
      pthread_join(value_thread[j], &status);

    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        value_e_ids.push_back(((int64_t*)temp_val_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }
  }

  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  free(temp_col_buffer);
  free(temp_val_buffer);
  close(fd_col);
  close(fd_val);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << getpid() << " spdk_sample total:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;

  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_pio_blk_latency_breakdown_cpu(torch::Tensor rowptr, torch::Tensor rowcount, torch::Tensor idx, int64_t num_neighbors, bool replace, std::string path) {
  std::chrono::system_clock::time_point start, end;
  std::chrono::system_clock::time_point t0, t1;
  // start = std::chrono::high_resolution_clock::now();
  
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();
  auto out_rowptr = torch::empty(idx.size(0) + 1, options); // Need to rowptr.options() and col.options()
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;
  std::vector<std::vector<double>> g_perf(11);

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
  // start = std::chrono::high_resolution_clock::now();
  int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
  if((fd_col < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }
  int fd_val = open((path+"value").c_str(), O_RDONLY | O_DIRECT);
  if((fd_val < 0))
  {
    std::cout << "can't open " << path+"value" << std::endl;
    exit(1);
  }
  void *temp_col_buffer, *temp_val_buffer;
  size_t partial_size = BUFFER_SIZE;
  // size_t partial_size = 32 * 1024 * 1024UL;
  size_t total_size = BULK_SIZE * partial_size;
  posix_memalign(&(temp_col_buffer), 4096, total_size);
  posix_memalign(&(temp_val_buffer), 4096, total_size);
  std::vector<int64_t> perm_vector(BULK_SIZE * num_neighbors);
  int64_t* perm_ptr = perm_vector.data();

  // end = std::chrono::high_resolution_clock::now();
  // std::cout << "variable setup:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
  int64_t col_data_e;
  int64_t target_LBA, LBA_offset, data_offset;

  start = std::chrono::high_resolution_clock::now();
  pthread_t col_thread[BULK_SIZE], value_thread[BULK_SIZE];
  int thr_id;
  void* status;
  struct pread_arg *arg_col, *arg_value;

  arg_col = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  arg_value = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  end = std::chrono::high_resolution_clock::now();
  std::cout << "thread setup:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() << "us" << std::endl;

  int64_t r, c, e, rc, offset = 0;
  
  for (int64_t i = 0; i < idx.size(0); i+=BULK_SIZE) {
    int64_t chunk_size = BULK_SIZE;
    if(i + BULK_SIZE > idx.size(0))
      chunk_size = idx.size(0) - i;

    size_t bytes = 0;

    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++){
      r = idx_data[i + j];
      rc = rowcount_data[r];
      std::vector<int64_t> chunk_aligned_pos_bytes(chunk_size);
      std::vector<int64_t> chunk_transfer_size_bytes(chunk_size);

      LBA_offset = rowptr_data[r] * sizeof(int64_t) / 512;
      data_offset = rowptr_data[r] * sizeof(int64_t) % 512;
      int64_t transfer_size_bytes = ((data_offset + rc * sizeof(int64_t) + 512) >> 9 ) << 9;
      chunk_aligned_pos_bytes[j] = LBA_offset * 512;
      // chunk_transfer_size_bytes[j] = transfer_size_bytes;
      chunk_transfer_size_bytes[j] = partial_size / 8;

      t0 = std::chrono::high_resolution_clock::now();
      arg_col[j].fd = fd_col;
      arg_col[j].buf = (void*)((char*)temp_col_buffer + j * partial_size);
      arg_col[j].count = chunk_transfer_size_bytes[j];
      arg_col[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&col_thread[j], NULL, pthread_pread, (void *)&arg_col[j]);
      if (thr_id < 0)
        std::cout << "col thread_create error: " << j << std::endl;

      arg_value[j].fd = fd_val;
      arg_value[j].buf = (void*)((char*)temp_val_buffer + j * partial_size);
      arg_value[j].count = chunk_transfer_size_bytes[j];
      arg_value[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&value_thread[j], NULL, pthread_pread, (void *)&arg_value[j]);
      if (thr_id < 0)
        std::cout << "value thread_create error: " << j << std::endl;
      t1 = std::chrono::high_resolution_clock::now();
      g_perf[8].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count());

      bytes += chunk_transfer_size_bytes[j];
      t0 = std::chrono::high_resolution_clock::now();
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int n_cnt = 0;
      for (const int64_t &p : perm) {
        perm_ptr[j * num_neighbors + n_cnt++] = p + data_offset / sizeof(int64_t) + j * partial_size / sizeof(int64_t);
      }
      t1 = std::chrono::high_resolution_clock::now();
      g_perf[9].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count());

      offset += n_cnt;
      out_rowptr_data[i + j + 1] = offset;
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[0].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    for(int64_t j = 0; j < chunk_size; j++)
    {
      pthread_join(col_thread[j], &status);
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[1].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    g_perf[2].push_back(bytes);
    // g_perf[2].push_back(bytes * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));

    std::vector<int64_t> output_col, output_value;
    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        output_col.push_back(((int64_t*)temp_col_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[3].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    g_perf[4].push_back(num_neighbors * 8 * chunk_size);
    // g_perf[4].push_back(num_neighbors * 8 * chunk_size * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));

    start = std::chrono::high_resolution_clock::now();
    for (int64_t j = 0; j < output_col.size(); j++) {
      if (n_id_map.count(output_col[j]) == 0) {
        n_id_map[output_col[j]] = n_ids.size();
        n_ids.push_back(output_col[j]);
      }
      cols.push_back(n_id_map[output_col[j]]);
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[5].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++)
      pthread_join(value_thread[j], &status);
    end = std::chrono::high_resolution_clock::now();
    g_perf[6].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        value_e_ids.push_back(((int64_t*)temp_val_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[7].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
  }

  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  std::cout << "perm and pthread           :" << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size()  << "us" << std::endl;
  std::cout << "pthread                    :" << (double)std::accumulate(g_perf[8].begin(), g_perf[8].end(), 0.0) / g_perf[8].size()  << "us" << std::endl;
  std::cout << "perm                       :" << (double)std::accumulate(g_perf[9].begin(), g_perf[9].end(), 0.0) / g_perf[9].size()  << "us" << std::endl;
  std::cout << "col read time (ssd->dram):" << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size()  << "us" << std::endl;
  std::cout << "avg col read bytes per chunk (ssd->dram):" << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / (g_perf[2].size() * 1024)  << "KB" << std::endl;
  std::cout << "col gather          :" << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size()  << "us" << std::endl;
  std::cout << "col gather size      :" << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size()  << "bytes" << std::endl;
  std::cout << "n_id_map                   :" << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) / g_perf[5].size()  << "us" << std::endl;
  std::cout << "value pthread join time    :" << (double)std::accumulate(g_perf[6].begin(), g_perf[6].end(), 0.0) / g_perf[6].size()  << "us" << std::endl;
  std::cout << "value gather  time    :" << (double)std::accumulate(g_perf[7].begin(), g_perf[7].end(), 0.0) / g_perf[7].size()  << "us" << std::endl;

  free(temp_col_buffer);
  free(temp_val_buffer);
  close(fd_col);
  close(fd_val);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << getpid() << " spdk_sample total:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;

  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_pio_blk_unroll_cpu(torch::Tensor rowptr, torch::Tensor rowcount, torch::Tensor idx, int64_t num_neighbors, bool replace, std::string path) {
  std::chrono::system_clock::time_point start, end;
  // start = std::chrono::high_resolution_clock::now();
  
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  int num_thread;

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();
  auto out_rowptr = torch::empty(idx.size(0) + 1, options); // Need to rowptr.options() and col.options()
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;
  std::vector<std::vector<double>> g_perf(9);

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
  // start = std::chrono::high_resolution_clock::now();
  int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
  if((fd_col < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }
  int fd_val = open((path+"value").c_str(), O_RDONLY | O_DIRECT);
  if((fd_val < 0))
  {
    std::cout << "can't open " << path+"value" << std::endl;
    exit(1);
  }
  void *temp_col_buffer, *temp_val_buffer;
  size_t partial_size = BUFFER_SIZE;
  // size_t partial_size = 32 * 1024 * 1024UL;
  size_t total_size = BULK_SIZE * partial_size;
  posix_memalign(&(temp_col_buffer), 512, total_size);
  posix_memalign(&(temp_val_buffer), 512, total_size);
  std::vector<int64_t> perm_vector(BULK_SIZE * num_neighbors);
  int64_t* perm_ptr = perm_vector.data();

  int64_t numentry_per_sector = total_size / sizeof(int64_t);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << "variable setup:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
  int64_t col_data_e;
  int64_t target_LBA, LBA_offset, data_offset;

  pthread_t col_thread[BULK_SIZE/num_unroll], value_thread[BULK_SIZE/num_unroll];
  int thr_id;
  void* status;
  struct pread_arg arg_col[BULK_SIZE/num_unroll][num_unroll], arg_value[BULK_SIZE/num_unroll][num_unroll];

  int64_t r, c, e, rc, offset = 0;
  
  for (int64_t i = 0; i < idx.size(0); i+=BULK_SIZE) {
    int64_t chunk_size = BULK_SIZE;
    if(i + BULK_SIZE > idx.size(0))
      chunk_size = idx.size(0) - i;

    size_t bytes = 0;

    start = std::chrono::high_resolution_clock::now();
    num_thread = 0;
    for(int64_t j = 0; j < BULK_SIZE; j++){
      arg_col[j/num_unroll][j%num_unroll].fd = -1;
      arg_value[j/num_unroll][j%num_unroll].fd = -1;
    }

    for(int64_t j = 0; j < chunk_size; j++){
      r = idx_data[i + j];
      rc = rowcount_data[r];
      std::vector<int64_t> chunk_aligned_pos_bytes(chunk_size);
      std::vector<int64_t> chunk_transfer_size_bytes(chunk_size);

      LBA_offset = rowptr_data[r] * sizeof(int64_t) / 512;
      data_offset = rowptr_data[r] * sizeof(int64_t) % 512;
      int64_t transfer_size_bytes = ((data_offset + rc * sizeof(int64_t) + 512) >> 9 ) << 9;
      chunk_aligned_pos_bytes[j] = LBA_offset * 512;
      chunk_transfer_size_bytes[j] = transfer_size_bytes;
      // chunk_transfer_size_bytes[j] = partial_size;

      arg_col[j/num_unroll][j%num_unroll].fd = fd_col;
      arg_col[j/num_unroll][j%num_unroll].buf = (void*)((char*)temp_col_buffer + j * partial_size);
      arg_col[j/num_unroll][j%num_unroll].count = chunk_transfer_size_bytes[j];
      arg_col[j/num_unroll][j%num_unroll].pos = chunk_aligned_pos_bytes[j];

      if ((j%num_unroll == num_unroll - 1) || (j == chunk_size - 1)){
        thr_id = pthread_create(&col_thread[num_thread], NULL, pthread_pread_unroll, (void *)&arg_col[j/num_unroll]);
        if (thr_id < 0)
          std::cout << "col thread_create error: " << j << std::endl;
      }

      arg_value[j/num_unroll][j%num_unroll].fd = fd_val;
      arg_value[j/num_unroll][j%num_unroll].buf = (void*)((char*)temp_val_buffer + j * partial_size);
      arg_value[j/num_unroll][j%num_unroll].count = chunk_transfer_size_bytes[j];
      arg_value[j/num_unroll][j%num_unroll].pos = chunk_aligned_pos_bytes[j];

      if ((j%num_unroll == num_unroll - 1) || (j == chunk_size - 1)){
        thr_id = pthread_create(&value_thread[num_thread++], NULL, pthread_pread_unroll, (void *)&arg_value[j/num_unroll]);
        if (thr_id < 0)
          std::cout << "value thread_create error: " << j << std::endl;
      }

      bytes += chunk_transfer_size_bytes[j];
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int n_cnt = 0;
      for (const int64_t &p : perm) {
        // std::cout << p + rowptr_data[r] << " ";
        perm_ptr[j * num_neighbors + n_cnt++] = p + data_offset / sizeof(int64_t) + j * partial_size / sizeof(int64_t);
      }
      // std::cout << std::endl;
      offset += n_cnt;
      out_rowptr_data[i + j + 1] = offset;
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[0].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    for(int64_t j = 0; j < num_thread; j++)
    {
      pthread_join(col_thread[j], &status);
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[1].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    g_perf[2].push_back(bytes * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));

    std::vector<int64_t> output_col, output_value;
    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        output_col.push_back(((int64_t*)temp_col_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[3].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    g_perf[4].push_back(num_neighbors * 8 * BULK_SIZE * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));

    start = std::chrono::high_resolution_clock::now();
    for (int64_t j = 0; j < output_col.size(); j++) {
      if (n_id_map.count(output_col[j]) == 0) {
        // if (output_col[j]>7455000)
        //   std::cout << "c: " << output_col[j] << ", i: " << i << ", chunk_size: " << chunk_size << std::endl;

        n_id_map[output_col[j]] = n_ids.size();
        n_ids.push_back(output_col[j]);
      }
      cols.push_back(n_id_map[output_col[j]]);
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[5].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    for(int64_t j = 0; j < num_thread; j++)
      pthread_join(value_thread[j], &status);

    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        value_e_ids.push_back(((int64_t*)temp_val_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }
  }

  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  // std::cout << "perm                       :" << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size()  << "us" << std::endl;
  // std::cout << "col read time (ssd->dram):" << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size()  << "us" << std::endl;
  // std::cout << "col read BW   (ssd->dram):" << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size()  << "GB/s" << std::endl;
  // std::cout << "col gather           (FPGA):" << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size()  << "us" << std::endl;
  // std::cout << "col gather BW        (FPGA):" << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size()  << "GB/s" << std::endl;
  // std::cout << "n_id_map                   :" << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) / g_perf[5].size()  << "us" << std::endl;

  free(temp_col_buffer);
  free(temp_val_buffer);
  close(fd_col);
  close(fd_val);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << getpid() << " spdk_sample total:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;

  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_pio_blk_mh_cpu(torch::Tensor rowptr, torch::Tensor rowcount, torch::Tensor idx, int64_t num_neighbors, bool replace, std::string path) {
  std::chrono::system_clock::time_point start, end;
  // start = std::chrono::high_resolution_clock::now();
  
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();
  auto out_rowptr = torch::empty(idx.size(0) + 1, options); // Need to rowptr.options() and col.options()
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
  // start = std::chrono::high_resolution_clock::now();
  int fd_col_0 = open("/mnt/0/32x/col", O_RDONLY | O_DIRECT);
  if((fd_col_0 < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }
  int fd_col_1 = open("/mnt/1/32x/col", O_RDONLY | O_DIRECT);
  if((fd_col_1 < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }
  int fd_val_0 = open("/mnt/0/32x/value", O_RDONLY | O_DIRECT);
  if((fd_val_0 < 0))
  {
    std::cout << "can't open " << path+"value" << std::endl;
    exit(1);
  }
  int fd_val_1 = open("/mnt/1/32x/value", O_RDONLY | O_DIRECT);
  if((fd_val_1 < 0))
  {
    std::cout << "can't open " << path+"value" << std::endl;
    exit(1);
  }
  void *temp_col_buffer, *temp_val_buffer;
  size_t partial_size =  BUFFER_SIZE;
  size_t total_size = BULK_SIZE * partial_size;
  posix_memalign(&(temp_col_buffer), 4096, total_size);
  posix_memalign(&(temp_val_buffer), 4096, total_size);
  std::vector<int64_t> perm_vector(BULK_SIZE * num_neighbors);
  int64_t* perm_ptr = perm_vector.data();

  // end = std::chrono::high_resolution_clock::now();
  // std::cout << "variable setup:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;
  int64_t col_data_e;
  int64_t target_LBA, LBA_offset, data_offset;

  pthread_t col_thread[BULK_SIZE], value_thread[BULK_SIZE];
  int thr_id;
  void* status;
  struct pread_arg *arg_col, *arg_value;

  arg_col = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  arg_value = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));

  int64_t r, c, e, rc, offset = 0;
  
  for (int64_t i = 0; i < idx.size(0); i+=BULK_SIZE) {
    int64_t chunk_size = BULK_SIZE;
    if(i + BULK_SIZE > idx.size(0))
      chunk_size = idx.size(0) - i;

    size_t bytes = 0;

    std::chrono::system_clock::time_point start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++){
      r = idx_data[i + j];
      rc = rowcount_data[r];
      std::vector<int64_t> chunk_aligned_pos_bytes(chunk_size);
      std::vector<int64_t> chunk_transfer_size_bytes(chunk_size);

      LBA_offset = rowptr_data[r] * sizeof(int64_t) / 512;
      data_offset = rowptr_data[r] * sizeof(int64_t) % 512;
      int64_t transfer_size_bytes = ((data_offset + rc * sizeof(int64_t) + 512) >> 9 ) << 9;
      chunk_aligned_pos_bytes[j] = LBA_offset * 512;
      //chunk_transfer_size_bytes[j] = transfer_size_bytes;
      chunk_transfer_size_bytes[j] = partial_size;
      if(j%2 == 0)
        arg_col[j].fd = fd_col_1;
      else
        arg_col[j].fd = fd_col_0;
      arg_col[j].buf = (void*)((char*)temp_col_buffer + j * partial_size);
      //arg_col[j].count = chunk_transfer_size_bytes[j];
      arg_col[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&col_thread[j], NULL, pthread_pread, (void *)&arg_col[j]);
      if (thr_id < 0)
        std::cout << "col thread_create error: " << j << std::endl;

      if (j%2 == 0)
        arg_value[j].fd = fd_val_1;
      else
        arg_value[j].fd = fd_val_0;
      arg_value[j].buf = (void*)((char*)temp_val_buffer + j * partial_size);
      arg_value[j].count = chunk_transfer_size_bytes[j];
      arg_value[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&value_thread[j], NULL, pthread_pread, (void *)&arg_value[j]);
      if (thr_id < 0)
        std::cout << "value thread_create error: " << j << std::endl;

      bytes += chunk_transfer_size_bytes[j];
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int n_cnt = 0;
      for (const int64_t &p : perm) {
        // std::cout << p + rowptr_data[r] << " ";
        perm_ptr[j * num_neighbors + n_cnt++] = p + data_offset / sizeof(int64_t) + j * partial_size / sizeof(int64_t);
      }
      std::cout << std::endl;
      offset += n_cnt;
      out_rowptr_data[i + j + 1] = offset;
    }
    // std::chrono::system_clock::time_point t0 = std::chrono::high_resolution_clock::now();
    // std::cout << "perm time:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(t0-start).count() / (double)1000 << "ms" << std::endl;
    for(int64_t j = 0; j < chunk_size; j++)
    {
      pthread_join(col_thread[j], &status);
      // std::cout << "Thread end" << status << std::endl;
    }

    std::vector<int64_t> output_col, output_value;
    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        output_col.push_back(((int64_t*)temp_col_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }
    for (int64_t j = 0; j < output_col.size(); j++) {
      if (n_id_map.count(output_col[j]) == 0) {
        n_id_map[output_col[j]] = n_ids.size();
        n_ids.push_back(output_col[j]);
      }
      cols.push_back(n_id_map[output_col[j]]);
    }

    for(int64_t j = 0; j < chunk_size; j++)
      pthread_join(value_thread[j], &status);
    for(int64_t j = 0; j < chunk_size; j++)
    {
      for(int64_t k = 0; k < num_neighbors; k++)
      {
        value_e_ids.push_back(((int64_t*)temp_val_buffer)[perm_ptr[k + j * num_neighbors]]);
      }
    }
  }

  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  free(temp_col_buffer);
  free(temp_val_buffer);
  close(fd_col_0);
  close(fd_col_1);
  close(fd_val_0);
  close(fd_val_1);
  // end = std::chrono::high_resolution_clock::now();
  // std::cout << getpid() << " spdk_sample total:" << (double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count() / (double)1000 << "ms" << std::endl;

  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_p2p_blk_pthread_cpu(torch::Tensor rowptr, torch::Tensor rowcount,
               torch::Tensor idx, int64_t num_neighbors, bool replace) {
  std::chrono::system_clock::time_point start, end;
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);
  
  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();

  auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;
  std::vector<std::vector<double>> g_perf(9);
  
  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }

  cl_kernel krnl_sample_col;
  cl_kernel krnl_sample_value;
  cl_event p2pEvt_col, migrateEvt_col, kernelEvt_col, data_read_event_col;
  cl_event p2pEvt_value, kernelEvt_value, data_read_event_value;

  int err = 0;
  
  buffer_perm =
    clCreateBuffer(context, CL_MEM_READ_ONLY,
                  num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  perm_ptr =
    (int64_t*)clEnqueueMapBuffer(command_queue, buffer_perm, CL_TRUE, CL_MAP_WRITE, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);
  
  buffer_output_col =
    clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                  num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  output_col_ptr =
    (int64_t*)clEnqueueMapBuffer(command_queue, buffer_output_col, CL_TRUE, CL_MAP_READ, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);

  buffer_output_value =
  clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  output_value_ptr =
  (int64_t*)clEnqueueMapBuffer(command_queue, buffer_output_value, CL_TRUE, CL_MAP_READ, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);  
  
  // krnl_sample_col = clCreateKernel(program, "adder:{adder_1}", &err);
  // krnl_sample_value = clCreateKernel(program, "adder:{adder_2}", &err);
  krnl_sample_col = clCreateKernel(program, "adder", &err);
  krnl_sample_value = clCreateKernel(program, "adder", &err);
  clSetKernelArg(krnl_sample_col, 0, sizeof(cl_mem), &(buffer_col));
  clSetKernelArg(krnl_sample_col, 1, sizeof(cl_mem), &(buffer_perm));
  clSetKernelArg(krnl_sample_col, 2, sizeof(cl_mem), &(buffer_output_col));
  clSetKernelArg(krnl_sample_value, 0, sizeof(cl_mem), &(buffer_value));
  clSetKernelArg(krnl_sample_value, 1, sizeof(cl_mem), &(buffer_perm));
  clSetKernelArg(krnl_sample_value, 2, sizeof(cl_mem), &(buffer_output_value));
  
  int64_t r, c, rc, offset = 0;
  int64_t block_offset = 0;
  
  pthread_t col_thread[BULK_SIZE], value_thread[BULK_SIZE];
  int thr_id;
  void *status;
  struct pread_arg *arg_col, *arg_value;

  arg_col = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  arg_value = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  
  for (int64_t i = 0; i < idx.size(0); i+=BULK_SIZE)
  {
    int64_t chunk_size = BULK_SIZE;
    if(i + BULK_SIZE > idx.size(0))
      chunk_size = idx.size(0) - i;

    int total_size = num_neighbors * chunk_size;
    clSetKernelArg(krnl_sample_col, 3, sizeof(int), &total_size);
    clSetKernelArg(krnl_sample_value, 3, sizeof(int), &total_size);
    p2pEvt_col = clCreateUserEvent(context, NULL);
    p2pEvt_value = clCreateUserEvent(context, NULL);
    std::vector<int64_t> chunk_aligned_pos_bytes(chunk_size);
    std::vector<int64_t> chunk_transfer_size_bytes(chunk_size);
    size_t bytes = 0;
    clEnqueueMigrateMemObjects(command_queue, 1, &buffer_perm, 0, 1, &p2pEvt_col, &migrateEvt_col);
    OCL_CHECK(err, err = clEnqueueTask(command_queue, krnl_sample_col, 1, &migrateEvt_col, &kernelEvt_col));
    clEnqueueMigrateMemObjects(command_queue, 1, &(buffer_output_col), CL_MIGRATE_MEM_OBJECT_HOST, 1, &kernelEvt_col, &data_read_event_col);
    OCL_CHECK(err, err = clEnqueueTask(command_queue, krnl_sample_value, 1, &p2pEvt_value, &kernelEvt_value));
    clEnqueueMigrateMemObjects(command_queue, 1, &(buffer_output_value), CL_MIGRATE_MEM_OBJECT_HOST, 1, &kernelEvt_value, &data_read_event_value);
    
    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++)
    {
      r = idx_data[i + j];
      rc = rowcount_data[r];

      size_t pos_bytes = (size_t)(rowptr_data[r] * sizeof(int64_t));
      size_t aligned_pos_bytes = (pos_bytes >> 9) << 9;
      size_t offset_bytes = pos_bytes % SECTORSIZE_BYTES;
      size_t transfer_size_bytes = ((offset_bytes + rc * sizeof(int64_t) + SECTORSIZE_BYTES) >> 9 ) << 9;
      block_offset = offset_bytes / sizeof(int64_t);
      assert(transfer_size_bytes <= BUFFER_SIZE);
      chunk_aligned_pos_bytes[j] = aligned_pos_bytes;
      chunk_transfer_size_bytes[j] = transfer_size_bytes;
      // chunk_transfer_size_bytes[j] = BUFFER_SIZE;
      
      arg_col[j].fd = col_fd;
      arg_col[j].buf = (void*)(col_ptr + j * BUFFER_SIZE / sizeof(int64_t));
      arg_col[j].count = chunk_transfer_size_bytes[j];
      arg_col[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&col_thread[j], NULL, pthread_pread, (void *)&arg_col[j]);
      if (thr_id < 0)
        std::cout << "col thread_create error: " << j << std::endl;

      arg_value[j].fd = value_fd;
      arg_value[j].buf = (void*)(value_ptr + j * BUFFER_SIZE / sizeof(int64_t));
      arg_value[j].count = chunk_transfer_size_bytes[j];
      arg_value[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&value_thread[j], NULL, pthread_pread, (void *)&arg_value[j]);
      if (thr_id < 0)
        std::cout << "value thread_create error: " << j << std::endl;

      bytes += chunk_transfer_size_bytes[j];

      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int n_cnt = 0;
      for (const int64_t &p : perm) {
        // std::cout << p + rowptr_data[r] << " ";
        perm_ptr[j * num_neighbors + n_cnt++] = p + block_offset + j * BUFFER_SIZE / sizeof(int64_t);
      }
      // std::cout << std::endl;
      offset += n_cnt;
      out_rowptr_data[i + j + 1] = offset;
    }

    for(int64_t j = 0; j < chunk_size; j++)
    {
      pthread_join(col_thread[j], &status);
    }
    
    // Launch the Kernel
    clSetUserEventStatus(p2pEvt_col, CL_COMPLETE);

    for(int64_t j = 0; j < chunk_size; j++)
      pthread_join(value_thread[j], &status);
    
    // Launch the Kernel
    clSetUserEventStatus(p2pEvt_value, CL_COMPLETE);
    
    clWaitForEvents(1, &data_read_event_col);
    int total_neighbor_cnt = num_neighbors * chunk_size;
    for(int j = 0; j < total_neighbor_cnt; j++)
    {
      c = output_col_ptr[j];
      if (n_id_map.count(c) == 0) {
        n_id_map[c] = n_ids.size();
        n_ids.push_back(c);
      }
      cols.push_back(n_id_map[c]);
    }

    clWaitForEvents(1, &data_read_event_value);
    for(int j = 0; j < total_neighbor_cnt; j++)
    {
      value_e_ids.push_back(output_value_ptr[j]);
    }
    clReleaseEvent(p2pEvt_col);
    clReleaseEvent(p2pEvt_value);
  }

  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  clReleaseKernel(krnl_sample_col);
  clReleaseKernel(krnl_sample_value);
  clEnqueueUnmapMemObject(command_queue, buffer_perm, perm_ptr, 0, NULL, NULL);
  clEnqueueUnmapMemObject(command_queue, buffer_output_col, output_col_ptr, 0, NULL, NULL);
  clEnqueueUnmapMemObject(command_queue, buffer_output_value, output_value_ptr, 0, NULL, NULL);
  clReleaseMemObject(buffer_perm);
  clReleaseMemObject(buffer_output_col);
  clReleaseMemObject(buffer_output_value);

  free(arg_col);
  free(arg_value);
  
  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_p2p_blk_pthread_latency_breakdown_cpu(torch::Tensor rowptr, torch::Tensor rowcount,
               torch::Tensor idx, int64_t num_neighbors, bool replace) {
  // std::cout << "sample_adj_p2p_blk_pthread_cpu" << std::endl;
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);
  
  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();

  auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;
  std::vector<std::vector<double>> g_perf(11);
  
  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }

  std::chrono::system_clock::time_point t0, t1;
  std::chrono::system_clock::time_point start = std::chrono::high_resolution_clock::now();
  cl_kernel krnl_sample_col;
  cl_kernel krnl_sample_value;
  cl_event p2pEvt_col, migrateEvt_col, kernelEvt_col, data_read_event_col;
  cl_event p2pEvt_value, kernelEvt_value, data_read_event_value;

  int err = 0;
  
  buffer_perm =
    clCreateBuffer(context, CL_MEM_READ_ONLY,
                  num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  perm_ptr =
    (int64_t*)clEnqueueMapBuffer(command_queue, buffer_perm, CL_TRUE, CL_MAP_WRITE, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);
  
  buffer_output_col =
    clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                  num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  output_col_ptr =
    (int64_t*)clEnqueueMapBuffer(command_queue, buffer_output_col, CL_TRUE, CL_MAP_READ, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);

  buffer_output_value =
  clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  output_value_ptr =
  (int64_t*)clEnqueueMapBuffer(command_queue, buffer_output_value, CL_TRUE, CL_MAP_READ, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);  
  std::chrono::system_clock::time_point end = std::chrono::high_resolution_clock::now();
  std::cout << "Kernel buffer setup:" << ((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) << "us" << std::endl;
  
  start = std::chrono::high_resolution_clock::now();
  // krnl_sample_col = clCreateKernel(program, "adder:{adder_1}", &err);
  // krnl_sample_value = clCreateKernel(program, "adder:{adder_2}", &err);
  krnl_sample_col = clCreateKernel(program, "adder", &err);
  krnl_sample_value = clCreateKernel(program, "adder", &err);
  clSetKernelArg(krnl_sample_col, 0, sizeof(cl_mem), &(buffer_col));
  clSetKernelArg(krnl_sample_col, 1, sizeof(cl_mem), &(buffer_perm));
  clSetKernelArg(krnl_sample_col, 2, sizeof(cl_mem), &(buffer_output_col));
  clSetKernelArg(krnl_sample_value, 0, sizeof(cl_mem), &(buffer_value));
  clSetKernelArg(krnl_sample_value, 1, sizeof(cl_mem), &(buffer_perm));
  clSetKernelArg(krnl_sample_value, 2, sizeof(cl_mem), &(buffer_output_value));
  end = std::chrono::high_resolution_clock::now();
  std::cout << "Kernel create and arg setup:" << ((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) << "us" << std::endl;
  
  int64_t r, c, rc, offset = 0;
  int64_t block_offset = 0;
  
  start = std::chrono::high_resolution_clock::now();
  pthread_t col_thread[BULK_SIZE], value_thread[BULK_SIZE];
  int thr_id;
  void *status;
  struct pread_arg *arg_col, *arg_value;

  arg_col = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  arg_value = (struct pread_arg *)calloc(BULK_SIZE, sizeof(struct pread_arg));
  end = std::chrono::high_resolution_clock::now();
  std::cout << "thread setup:" << ((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) << "us" << std::endl;
  
  for (int64_t i = 0; i < idx.size(0); i+=BULK_SIZE)
  {
    int64_t chunk_size = BULK_SIZE;
    if(i + BULK_SIZE > idx.size(0))
      chunk_size = idx.size(0) - i;

    int total_size = num_neighbors * chunk_size;
    clSetKernelArg(krnl_sample_col, 3, sizeof(int), &total_size);
    clSetKernelArg(krnl_sample_value, 3, sizeof(int), &total_size);
    p2pEvt_col = clCreateUserEvent(context, NULL);
    p2pEvt_value = clCreateUserEvent(context, NULL);
    std::vector<int64_t> chunk_aligned_pos_bytes(chunk_size);
    std::vector<int64_t> chunk_transfer_size_bytes(chunk_size);
    size_t bytes = 0;
    clEnqueueMigrateMemObjects(command_queue, 1, &buffer_perm, 0, 1, &p2pEvt_col, &migrateEvt_col);
    OCL_CHECK(err, err = clEnqueueTask(command_queue, krnl_sample_col, 1, &migrateEvt_col, &kernelEvt_col));
    clEnqueueMigrateMemObjects(command_queue, 1, &(buffer_output_col), CL_MIGRATE_MEM_OBJECT_HOST, 1, &kernelEvt_col, &data_read_event_col);
    OCL_CHECK(err, err = clEnqueueTask(command_queue, krnl_sample_value, 1, &p2pEvt_value, &kernelEvt_value));
    clEnqueueMigrateMemObjects(command_queue, 1, &(buffer_output_value), CL_MIGRATE_MEM_OBJECT_HOST, 1, &kernelEvt_value, &data_read_event_value);
    
    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++)
    {
      r = idx_data[i + j];
      rc = rowcount_data[r];

      size_t pos_bytes = (size_t)(rowptr_data[r] * sizeof(int64_t));
      size_t aligned_pos_bytes = (pos_bytes >> 9) << 9;
      size_t offset_bytes = pos_bytes % SECTORSIZE_BYTES;
      size_t transfer_size_bytes = ((offset_bytes + rc * sizeof(int64_t) + SECTORSIZE_BYTES) >> 9 ) << 9;
      block_offset = offset_bytes / sizeof(int64_t);
      assert(transfer_size_bytes <= BUFFER_SIZE);
      chunk_aligned_pos_bytes[j] = aligned_pos_bytes;
      // chunk_transfer_size_bytes[j] = transfer_size_bytes;
      chunk_transfer_size_bytes[j] = BUFFER_SIZE / 8;
      
      t0 = std::chrono::high_resolution_clock::now();
      arg_col[j].fd = col_fd;
      arg_col[j].buf = (void*)(col_ptr + j * BUFFER_SIZE / sizeof(int64_t));
      arg_col[j].count = chunk_transfer_size_bytes[j];
      arg_col[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&col_thread[j], NULL, pthread_pread, (void *)&arg_col[j]);
      if (thr_id < 0)
        std::cout << "col thread_create error: " << j << std::endl;

      arg_value[j].fd = value_fd;
      arg_value[j].buf = (void*)(value_ptr + j * BUFFER_SIZE / sizeof(int64_t));
      arg_value[j].count = chunk_transfer_size_bytes[j];
      arg_value[j].pos = chunk_aligned_pos_bytes[j];

      thr_id = pthread_create(&value_thread[j], NULL, pthread_pread, (void *)&arg_value[j]);
      if (thr_id < 0)
        std::cout << "value thread_create error: " << j << std::endl;
      t1 = std::chrono::high_resolution_clock::now();
      g_perf[8].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count());

      bytes += chunk_transfer_size_bytes[j];

      t0 = std::chrono::high_resolution_clock::now();
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int n_cnt = 0;
      for (const int64_t &p : perm) {
        perm_ptr[j * num_neighbors + n_cnt++] = p + block_offset + j * BUFFER_SIZE / sizeof(int64_t);
      }
      t1 = std::chrono::high_resolution_clock::now();
      g_perf[9].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count());
      offset += n_cnt;
      out_rowptr_data[i + j + 1] = offset;
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[0].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    for(int64_t j = 0; j < chunk_size; j++)
    {
      pthread_join(col_thread[j], &status);
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[1].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()); // col read
    g_perf[2].push_back(bytes);
    // g_perf[2].push_back(bytes * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));
    
    // Launch the Kernel
    start = std::chrono::high_resolution_clock::now();
    clSetUserEventStatus(p2pEvt_col, CL_COMPLETE);
    clWaitForEvents(1, &data_read_event_col);
    end = std::chrono::high_resolution_clock::now();
    g_perf[5].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    g_perf[6].push_back(num_neighbors * 8 * chunk_size);
    // g_perf[6].push_back(num_neighbors * 8 * chunk_size * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));

    start = std::chrono::high_resolution_clock::now();
    for(int64_t j = 0; j < chunk_size; j++)
      pthread_join(value_thread[j], &status);
    end = std::chrono::high_resolution_clock::now();
    g_perf[3].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    // g_perf[4].push_back(bytes * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));
    
    // Launch the Kernel
    clSetUserEventStatus(p2pEvt_value, CL_COMPLETE);
    
    clWaitForEvents(1, &data_read_event_col);
    int total_neighbor_cnt = num_neighbors * chunk_size;
    start = std::chrono::high_resolution_clock::now();
    for(int j = 0; j < total_neighbor_cnt; j++)
    {
      c = output_col_ptr[j];
      if (n_id_map.count(c) == 0) {
        n_id_map[c] = n_ids.size();
        n_ids.push_back(c);
      }
      cols.push_back(n_id_map[c]);
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[7].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    start = std::chrono::high_resolution_clock::now();
    clWaitForEvents(1, &data_read_event_value);
    end = std::chrono::high_resolution_clock::now();
    g_perf[10].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    // std::cout << "output_value_ptr" << std::endl;
    for(int j = 0; j < total_neighbor_cnt; j++)
    {
      // std::cout << output_value_ptr[i] << " ";
      value_e_ids.push_back(output_value_ptr[j]);
    }
    // std::cout << std::endl;
    clReleaseEvent(p2pEvt_col);
    clReleaseEvent(p2pEvt_value);
  }


  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  std::cout << "perm and pthread               :" << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size()  << "us" << std::endl;
  std::cout << "pthread create                 :" << (double)std::accumulate(g_perf[8].begin(), g_perf[8].end(), 0.0) / g_perf[8].size()  << "us" << std::endl;
  std::cout << "perm                           :" << (double)std::accumulate(g_perf[9].begin(), g_perf[9].end(), 0.0) / g_perf[9].size()  << "us" << std::endl;
  std::cout << "col read time   (ssd->fpga)    :" << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size()  << "us" << std::endl;
  std::cout << "avg col read bytes per chunk   :" << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / (g_perf[2].size()* 1024)  << "KB" << std::endl;
  // std::cout << "col read BW     (ssd->fpga):" << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size()  << "GB/s" << std::endl;
  // std::cout << "col value read BW   (ssd->fpga):" << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size()  << "GB/s" << std::endl;
  std::cout << "col gather           (FPGA)    :" << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) / g_perf[5].size()  << "us" << std::endl;
  std::cout << "col gather bytes     (FPGA)    :" << (double)std::accumulate(g_perf[6].begin(), g_perf[6].end(), 0.0) / g_perf[6].size()  << "bytes" << std::endl;
  std::cout << "value join time (ssd->fpga)    :" << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size()  << "us" << std::endl;
  std::cout << "n_id_map                       :" << (double)std::accumulate(g_perf[7].begin(), g_perf[7].end(), 0.0) / g_perf[7].size()  << "us" << std::endl;
  std::cout << "value gather wait time         :" << (double)std::accumulate(g_perf[10].begin(), g_perf[10].end(), 0.0) / g_perf[10].size()  << "us" << std::endl;

  clReleaseKernel(krnl_sample_col);
  clReleaseKernel(krnl_sample_value);
  clEnqueueUnmapMemObject(command_queue, buffer_perm, perm_ptr, 0, NULL, NULL);
  clEnqueueUnmapMemObject(command_queue, buffer_output_col, output_col_ptr, 0, NULL, NULL);
  clEnqueueUnmapMemObject(command_queue, buffer_output_value, output_value_ptr, 0, NULL, NULL);
  clReleaseMemObject(buffer_perm);
  clReleaseMemObject(buffer_output_col);
  clReleaseMemObject(buffer_output_value);

  free(arg_col);
  free(arg_value);
  
  return std::make_tuple(out_rowptr, col, n_id, val);
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_p2p_blk_pthread_unroll_cpu(torch::Tensor rowptr, torch::Tensor rowcount,
               torch::Tensor idx, int64_t num_neighbors, bool replace) {
  // std::cout << "sample_adj_p2p_blk_pthread_cpu" << std::endl;
  CHECK_CPU(rowptr);
  CHECK_CPU(idx);
  CHECK_INPUT(idx.dim() == 1);

  int num_thread;
  
  auto rowptr_data = rowptr.data_ptr<int64_t>();
  auto rowcount_data = rowcount.data_ptr<int64_t>();
  auto idx_data = idx.data_ptr<int64_t>();
  auto options = rowptr.options();

  auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
  auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> value_e_ids;
  std::vector<std::vector<double>> g_perf(9);
  
  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }

  std::chrono::system_clock::time_point start = std::chrono::high_resolution_clock::now();
  cl_kernel krnl_sample_col;
  cl_kernel krnl_sample_value;
  cl_event p2pEvt_col, migrateEvt_col, kernelEvt_col, data_read_event_col;
  cl_event p2pEvt_value, kernelEvt_value, data_read_event_value;

  int err = 0;
  
  buffer_perm =
    clCreateBuffer(context, CL_MEM_READ_ONLY,
                  num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  perm_ptr =
    (int64_t*)clEnqueueMapBuffer(command_queue, buffer_perm, CL_TRUE, CL_MAP_WRITE, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);
  
  buffer_output_col =
    clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                  num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  output_col_ptr =
    (int64_t*)clEnqueueMapBuffer(command_queue, buffer_output_col, CL_TRUE, CL_MAP_READ, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);

  buffer_output_value =
  clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                num_neighbors * BULK_SIZE * sizeof(int64_t), NULL, &err);
  output_value_ptr =
  (int64_t*)clEnqueueMapBuffer(command_queue, buffer_output_value, CL_TRUE, CL_MAP_READ, 0, num_neighbors * BULK_SIZE * sizeof(int64_t), 0, NULL, NULL, NULL);  
  
  // krnl_sample_col = clCreateKernel(program, "adder:{adder_1}", &err);
  // krnl_sample_value = clCreateKernel(program, "adder:{adder_2}", &err);
  krnl_sample_col = clCreateKernel(program, "adder", &err);
  krnl_sample_value = clCreateKernel(program, "adder", &err);

  clSetKernelArg(krnl_sample_col, 0, sizeof(cl_mem), &(buffer_col));
  clSetKernelArg(krnl_sample_col, 1, sizeof(cl_mem), &(buffer_perm));
  clSetKernelArg(krnl_sample_col, 2, sizeof(cl_mem), &(buffer_output_col));
  clSetKernelArg(krnl_sample_value, 0, sizeof(cl_mem), &(buffer_value));
  clSetKernelArg(krnl_sample_value, 1, sizeof(cl_mem), &(buffer_perm));
  clSetKernelArg(krnl_sample_value, 2, sizeof(cl_mem), &(buffer_output_value));
  
  int64_t r, c, rc, offset = 0;
  int64_t block_offset = 0;

  pthread_t col_thread[BULK_SIZE/num_unroll], value_thread[BULK_SIZE/num_unroll];
  int thr_id;
  void *status;
  struct pread_arg arg_col[BULK_SIZE/num_unroll][num_unroll], arg_value[BULK_SIZE/num_unroll][num_unroll];

  std::chrono::system_clock::time_point end = std::chrono::high_resolution_clock::now();
  // std::cout << "Kernel setup:" << ((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) << "us" << std::endl;

  for (int64_t i = 0; i < idx.size(0); i+=BULK_SIZE)
  {
    int64_t chunk_size = BULK_SIZE;
    if(i + BULK_SIZE > idx.size(0))
      chunk_size = idx.size(0) - i;

    int total_size = num_neighbors * chunk_size;
    clSetKernelArg(krnl_sample_col, 3, sizeof(int), &total_size);
    clSetKernelArg(krnl_sample_value, 3, sizeof(int), &total_size);
    p2pEvt_col = clCreateUserEvent(context, NULL);
    p2pEvt_value = clCreateUserEvent(context, NULL);
    std::vector<int64_t> chunk_aligned_pos_bytes(BULK_SIZE);
    std::vector<int64_t> chunk_transfer_size_bytes(BULK_SIZE);
    size_t bytes = 0;
    clEnqueueMigrateMemObjects(command_queue, 1, &buffer_perm, 0, 1, &p2pEvt_col, &migrateEvt_col);
    OCL_CHECK(err, err = clEnqueueTask(command_queue, krnl_sample_col, 1, &migrateEvt_col, &kernelEvt_col));
    clEnqueueMigrateMemObjects(command_queue, 1, &(buffer_output_col), CL_MIGRATE_MEM_OBJECT_HOST, 1, &kernelEvt_col, &data_read_event_col);
    OCL_CHECK(err, err = clEnqueueTask(command_queue, krnl_sample_value, 1, &p2pEvt_value, &kernelEvt_value));
    clEnqueueMigrateMemObjects(command_queue, 1, &(buffer_output_value), CL_MIGRATE_MEM_OBJECT_HOST, 1, &kernelEvt_value, &data_read_event_value);

    start = std::chrono::high_resolution_clock::now();
    num_thread = 0;
    for(int64_t j = 0; j < BULK_SIZE; j++){
      arg_col[j/num_unroll][j%num_unroll].fd = -1;
      arg_value[j/num_unroll][j%num_unroll].fd = -1;
    }

    for(int64_t j = 0; j < chunk_size; j++)
    {
      
      r = idx_data[i + j];
      rc = rowcount_data[r];

      size_t pos_bytes = (size_t)(rowptr_data[r] * sizeof(int64_t));
      size_t aligned_pos_bytes = (pos_bytes >> 9) << 9;
      size_t offset_bytes = pos_bytes % SECTORSIZE_BYTES;
      size_t transfer_size_bytes = ((offset_bytes + rc * sizeof(int64_t) + SECTORSIZE_BYTES) >> 9 ) << 9;
      block_offset = offset_bytes / sizeof(int64_t);
      assert(transfer_size_bytes <= BUFFER_SIZE);
      chunk_aligned_pos_bytes[j] = aligned_pos_bytes;
      chunk_transfer_size_bytes[j] = transfer_size_bytes;
      
      arg_col[j/num_unroll][j%num_unroll].fd = col_fd;
      arg_col[j/num_unroll][j%num_unroll].buf = (void*)(col_ptr + j * BUFFER_SIZE / sizeof(int64_t));
      arg_col[j/num_unroll][j%num_unroll].count = chunk_transfer_size_bytes[j];
      arg_col[j/num_unroll][j%num_unroll].pos = chunk_aligned_pos_bytes[j];

      if ((j%num_unroll == num_unroll - 1) || (j == chunk_size - 1)){
        thr_id = pthread_create(&col_thread[num_thread], NULL, pthread_pread_unroll, (void *)&arg_col[j/num_unroll]);
        if (thr_id < 0)
          std::cout << "col thread_create error: " << j << std::endl;
      }

      arg_value[j/num_unroll][j%num_unroll].fd = value_fd;
      arg_value[j/num_unroll][j%num_unroll].buf = (void*)(value_ptr + j * BUFFER_SIZE / sizeof(int64_t));
      arg_value[j/num_unroll][j%num_unroll].count = chunk_transfer_size_bytes[j];
      arg_value[j/num_unroll][j%num_unroll].pos = chunk_aligned_pos_bytes[j];

      if ((j%num_unroll == num_unroll - 1) || (j == chunk_size - 1)){
        thr_id = pthread_create(&value_thread[num_thread++], NULL, pthread_pread_unroll, (void *)&arg_value[j/num_unroll]);
        if (thr_id < 0)
          std::cout << "value thread_create error: " << j << std::endl;
      }

      bytes += chunk_transfer_size_bytes[j];

      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      int n_cnt = 0;
      for (const int64_t &p : perm) {
        // std::cout << p + rowptr_data[r] << " ";
        perm_ptr[j * num_neighbors + n_cnt++] = p + block_offset + j * BUFFER_SIZE / sizeof(int64_t);
      }
      // std::cout << std::endl;
      offset += n_cnt;
      out_rowptr_data[i + j + 1] = offset;
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[0].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    for(int64_t j = 0; j < num_thread; j++)
    {
      pthread_join(col_thread[j], &status);
    }
    // end = std::chrono::high_resolution_clock::now();
    // g_perf[1].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()); // col read
    // g_perf[2].push_back(bytes * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));
    
    
    // Launch the Kernel
    start = std::chrono::high_resolution_clock::now();
    clSetUserEventStatus(p2pEvt_col, CL_COMPLETE);
    clWaitForEvents(1, &data_read_event_col);
    end = std::chrono::high_resolution_clock::now();
    g_perf[5].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    g_perf[6].push_back(num_neighbors * 8 * BULK_SIZE * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));

    for(int64_t j = 0; j < num_thread; j++)
      pthread_join(value_thread[j], &status);
    end = std::chrono::high_resolution_clock::now();
    // g_perf[3].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());
    // g_perf[4].push_back(2 * bytes * 1000000 / (((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()) * 1024 * 1024 * 1024));
    // Launch the Kernel
    clSetUserEventStatus(p2pEvt_value, CL_COMPLETE);
    
    clWaitForEvents(1, &data_read_event_col);
    int total_neighbor_cnt = num_neighbors * chunk_size;
    start = std::chrono::high_resolution_clock::now();
    for(int j = 0; j < total_neighbor_cnt; j++)
    {
      c = output_col_ptr[j];
      // if (c>7455000)
      //   std::cout << "c: " << c << ", i: " << i << ", chunk_size: " << chunk_size << std::endl;
      if (n_id_map.count(c) == 0) {
        n_id_map[c] = n_ids.size();
        n_ids.push_back(c);
      }
      cols.push_back(n_id_map[c]);
    }
    end = std::chrono::high_resolution_clock::now();
    g_perf[7].push_back((double) std::chrono::duration_cast<std::chrono::microseconds>(end-start).count());

    clWaitForEvents(1, &data_read_event_value);
    // std::cout << "output_value_ptr" << std::endl;
    for(int j = 0; j < total_neighbor_cnt; j++)
    {
      // std::cout << output_value_ptr[i] << " ";
      value_e_ids.push_back(output_value_ptr[j]);
    }
    // std::cout << std::endl;
    clReleaseEvent(p2pEvt_col);
    clReleaseEvent(p2pEvt_value);
  }


  int64_t n_len = n_ids.size(), e_len = cols.size();
  auto col = torch::from_blob(cols.data(), {e_len}, options).clone();
  auto n_id = torch::from_blob(n_ids.data(), {n_len}, options).clone();
  auto val = torch::from_blob(value_e_ids.data(), {e_len}, options).clone();

  // std::cout << "perm                       :" << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size()  << "us" << std::endl;
  // std::cout << "col read time   (ssd->fpga):" << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size()  << "us" << std::endl;
  // std::cout << "col read BW     (ssd->fpga):" << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size()  << "GB/s" << std::endl;
  // std::cout << "col value read time (ssd->fpga):" << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size()  << "us" << std::endl;
  // std::cout << "col value read BW   (ssd->fpga):" << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size()  << "GB/s" << std::endl;
  // std::cout << "col gather           (FPGA):" << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) / g_perf[5].size()  << "us" << std::endl;
  // std::cout << "col gather BW        (FPGA):" << (double)std::accumulate(g_perf[6].begin(), g_perf[6].end(), 0.0) / g_perf[6].size()  << "GB/s" << std::endl;
  // std::cout << "n_id_map                   :" << (double)std::accumulate(g_perf[7].begin(), g_perf[7].end(), 0.0) / g_perf[7].size()  << "us" << std::endl;

  clReleaseKernel(krnl_sample_col);
  clReleaseKernel(krnl_sample_value);
  clEnqueueUnmapMemObject(command_queue, buffer_perm, perm_ptr, 0, NULL, NULL);
  clEnqueueUnmapMemObject(command_queue, buffer_output_col, output_col_ptr, 0, NULL, NULL);
  clEnqueueUnmapMemObject(command_queue, buffer_output_value, output_value_ptr, 0, NULL, NULL);
  clReleaseMemObject(buffer_perm);
  clReleaseMemObject(buffer_output_col);
  clReleaseMemObject(buffer_output_value);
  
  return std::make_tuple(out_rowptr, col, n_id, val);
}


void p2p_init(std::string col_path, std::string value_path, std::string embedding_path, std::string binaryFile_path) {
  // TODO: path
	//const char* binaryFile = "/home/via/mh/krnl_sample.xclbin";
	const char* binaryFile = binaryFile_path.c_str();
  
  // Open NVMe SSD files
  col_fd = open(col_path.c_str(), O_RDONLY | O_DIRECT);
  if (col_fd < 0) {
    std::cout << "ERR: open " << col_path << "failed: " << strerror(errno) << std::endl;
    exit(1);
  }
  std::cout << "INFO: Successfully opened NVME SSD " << col_path << std::endl;

  value_fd = open(value_path.c_str(), O_RDONLY | O_DIRECT);
  if (value_fd < 0) {
    std::cout << "ERR: open " << value_path << "failed: " << strerror(errno) << std::endl;
    exit(1);
  }
  std::cout << "INFO: Successfully opened NVME SSD " << value_path << std::endl;

  // embedding_fd = open(embedding_path.c_str(), O_RDWR | O_DIRECT);
  // if (col_fd < 0) {
  //   cout << "ERR: open " << embedding_path << "failed: " << strerror(errno) << endl;
    // exit(1);
  // }
  // cout << "INFO: Successfully opened NVME SSD " << embedding_path << endl;
  
  // Platform and device setup
  cl_int err = CL_SUCCESS;
  int error;
  cl_platform_id platform = nullptr;
  error = clGetPlatformIDs(1, &platform, nullptr);

  cl_uint num_devices = 0;
  error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ACCELERATOR, 0, nullptr,
                         &num_devices);
  if (error != CL_SUCCESS) {
    printf("Error: no devices\n");
    exit(EXIT_FAILURE);
  }
  std::vector<cl_device_id> devices(num_devices);
  error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ACCELERATOR, num_devices,
                         devices.data(), nullptr);
  if (error != CL_SUCCESS) {
    printf("Error: could not determine device name\n");
    exit(EXIT_FAILURE);
  }
  cl_device_id device = devices.front();

  context = clCreateContext(0, 1, &device, nullptr, nullptr, &err);
  if (err != CL_SUCCESS)
    std::cout << "clCreateContext call: Failed to create a compute context"
              << err << std::endl;

  command_queue = clCreateCommandQueue(
      context, device,
      CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
  if (err != CL_SUCCESS)
    std::cout << "clCreateCommandQueue call: Failed to create command queue"
              << err << std::endl;

  // Read xclbin and create program
  std::vector<unsigned char> binary = readBinary(binaryFile);
  size_t binary_size = binary.size();
  const unsigned char *binary_data = binary.data();
  program = clCreateProgramWithBinary(
      context, 1, &device, &binary_size, &binary_data, NULL, &err);

  cl_mem_ext_ptr_t colExt, valueExt;
  colExt = {XCL_MEM_EXT_P2P_BUFFER, NULL, 0};
  valueExt = {XCL_MEM_EXT_P2P_BUFFER, NULL, 0};
  
  buffer_col =
      clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_EXT_PTR_XILINX,
                    BUFFER_SIZE * BULK_SIZE, &colExt, &err);
  col_ptr =
    (int64_t *)clEnqueueMapBuffer(command_queue, buffer_col, CL_TRUE, CL_MAP_READ, 0, BUFFER_SIZE * BULK_SIZE, 0, NULL, NULL, NULL);
  
  buffer_value =
    clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_EXT_PTR_XILINX,
                  BUFFER_SIZE * BULK_SIZE, &valueExt, &err);
  value_ptr =
    (int64_t *)clEnqueueMapBuffer(command_queue, buffer_value, CL_TRUE, CL_MAP_READ, 0, BUFFER_SIZE * BULK_SIZE, 0, NULL, NULL, NULL);

}