// XRT native APIs - https://xilinx.github.io/XRT/master/html/xrt_native_apis.html
// use with -I$XILINX_XRT/include and -L$XILINX_XRT/lib, -lxrt_coreutil -pthread

// IN CASE OF COMPILE ERROR @ BUILD STAGE USING VITIS IDE,
// TRY ADDING -lxrt_coreutil TO THE MAKE COMMAND

// XRT example includes these
#include <fcntl.h>
#include <fstream>
#include <iomanip>
#include <iosfwd>
#include <iostream>
#include <unistd.h>
#include <vector>
// includes for using XRT buffer objects
#include <xrt/xrt_device.h>
#include <experimental/xrt_xclbin.h>
#include <xrt/xrt_bo.h>
#include <xrt/xrt_kernel.h>
#include <experimental/xrt_ip.h>
//#include <experimental/aie.h>
//#include <experimental/graph.h>

// Returns 'rowptr', 'col', 'n_id'
std::tuple<torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_xrt_cpu(torch::Tensor rowptr, torch::Tensor rowcount,
                   torch::Tensor idx, int64_t num_neighbors, std::string path) {
    // ******************** TIPS ********************
    // 1. @path is the path to column information file, check "open column info"
    // 2. variable BUFFER_SIZE is used but not defined elsewhere -- define it somewhere
    // 3. CHECK_CPU() and CHECK_INPUT() are used in sample code as sanity check measures, but are not defined anywhere I can see
    
    CHECK_CPU(rowptr);
    CHECK_CPU(idx);
    CHECK_INPUT(idx.dim() == 1);
    
    auto rowptr_data = rowptr.data_ptr<int64_t>();
    auto col_data = col.data_ptr<int64_t>();
    auto rowcount_data = rowcount.data_ptr<int64_t>();
    auto idx_data = idx.data_ptr<int64_t>();

    auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
    auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
    out_rowptr_data[0] = 0;

    // buffer size in Bytes
    //int64_t buffersize = 4096;
    //int64_t numentry_per_buffer = buffersize / sizeof(int64_t);
    //std::vector<int64_t> temp_col_buffer(numentry_per_buffer);

    std::vector<int64_t> cols, n_ids;
    std::unordered_map<int64_t, int64_t> n_id_map;

    // checkpointing variables
    std::chrono::system_clock::time_point start, end;
    // g_perf measurements:
    // 0. init
    // 1. xrt::bo init & allocation
    // 3. P2P read into col_buffer with pread
    // 6. kernel execution
    // 7. sync: from device
    // 8. remaining algorithm
    // 9. close
    std::vector<std::vector<double> > g_perf(6);
   
    // init: gperf[0]
    start = std::chrono::high_resolution_clock::now();
    int64_t i;
    for (int64_t n = 0; n < idx.size(0); ++n) {
        i = idx_data[n];
        n_id_map[i] = n;
        n_ids.push_back(i);
    }

    // get XRT device ready
    auto device = xrt::device(0);
    auto uuid = device.load_xclbin("/home/via/jinha/xclbins/211023_with_interface.xclbin");
    auto kernel = xrt::kernel(device, uuid, "bandwidth");

    // open column info
    int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
    if((fd_col < 0)) {
        std::cout << "can't open " << path+"col" << std::endl;
        exit(1);
    }

    // allocate buffers using XRT API
    uint64_t NUM_SECTOR_PER_BLOCK_BITS = 9;
    uint64_t NUM_SECTOR_PER_BLOCK = 1 << NUM_SECTOR_PER_BLOCK_BITS;
    
    int64_t numentry_per_sector = BUFFER_SIZE / sizeof(int64_t);
    int64_t lba_offset, data_offset;
    end = std::chrono::high_resolution_clock::now();
    g_perf[0].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

    // xrt::bo init & allocation: gperf[1]
    start = std::chrono::high_resolution_clock::now();
    // ignore "num_neighbors < 0" or "replace" cases
    int64_t r, c, e, rc, offset = 0;
    // calculate "max" values
    int64_t max_rc = MAX_RC;
    int64_t max_col_buffer_size = MAX_RC * sizeof(int64_t);
    int64_t max_perm_buffer_size = num_neighbors * sizeof(int64_t);
    int64_t max_c_buffer_size = num_neighbors * sizeof(int64_t);
    // buffer allocations that were previously done inside the for-loop
    auto col_buffer = xrt::bo(device, max_col_buffer_size, xrt::bo::flags::p2p, kernel.group_id(0));
    auto perm_buffer = xrt::bo(device, max_perm_buffer_size, kernel.group_id(1));
    auto c_buffer = xrt::bo(device, BUFFER_SIZE, kernel.group_id(2));
    
    auto col_buffer_map = col_buffer.map<int64_t *>();
    auto perm_buffer_map = perm_buffer.map<int64_t *>();
    auto c_buffer_map = c_buffer.map<int64_t *>();
    end = std::chrono::high_resolution_clock::now();
    g_perf[1].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

    for (int64_t i = 0; i < idx.size(0); ++i) {
        r = idx_data[i];
        rc = rowcount_data[r];
        /*
        // sampling start for i (perm calculation using Robert Floyd algorithm): gperf[2]
        start = std::chrono::high_resolution_clock::now();
        std::unordered_set<int64_t> perm;
        if (rc <= num_neighbors) {
            // push all neighbors if not enough neighbors
            for (int64_t x = 0; x < rc; ++x) {
                //perm.insert(x);
                perm.insert(x + ((rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)));
            }
        }
        else {
            // Robert Floyd sampling algorithm
            for (int64_t x = rc - num_neighbors; x < rc; ++x) {
                if (!perm.insert(rand() % x).second) {
                    perm.insert(x + ((rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)));
                }
            }
        }
        end = std::chrono::high_resolution_clock::now();
        g_perf[2].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
        */
        // sampling done (for i)
        lba_offset = rowptr_data[r] * sizeof(int64_t) / NUM_SECTOR_PER_BLOCK;
        data_offset = rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK;
        int64_t transfer_size_bytes = ((data_offset + rc * sizeof(int64_t) + NUM_SECTOR_PER_BLOCK) >> NUM_SECTOR_PER_BLOCK_BITS) << NUM_SECTOR_PER_BLOCK_BITS;
        
        // load contents to col_buffer (P2P read?): gperf[3]
        start = std::chrono::high_resolution_clock::now();
        pread(fd_col, col_buffer_map, transfer_size_bytes, lba_offset * 512);
        end = std::chrono::high_resolution_clock::now();
        g_perf[3].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
        /*
        // load contents to perm_buffer: gperf[4]
        start = std::chrono::high_resolution_clock::now();
        uint64_t perm_copy_index = 0;
        for (const int64_t &p : perm) {
            perm_buffer_map[perm_copy_index] = p;
            ++perm_copy_index;
        }
        end = std::chrono::high_resolution_clock::now();
        g_perf[4].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
        
        // sync, to device: gperf[5]
        start = std::chrono::high_resolution_clock::now();
        perm_buffer.sync(XCL_BO_SYNC_BO_TO_DEVICE);
        end = std::chrono::high_resolution_clock::now();
        g_perf[5].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
        */

        // kernel execution: gperf[6]
        start = std::chrono::high_resolution_clock::now();
        auto run = kernel(col_buffer, perm_buffer, c_buffer, num_neighbor, rc, (rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t));
        run.wait();
        end = std::chrono::high_resolution_clock::now();
        g_perf[6].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
        
        // sync, from device: gperf[7]
        start = std::chrono::high_resolution_clock::now();
        perm_buffer.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
        c_buffer.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
        end = std::chrono::high_resolution_clock::now();
        g_perf[7].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

        // after kernel call --> values of c are calculated already: gperf[8]
        start = std::chrono::high_resolution_clock::now();
        loop_ind = 0;
        for (const int64_t &p : perm) {
            // fetch c accordingly from the buffer calculated by FPGA
            c = c_buffer_map[loop_ind];
            ++loop_ind;
            if (n_id_map.count(c) == 0) {
                // no duplicate node
                n_id_map[c] = n_ids.size();
                n_ids.push_back(c);
            }
            cols.push_back(n_id_map[c]);
        }
        // done setting values (for i)
        out_rowptr_data[i + 1] = cols.size();
        end = std::chrono::high_resolution_clock::now();
        g_perf[8].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
    }

    // return tuple of tensors: gperf[9]
    start = std::chrono::high_resolution_clock::now();
    int64_t n_len = n_ids.size();
    int64_t e_len = cols.size();
    auto col = torch::from_blob(cols.data(), {e_len}, rowptr.options()).clone();
    auto n_id = torch::from_blob(n_ids.data(), {n_len}, rowptr.options()).clone();

    // XRT buffer object is freed automatically
    close(fd_col);
    end = std::chrono::high_resolution_clock::now();
    g_perf[9].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

    // 0. init
    // 1. xrt::bo init & allocation
    // 3. P2P read into col_buffer with pread
    // 6. kernel execution
    // 7. sync: from device
    // 8. remaining algorithm
    // 9. close
    std::cout << "[sample_adj_xrt_cpu()] **** TOTAL TIME BREAKDOWN (AVERAGE) ****" << std::endl;
    std::cout << "[sample_adj_xrt_cpu()]                                  init: "
              << (double)std::accumulate(g_perf[0].begin, g_perf[0].end(), 0.0) / g_perf[0].size() << "us" << std::endl;
    std::cout << "[sample_adj_xrt_cpu()]             xrt::bo init & allocation: "
              << (double)std::accumulate(g_perf[1].begin, g_perf[1].end(), 0.0) / g_perf[1].size() << "us" << std::endl;
    std::cout << "[sample_adj_xrt_cpu()] P2P read into col_buffer with pread(): "
              << (double)std::accumulate(g_perf[3].begin, g_perf[3].end(), 0.0) / g_perf[3].size() << "us" << std::endl;
    std::cout << "[sample_adj_xrt_cpu()]                      kernel execution: "
              << (double)std::accumulate(g_perf[6].begin, g_perf[6].end(), 0.0) / g_perf[6].size() << "us" << std::endl;
    std::cout << "[sample_adj_xrt_cpu()]  sync after kernel call (FROM DEVICE): "
              << (double)std::accumulate(g_perf[7].begin, g_perf[7].end(), 0.0) / g_perf[7].size() << "us" << std::endl;
    std::cout << "[sample_adj_xrt_cpu()]         remaining algorithm execution: "
              << (double)std::accumulate(g_perf[8].begin, g_perf[8].end(), 0.0) / g_perf[8].size() << "us" << std::endl;
    std::cout << "[sample_adj_xrt_cpu()]                                 close: "
              << (double)std::accumulate(g_perf[9].begin, g_perf[9].end(), 0.0) / g_perf[9].size() << "us" << std::endl;

    return std::make_tuple(out_rowptr, col, n_id);
}
