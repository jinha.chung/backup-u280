/**********
Copyright (c) 2019, Xilinx, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********/
/*******************************************************************************
Description:
    In this Example, a vector addition implementation is done
*******************************************************************************/

// Includes
#include <ap_int.h>

#define DATA_SIZE 4096
typedef ap_int<64> intV_t;
typedef ap_int<512> floatV_t; // 512bit = 64B = 16 dim (minimum)

/* Sample implementation from earlier
extern "C" {

    // Vector Addition Kernel Implementation using dataflow
    // Arguments:
    //     in   (input)  --> Input Vector
    //     out  (output) --> Output Vector
    //     inc  (input)  --> Increment
    //     size (input)  --> Size of Vector in Integer

void adder(intV_t *in, intV_t *idx, intV_t *out, intV_t offset, int idx_size) {
#pragma HLS INTERFACE m_axi port = in offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = idx offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in bundle = control
#pragma HLS INTERFACE s_axilite port = idx bundle = control
#pragma HLS INTERFACE s_axilite port = out bundle = control
#pragma HLS INTERFACE s_axilite port = offset bundle = control
#pragma HLS INTERFACE s_axilite port = idx_size bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control

gather_node:
  for (int i = 0; i < idx_size; i++) {
#pragma HLS PIPELINE II = 1
    out[i] = in[offset + idx[i]];
  }  
}
}
*/


extern "C" {
/*
    Vector Addition Kernel Implementation using dataflow
    Needs to do: c = ((int64_t*)col_buffer)[p];
    Arguments:
        in_col    (input)  --> Input Vector
        in_perm   (input)  --> Input Vector
        out_c     (output) --> Output Vector
        idx_size  (input)  --> Size of Vector in Integer
*/
void adder(intV_t *in_col, intV_t *in_perm, intV_t *out_c, int idx_size) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_perm offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_perm bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = idx_size bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_node: 
  for (int i = 0; i < idx_size; ++i) {
#pragma HLS PIPELINE II = 1
    out_c[i] = in_col[in_perm[i]];
  }
}
}
