/**
* Copyright (C) 2019-2021 Xilinx, Inc
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/

// OpenCL utility layer include
//#include "cmdlineparser.h"
//#include "xcl2.hpp"
#include <fcntl.h>
#include <fstream>
#include <iomanip>
#include <iosfwd>
#include <iostream>
#include <unistd.h>
#include <vector>

// MODIFICATIONS - includes for my code
#include <xrt/xrt_device.h>
#include <experimental/xrt_xclbin.h>
#include <xrt/xrt_bo.h>
#include <xrt/xrt_kernel.h>
#include <experimental/xrt_ip.h>

size_t max_buffer = 16 * 1024 * 1024;
size_t min_buffer = 4 * 1024;
size_t max_size = 128 * 1024 * 1024; // 128MB


// MODIFICATIONS - kernel functionality check, according to Vitis XRT API example:
// https://github.com/Xilinx/Vitis_Accel_Examples/blob/master/host_xrt/p2p_simple_xrt/src/host.cpp
void test_adder() {
    std::cout << "[TEST_ADDER] Starting. Entering test_adder()" << std::endl;

    // 1. get device and kernel ready
    std::cout << "[TEST_ADDER] Getting device and kernel ready" << std::endl;
    auto device = xrt::device(0);
    std::cout << "[TEST_ADDER] Checkpoint - xrt::device() complete" << std::endl;
    auto uuid = device.load_xclbin("/home/via/gnn_workspace/jh_gnn_211018/Hardware/bandwidth.xclbin");
    std::cout << "[TEST_ADDER] Checkpoint - device.load_xclbin() complete" << std::endl;
    auto krnl = xrt::kernel(device, uuid, "bandwidth");
    
    // 2. generate inputs
    std::cout << "[TEST_ADDER] Generating host-side inputs" << std::endl;
    uint64_t BUFFER_SIZE = 1024;
    void *input_col, *input_perm, *output_c;
    //input_col = calloc(BUFFER_SIZE, sizeof(uint64_t));
    //input_perm = calloc(BUFFER_SIZE, sizeof(uint64_t));
    //output_c = calloc(BUFFER_SIZE, sizeof(uint64_t));
    posix_memalign(&input_col, 4096, BUFFER_SIZE * sizeof(uint64_t));
    posix_memalign(&input_perm, 4096, BUFFER_SIZE * sizeof(uint64_t));
    posix_memalign(&output_c, 4096, BUFFER_SIZE * sizeof(uint64_t));
/*    
    for (uint64_t i = 0; i < BUFFER_SIZE; ++i) {
        ((uint64_t *)input_col)[i] = (i * i) % BUFFER_SIZE;
        ((uint64_t *)input_perm)[i] = BUFFER_SIZE - i - 1;
    }
*/
    // 3. generate buffers for FPGA
    std::cout << "[TEST_ADDER] Generating FPGA-side buffers" << std::endl;
    /*
    auto input_col_buf = xrt::bo(device,
                                 (uint64_t *)input_col,
                                 BUFFER_SIZE,
                                 xrt::bo::flags::p2p,
                                 krnl.group_id(0));
    auto input_perm_buf = xrt::bo(device,
                                  (uint64_t *)input_perm,
                                  BUFFER_SIZE,
                                  xrt::bo::flags::p2p,
                                  krnl.group_id(1));
    auto output_c_buf = xrt::bo(device,
                                (uint64_t *)output_c,
                                BUFFER_SIZE,
                                xrt::bo::flags::p2p,
                                krnl.group_id(2));

    */

    // get my shit together!
    auto input_col_buf = xrt::bo(device, (uint64_t *)input_col, BUFFER_SIZE * sizeof(uint64_t), krnl.group_id(0));
    auto input_perm_buf = xrt::bo(device, (uint64_t *)input_perm, BUFFER_SIZE * sizeof(uint64_t), krnl.group_id(1));
    auto output_c_buf = xrt::bo(device, (uint64_t *)output_c, BUFFER_SIZE * sizeof(uint64_t), krnl.group_id(2));

    auto input_col_buf_map = input_col_buf.map<uint64_t *>();
    auto input_perm_buf_map = input_perm_buf.map<uint64_t *>();
    auto output_c_buf_map = output_c_buf.map<uint64_t *>();
    
    for (uint64_t i = 0; i < BUFFER_SIZE; ++i) {
        ((uint64_t *)input_col_buf_map)[i] = (i * i) % BUFFER_SIZE;
        ((uint64_t *)input_perm_buf_map)[i] = BUFFER_SIZE - i - 1;
	((uint64_t *)output_c_buf_map)[i] = 10000;
    }
    std::cout << "[TEST_ADDER] After initializing buffer values:"
              << "input_col_buf[0]: " << ((uint64_t *)input_col_buf_map)[0]
              << ", input_col_buf[1023]: " << ((uint64_t *)input_col_buf_map)[BUFFER_SIZE - 1]
              << ", output_c_buf[0]: " << ((uint64_t *)output_c_buf_map)[0]
              << ", and output_c_buf[1023]: " << ((uint64_t *)output_c_buf_map)[BUFFER_SIZE - 1]
              << std::endl;

    // sync
    input_col_buf.sync(XCL_BO_SYNC_BO_TO_DEVICE);
    input_perm_buf.sync(XCL_BO_SYNC_BO_TO_DEVICE);
    
    std::cout << "[TEST_ADDER] After syncing buffer values:"
              << "input_col_buf[0]: " << ((uint64_t *)input_col_buf_map)[0]
              << ", input_col_buf[1023]: " << ((uint64_t *)input_col_buf_map)[BUFFER_SIZE - 1]
              << ", output_c_buf[0]: " << ((uint64_t *)output_c_buf_map)[0]
              << ", and output_c_buf[1023]: " << ((uint64_t *)output_c_buf_map)[BUFFER_SIZE - 1]
              << std::endl;

    // 4. run kernel
    std::cout << "[TEST_ADDER] Running kernel" << std::endl;
    auto run = krnl(input_col_buf, input_perm_buf, output_c_buf, (int)BUFFER_SIZE);
    run.wait();

    // sync
    output_c_buf.sync(XCL_BO_SYNC_BO_FROM_DEVICE);

    // 5. check output
    std::cout << "[TEST_ADDER] Checking outputs" << std::endl;
    uint64_t total_num_wrong = 0;
    for (uint64_t i = 0; i < BUFFER_SIZE; ++i) {
        //if (((uint64_t *)output_c)[i] != (((uint64_t *)input_col)[((uint64_t *)input_perm)[i]])) {
	uint64_t *input_col_ptr = (uint64_t *)input_col;
	uint64_t *input_perm_ptr = (uint64_t *)input_perm;
	uint64_t *output_c_ptr = (uint64_t *)output_c;
	if (input_col_ptr[input_perm_ptr[i]] != output_c_ptr[i]) {
        //if (((uint64_t *)input_col_buf_map)[((uint64_t *)input_perm_buf_map)[i]] != ((uint64_t *)output_c_buf_map)[i]) {
            std::cout << "[TEST_ADDER] WRONG @ INDEX " << i << ": "
		      //<< "should be " << ((uint64_t *)input_col)[((uint64_t *)input_perm)[i]]
		      //<< ", but got " << ((int64_t *)output_c)[i] << " instead"
                      << "should be " << ((uint64_t *)input_col_buf_map)[((uint64_t *)input_perm_buf_map)[i]]
                      << ", but got " << ((uint64_t *)output_c_buf_map)[i] << " instead"
		      //<< "should be " << input_col_ptr[input_perm_ptr[i]]
		      //<< ", but got " << output_c_ptr[i] << " instead"
		      << std::endl;
            ++total_num_wrong;
        }
    }
    std::cout << "[TEST_ADDER] Total of " << total_num_wrong << " wrong answers" << std::endl;

    // 6. finished
    std::cout << "[TEST_ADDER] Finished. Leaving test_adder()" << std::endl;
}

int main(int argc, char** argv) {

    test_adder();

    return 0;
}

/*
int main(int argc, char** argv) {
    // Command Line Parser
    sda::utils::CmdLineParser parser;

    // Switches
    // ************** //"<Full Arg>",  "<Short Arg>", "<Description>", "<Default>"
    parser.addSwitch("--xclbin_file", "-x", "input binary file string", "");
    parser.addSwitch("--file_path", "-p", "file path string", "");
    parser.addSwitch("--input_file", "-f", "input file string", "");
    parser.parse(argc, argv);

    // Read settings
    auto binaryFile = parser.value("xclbin_file");
    std::string filepath = parser.value("file_path");
    std::string filename;

    if (argc < 5) {
        parser.printHelp();
        return EXIT_FAILURE;
    }

    if (filepath.empty()) {
        std::cout << "\nWARNING: As file path is not provided using -p option, going with -f option which is local "
                     "file testing. Please use -p option, if looking for actual p2p operation on NVMe drive.\n";
        filename = parser.value("input_file");
    } else {
        std::cout << "\nWARNING: Ignoring -f option when -p options is set. -p has high precedence over -f.\n";
        filename = filepath;
    }

    int nvmeFd = -1;
    if (xcl::is_emulation()) {
        max_buffer = 16 * 1024;
    }

    cl_int err;
    cl::Context context;
    cl::CommandQueue q;
    std::vector<int, aligned_allocator<int> > source_input_A(max_buffer);

    // OPENCL HOST CODE AREA START
    // get_xil_devices() is a utility API which will find the xilinx
    // platforms and will return list of devices connected to Xilinx platform
    auto devices = xcl::get_xil_devices();
    // read_binary_file() is a utility API which will load the binaryFile
    // and will return the pointer to file buffer.
    auto fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{fileBuf.data(), fileBuf.size()}};
    bool valid_device = false;
    cl::Program program;

    for (unsigned int i = 0; i < devices.size(); i++) {
        auto device = devices[i];
        if (xcl::is_hw_emulation()) {
            auto device_name = device.getInfo<CL_DEVICE_NAME>();
            if (device_name.find("2018") != std::string::npos) {
                std::cout << "[INFO]: The example is not supported for " << device_name
                          << " this platform for hw_emu. Please try other flows." << '\n';
                return EXIT_SUCCESS;
            }
        }
        // Creating Context and Command Queue for selected Device
        OCL_CHECK(err, context = cl::Context(device, nullptr, nullptr, nullptr, &err));
        OCL_CHECK(err, q = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err));
        std::cout << "Trying to program device[" << i << "]: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
        program = cl::Program(context, {device}, bins, nullptr, &err);
        if (err != CL_SUCCESS) {
            std::cout << "Failed to program device[" << i << "] with xclbin file!\n";
        } else {
            std::cout << "Device[" << i << "]: program successful!\n";
            valid_device = true;
            break; // we break because we found a valid device
        }
    }
    if (!valid_device) {
        std::cerr << "Failed to program any device found, exit!\n";
        exit(EXIT_FAILURE);
    }

    // P2P transfer from host to SSD
    std::cout << "############################################################\n";
    std::cout << "                  Writing data to SSD                       \n";
    std::cout << "############################################################\n";
    // Get access to the NVMe SSD.
    nvmeFd = open(filename.c_str(), O_RDWR | O_DIRECT);
    if (nvmeFd < 0) {
        std::cerr << "ERROR: open " << filename << "failed: " << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "INFO: Successfully opened NVME SSD " << filename << std::endl;
    p2p_host_to_ssd(nvmeFd, context, q, program, source_input_A);
    (void)close(nvmeFd);

    // P2P transfer from SSD to host
    std::cout << "############################################################\n";
    std::cout << "                  Reading data from SSD                       \n";
    std::cout << "############################################################\n";

    nvmeFd = open(filename.c_str(), O_RDWR | O_DIRECT);
    if (nvmeFd < 0) {
        std::cerr << "ERROR: open " << filename << "failed: " << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "INFO: Successfully opened NVME SSD " << filename << std::endl;

    p2p_ssd_to_host(nvmeFd, context, q, program, &source_input_A);

    (void)close(nvmeFd);

    std::cout << "TEST PASSED" << std::endl;
    return EXIT_SUCCESS;
}
*/
