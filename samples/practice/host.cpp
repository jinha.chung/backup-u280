/**
* Copyright (C) 2019-2021 Xilinx, Inc
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/

// OpenCL utility layer include
//#include "cmdlineparser.h"
//#include "xcl2.hpp"
#include <fcntl.h>
#include <fstream>
#include <iomanip>
#include <iosfwd>
#include <iostream>
#include <unistd.h>
#include <vector>

// MODIFICATIONS - includes for my code
#include <xrt/xrt_device.h>
#include <experimental/xrt_xclbin.h>
#include <xrt/xrt_bo.h>
#include <xrt/xrt_kernel.h>
#include <experimental/xrt_ip.h>

size_t max_buffer = 16 * 1024 * 1024;
size_t min_buffer = 4 * 1024;
size_t max_size = 128 * 1024 * 1024; // 128MB

/*
void p2p_host_to_ssd(int& nvmeFd,
                     cl::Context context,
                     cl::CommandQueue q,
                     cl::Program program,
                     std::vector<int, aligned_allocator<int> > source_input_A) {
    int err;
    int ret = 0;
    size_t vector_size_bytes = sizeof(int) * max_buffer;

    cl::Kernel krnl_adder;
    // Allocate Buffer in Global Memory
    cl_mem_ext_ptr_t outExt;
    outExt = {XCL_MEM_EXT_P2P_BUFFER, nullptr, 0};

    OCL_CHECK(err, cl::Buffer input_a(context, CL_MEM_READ_ONLY, vector_size_bytes, nullptr, &err));
    OCL_CHECK(err,
              cl::Buffer p2pBo(context, CL_MEM_WRITE_ONLY | CL_MEM_EXT_PTR_XILINX, vector_size_bytes, &outExt, &err));
    OCL_CHECK(err, krnl_adder = cl::Kernel(program, "bandwidth", &err));

    int* inputPtr = (int*)q.enqueueMapBuffer(input_a, CL_TRUE, CL_MAP_WRITE | CL_MAP_READ, 0, vector_size_bytes,
                                             nullptr, nullptr, &err);

    for (uint32_t i = 0; i < max_buffer; i++) {
        inputPtr[i] = source_input_A[i];
    }
    q.finish();

    // Set the Kernel Arguments
    OCL_CHECK(err, err = krnl_adder.setArg(0, input_a));

    // Copy input data to device global memory
    OCL_CHECK(err, err = q.enqueueMigrateMemObjects({input_a}, 0));

    // Launch the Kernel
    OCL_CHECK(err, err = q.enqueueTask(krnl_adder));

    std::cout << "\nMap P2P device buffers to host access pointers\n" << std::endl;
    void* p2pPtr = q.enqueueMapBuffer(p2pBo,                      // buffer
                                      CL_TRUE,                    // blocking call
                                      CL_MAP_WRITE | CL_MAP_READ, // Indicates we will be writing
                                      0,                          // buffer offset
                                      vector_size_bytes,          // size in bytes
                                      nullptr, nullptr,
                                      &err); // error code

    std::cout << "Start P2P Write of various buffer sizes from device buffers to SSD\n" << std::endl;
    for (size_t bufsize = min_buffer; bufsize <= vector_size_bytes; bufsize *= 2) {
        std::string size_str = xcl::convert_size(bufsize);

        int iter = max_size / bufsize;
        if (xcl::is_emulation()) {
            iter = 2; // Reducing iteration to run faster in emulation flow.
        }
        std::chrono::high_resolution_clock::time_point p2pStart = std::chrono::high_resolution_clock::now();
        for (int i = 0; i < iter; i++) {
            ret = pwrite(nvmeFd, (void*)p2pPtr, bufsize, 0);
            if (ret == -1) std::cout << "P2P: write() failed, err: " << ret << ", line: " << __LINE__ << std::endl;
        }
        std::chrono::high_resolution_clock::time_point p2pEnd = std::chrono::high_resolution_clock::now();
        cl_ulong p2pTime = std::chrono::duration_cast<std::chrono::microseconds>(p2pEnd - p2pStart).count();
        ;
        double dnsduration = (double)p2pTime;
        double dsduration = dnsduration / ((double)1000000);
        double gbpersec = (iter * bufsize / dsduration) / ((double)1024 * 1024 * 1024);
        std::cout << "Buffer = " << size_str << " Iterations = " << iter << " Throughput = " << std::setprecision(2)
                  << std::fixed << gbpersec << "GB/s\n";
    }
}

void p2p_ssd_to_host(int& nvmeFd,
                     cl::Context context,
                     cl::CommandQueue q,
                     cl::Program program,
                     std::vector<int, aligned_allocator<int> >* source_input_A) {
    int err;
    size_t vector_size_bytes = sizeof(int) * max_buffer;

    cl::Kernel krnl_adder1;
    // Allocate Buffer in Global Memory
    cl_mem_ext_ptr_t inExt;
    inExt = {XCL_MEM_EXT_P2P_BUFFER, nullptr, 0};

    OCL_CHECK(err, cl::Buffer buffer_input(context, CL_MEM_READ_ONLY | CL_MEM_EXT_PTR_XILINX, vector_size_bytes, &inExt,
                                           &err));
    OCL_CHECK(err, krnl_adder1 = cl::Kernel(program, "bandwidth", &err));
    // Set the Kernel Arguments
    OCL_CHECK(err, err = krnl_adder1.setArg(0, buffer_input));

    std::cout << "\nMap P2P device buffers to host access pointers\n" << std::endl;
    void* p2pPtr1 = q.enqueueMapBuffer(buffer_input,      // buffer
                                       CL_TRUE,           // blocking call
                                       CL_MAP_READ,       // Indicates we will be writing
                                       0,                 // buffer offset
                                       vector_size_bytes, // size in bytes
                                       nullptr, nullptr,
                                       &err); // error code

    std::cout << "Start P2P Read of various buffer sizes from SSD to device buffers\n" << std::endl;
    for (size_t bufsize = min_buffer; bufsize <= vector_size_bytes; bufsize *= 2) {
        std::string size_str = xcl::convert_size(bufsize);

        int iter = max_size / bufsize;
        if (xcl::is_emulation()) {
            iter = 2; // Reducing iteration to run faster in emulation flow.
        }
        std::chrono::high_resolution_clock::time_point p2pStart = std::chrono::high_resolution_clock::now();
        for (int i = 0; i < iter; i++) {
            if (pread(nvmeFd, (void*)p2pPtr1, bufsize, 0) <= 0) {
                std::cerr << "ERR: pread failed: "
                          << " error: " << strerror(errno) << std::endl;
                exit(EXIT_FAILURE);
            }
        }
        std::chrono::high_resolution_clock::time_point p2pEnd = std::chrono::high_resolution_clock::now();
        cl_ulong p2pTime = std::chrono::duration_cast<std::chrono::microseconds>(p2pEnd - p2pStart).count();
        ;
        double dnsduration = (double)p2pTime;
        double dsduration = dnsduration / ((double)1000000);
        double gbpersec = (iter * bufsize / dsduration) / ((double)1024 * 1024 * 1024);
        std::cout << "Buffer = " << size_str << " Iterations = " << iter << " Throughput = " << std::setprecision(2)
                  << std::fixed << gbpersec << "GB/s\n";
    }
}
*/

/*
// MODIFICATIONS - check functionality of my kernel
void test_adder() {
    uint64_t BUFFER_SIZE = 1024;
    void *input_col, *input_perm, *output_c;
    posix_memalign(&input_col, 4096, BUFFER_SIZE * sizeof(int64_t));
    posix_memalign(&input_perm, 4096, BUFFER_SIZE * sizeof(int64_t));
    posix_memalign(&output_c, 4096, BUFFER_SIZE * sizeof(int64_t)); // necessary for output buffer?
    // 1. generate inputs
    for (uint64_t i = 0; i < BUFFER_SIZE; ++i) {
        ((uint64_t *)input_col)[i] = (i * i) % BUFFER_SIZE;
        ((uint64_t *)input_perm)[i] = BUFFER_SIZE - i - 1;
    }
    // 2. get device ready and init kernel
    auto device = xrt::device(0);
    //auto uuid = device.load_xclbin("/home/via/gnn_workspace/jh_gnn_211018/Hardware/bandwidth.xclbin");
    auto uuid = device.load_xclbin("/home/via/gnn_workspace/jh_gnn_211018/Emulation-HW/bandwidth.xclbin");
    auto krnl = xrt::kernel(device, uuid, "adder");
    // 3. generate buffers for FPGA
    auto input_col_buf = xrt::bo(device,
                                 input_col,
                                 BUFFER_SIZE,
                                 xrt::bo::flags::p2p,
                                 krnl.group_id(0));
    auto input_perm_buf = xrt::bo(device,
                                  input_perm,
                                  BUFFER_SIZE,
                                  xrt::bo::flags::p2p,
                                  krnl.group_id(1));
    auto output_c_buf = xrt::bo(device,
                                output_c,
                                BUFFER_SIZE,
                                xrt::bo::flags::p2p,
                                krnl.group_id(2));
    // 4. run kernel
    auto run = kernel(input_col_buf, input_perm_buf, output_c_buf, (int)BUFFER_SIZE);
}
*/

// MODIFICATIONS - kernel functionality check, according to Vitis XRT API example:
// https://github.com/Xilinx/Vitis_Accel_Examples/blob/master/host_xrt/p2p_simple_xrt/src/host.cpp
void test_adder() {
    std::cout << "[TEST_ADDER] Starting. Entering test_adder()" << std::endl;

    // 1. get device and kernel ready
    std::cout << "[TEST_ADDER] Getting device and kernel ready" << std::endl;
    auto device = xrt::device(0);
    std::cout << "[TEST_ADDER] Checkpoint - xrt::device() complete" << std::endl;
    auto uuid = device.load_xclbin("/home/via/gnn_workspace/jh_gnn_211018/Emulation-HW/bandwidth.xclbin");
    std::cout << "[TEST_ADDER] Checkpoint - device.load_xclbin() complete" << std::endl;
    auto krnl = xrt::kernel(device, uuid, "bandwidth");
    
    // 2. generate inputs
    std::cout << "[TEST_ADDER] Generating host-side inputs" << std::endl;
    uint64_t BUFFER_SIZE = 1024;
    uint64_t *input_col, *input_perm, *output_c;
    
    for (uint64_t i = 0; i < BUFFER_SIZE; ++i) {
        input_col[i] = (i * i) % BUFFER_SIZE;
        input_perm[i] = BUFFER_SIZE - i - 1;
    }

    // 3. generate buffers for FPGA
    std::cout << "[TEST_ADDER] Generating FPGA-side buffers" << std::endl;
    auto input_col_buf = xrt::bo(device,
                                 input_col,
                                 BUFFER_SIZE,
                                 xrt::bo::flags::p2p,
                                 krnl.group_id(0));
    auto input_perm_buf = xrt::bo(device,
                                  input_perm,
                                  BUFFER_SIZE,
                                  xrt::bo::flags::p2p,
                                  krnl.group_id(1));
    auto output_c_buf = xrt::bo(device,
                                output_c,
                                BUFFER_SIZE,
                                xrt::bo::flags::p2p,
                                krnl.group_id(2));

    // 4. run kernel
    std::cout << "[TEST_ADDER] Running kernel" << std::endl;
    auto run = krnl(input_col_buf, input_perm_buf, output_c_buf, (int)BUFFER_SIZE);
    run.wait();

    // 5. check output
    std::cout << "[TEST_ADDER] Checking outputs" << std::endl;
    uint64_t total_num_wrong = 0;
    for (uint64_t i = 0; i < BUFFER_SIZE; ++i) {
        if (output_c[i] != (input_col[input_perm[i]])) {
            std::cout << "[TEST_ADDER] WRONG @ INDEX " << i << std::endl;
            ++total_num_wrong;
        }
    }
    std::cout << "[TEST_ADDER] Total of " << total_num_wrong << " wrong answers" << std::endl;

    // 6. finished
    std::cout << "[TEST_ADDER] Finished. Leaving test_adder()" << std::endl;
}

int main(int argc, char** argv) {

    test_adder();

    return 0;
}

/*
int main(int argc, char** argv) {
    // Command Line Parser
    sda::utils::CmdLineParser parser;

    // Switches
    // ************** //"<Full Arg>",  "<Short Arg>", "<Description>", "<Default>"
    parser.addSwitch("--xclbin_file", "-x", "input binary file string", "");
    parser.addSwitch("--file_path", "-p", "file path string", "");
    parser.addSwitch("--input_file", "-f", "input file string", "");
    parser.parse(argc, argv);

    // Read settings
    auto binaryFile = parser.value("xclbin_file");
    std::string filepath = parser.value("file_path");
    std::string filename;

    if (argc < 5) {
        parser.printHelp();
        return EXIT_FAILURE;
    }

    if (filepath.empty()) {
        std::cout << "\nWARNING: As file path is not provided using -p option, going with -f option which is local "
                     "file testing. Please use -p option, if looking for actual p2p operation on NVMe drive.\n";
        filename = parser.value("input_file");
    } else {
        std::cout << "\nWARNING: Ignoring -f option when -p options is set. -p has high precedence over -f.\n";
        filename = filepath;
    }

    int nvmeFd = -1;
    if (xcl::is_emulation()) {
        max_buffer = 16 * 1024;
    }

    cl_int err;
    cl::Context context;
    cl::CommandQueue q;
    std::vector<int, aligned_allocator<int> > source_input_A(max_buffer);

    // OPENCL HOST CODE AREA START
    // get_xil_devices() is a utility API which will find the xilinx
    // platforms and will return list of devices connected to Xilinx platform
    auto devices = xcl::get_xil_devices();
    // read_binary_file() is a utility API which will load the binaryFile
    // and will return the pointer to file buffer.
    auto fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{fileBuf.data(), fileBuf.size()}};
    bool valid_device = false;
    cl::Program program;

    for (unsigned int i = 0; i < devices.size(); i++) {
        auto device = devices[i];
        if (xcl::is_hw_emulation()) {
            auto device_name = device.getInfo<CL_DEVICE_NAME>();
            if (device_name.find("2018") != std::string::npos) {
                std::cout << "[INFO]: The example is not supported for " << device_name
                          << " this platform for hw_emu. Please try other flows." << '\n';
                return EXIT_SUCCESS;
            }
        }
        // Creating Context and Command Queue for selected Device
        OCL_CHECK(err, context = cl::Context(device, nullptr, nullptr, nullptr, &err));
        OCL_CHECK(err, q = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err));
        std::cout << "Trying to program device[" << i << "]: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
        program = cl::Program(context, {device}, bins, nullptr, &err);
        if (err != CL_SUCCESS) {
            std::cout << "Failed to program device[" << i << "] with xclbin file!\n";
        } else {
            std::cout << "Device[" << i << "]: program successful!\n";
            valid_device = true;
            break; // we break because we found a valid device
        }
    }
    if (!valid_device) {
        std::cerr << "Failed to program any device found, exit!\n";
        exit(EXIT_FAILURE);
    }

    // P2P transfer from host to SSD
    std::cout << "############################################################\n";
    std::cout << "                  Writing data to SSD                       \n";
    std::cout << "############################################################\n";
    // Get access to the NVMe SSD.
    nvmeFd = open(filename.c_str(), O_RDWR | O_DIRECT);
    if (nvmeFd < 0) {
        std::cerr << "ERROR: open " << filename << "failed: " << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "INFO: Successfully opened NVME SSD " << filename << std::endl;
    p2p_host_to_ssd(nvmeFd, context, q, program, source_input_A);
    (void)close(nvmeFd);

    // P2P transfer from SSD to host
    std::cout << "############################################################\n";
    std::cout << "                  Reading data from SSD                       \n";
    std::cout << "############################################################\n";

    nvmeFd = open(filename.c_str(), O_RDWR | O_DIRECT);
    if (nvmeFd < 0) {
        std::cerr << "ERROR: open " << filename << "failed: " << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "INFO: Successfully opened NVME SSD " << filename << std::endl;

    p2p_ssd_to_host(nvmeFd, context, q, program, &source_input_A);

    (void)close(nvmeFd);

    std::cout << "TEST PASSED" << std::endl;
    return EXIT_SUCCESS;
}
*/
