/**
* Copyright (C) 2019-2021 Xilinx, Inc
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*/

#include <ap_int.h>

#define DATA_SIZE 4096
typedef ap_int<64> intV_t;
typedef ap_int<512> floatV_t; // 512bit = 64B = 16 dim (minimum)

/*
extern "C" {
void bandwidth(unsigned int* buffer0) {
    // Intentional empty kernel as this example doesn't require actual
    // kernel to work.
}
}
*/

extern "C" {
/*
    Vector Addition Kernel Implementation using dataflow
    Needs to do: c = ((int64_t*)col_buffer)[p];
    Arguments:
        in_col    (input)  --> Input Vector
        in_perm   (input)  --> Input Vector
        out_c     (output) --> Output Vector
        idx_size  (input)  --> Size of Vector in Integer
*/
void bandwidth(intV_t *in_col, intV_t *in_perm, intV_t *out_c, int idx_size) {
#pragma HLS INTERFACE m_axi port = in_col offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = in_perm offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_c offset = slave bundle = gmem
#pragma HLS INTERFACE s_axilite port = in_col bundle = control
#pragma HLS INTERFACE s_axilite port = in_perm bundle = control
#pragma HLS INTERFACE s_axilite port = out_c bundle = control
#pragma HLS INTERFACE s_axilite port = idx_size bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control
gather_node:
  for (int i = 0; i < idx_size; ++i) {
#pragma HLS PIPELINE II = 1
    out_c[i] = in_col[in_perm[i]];
  }
}
}

