// XRT native APIs - https://xilinx.github.io/XRT/master/html/xrt_native_apis.html
// use with -I$XILINX_XRT/include and -L$XILINX_XRT/lib, -lxrt_coreutil -pthread
#include <xrt/xrt_device.h>
#include <experimental/xrt_xclbin.h>
#include <xrt/xrt_bo.h>
#include <xrt/xrt_kernel.h>
#include <experimental/xrt_ip.h>
//#include <experimental/aie.h>
//#include <experimental/graph.h>

#define USE_FPGA

// Returns 'rowptr', 'col', 'n_id'
std::tuple<torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_cpu(torch::Tensor rowptr, torch::Tensor col, torch::Tensor rowcount,
               torch::Tensor idx, int64_t num_neighbors) {
    auto rowptr_data = rowptr.data_ptr<int64_t>();
    auto col_data = col.data_ptr<int64_t>();
    auto rowcount_data = rowcount.data_ptr<int64_t>();
    auto idx_data = idx.data_ptr<int64_t>();

    auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
    auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
    out_rowptr_data[0] = 0;

    std::vector<int64_t> cols, n_ids;
    std::unordered_map<int64_t, int64_t> n_id_map;
    //std::vector<int64_t> e_ids;
    //std::vector<std::vector<double> > g_perf(3); // timestamps for time breakdown

    int64_t i;
    for (int64_t n = 0; n < idx.size(0); ++n) {
        i = idx_data[n];
        n_id_map[i] = n;
        n_ids.push_back(i);
    }

    // ignore "num_neighbors < 0" or "replace" cases
    int64_t r, c, e, rc, offset = 0;
    for (int64_t i = 0; i < idx.size(0); ++i) {
        r = idx_data[i];
        rc = rowcount_data[r];
        // sampling start for i
        std::unordered_set<int64_t> perm;
        if (rc <= num_neighbors) {
            // push all neighbors if not enough neighbors
            for (int64_t x = 0; x < rc; ++x) {
                perm.insert(x);
            }
        }
        else {
            // Robert Floyd sampling algorithm
            for (int64_t x = rc - num_neighbors; x < rc; ++x) {
                if (!perm.insert(rand() % x).second) {
                    perm.insert(x);
                }
            }
        }
        // sampling done (for i)
        // set values with sampled points for i
        int count = 0;
        for (const int64_t &p : perm) {
            e = rowptr_data[r] + p;
            c = col_data[e];

            if (n_id_map.count(c) == 0) {
                // no duplicate node
                n_id_map[c] = n_ids.size();
                n_ids.push_back(c);
            }
            cols.push_back(n_id_map[c]);
        }
        // done setting values (for i)
        out_rowptr_data[i + 1] = cols.size();
    }

    // return tuple of tensors
    int64_t n_len = n_ids.size();
    int64_t e_len = cols.size();
    col = torch::from_blob(cols.data(), {e_len}, col.options()).clone();
    auto n_id = torch::from_blob(n_ids.data(), {n_len}, col.options()).clone();

    return std::make_tuple(out_rowptr, col, n_id);
}

// Returns 'rowptr', 'col', 'n_id'
std::tuple<torch::Tensor, torch::Tensor, torch::Tensor>
sample_adj_xrt_cpu(torch::Tensor rowptr, torch::Tensor rowcount,
                   torch::Tensor idx, int64_t num_neighbors, std::string path) {
    CHECK_CPU(rowptr);
    CHECK_CPU(idx);
    CHECK_INPUT(idx.dim() == 1);
    
    auto rowptr_data = rowptr.data_ptr<int64_t>();
    auto col_data = col.data_ptr<int64_t>();
    auto rowcount_data = rowcount.data_ptr<int64_t>();
    auto idx_data = idx.data_ptr<int64_t>();

    auto out_rowptr = torch::empty(idx.size(0) + 1, rowptr.options());
    auto out_rowptr_data = out_rowptr.data_ptr<int64_t>();
    out_rowptr_data[0] = 0;

    // buffer size in Bytes
    //int64_t buffersize = 4096;
    //int64_t numentry_per_buffer = buffersize / sizeof(int64_t);
    //std::vector<int64_t> temp_col_buffer(numentry_per_buffer);

    std::vector<int64_t> cols, n_ids;
    std::unordered_map<int64_t, int64_t> n_id_map;
    //int64_t lba_offset, data_offset;
    std::vector<std::vector<double> > g_perf(6);
    
    int64_t i;
    for (int64_t n = 0; n < idx.size(0); ++n) {
        i = idx_data[n];
        n_id_map[i] = n;
        n_ids.push_back(i);
    }

    // get XRT device ready
    auto device = xrt::device(0);
    // TODO: fix xclbin name and kernel name
    auto uuid = device.load_xclbin("/home/via/somewhere.xclbin");
    auto kernel = xrt::kernel(device, uuid, "kernel_name");

    // open column info
    int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
    if((fd_col < 0)) {
        std::cout << "can't open " << path+"col" << std::endl;
        exit(1);
    }

    // allocate buffers using XRT API
    // TODO: where is BUFFER_SIZE defined?
    uint64_t NUM_SECTOR_PER_BLOCK_BITS = 9;
    uint64_t NUM_SECTOR_PER_BLOCK = 1 << NUM_SECTOR_PER_BLOCK_BITS;
    // TODO: choose one method of the two
    // TODO: group_id(X) means "connected to argument X of kernel" -- choose X wisely
    //auto col_buffer = xrt::bo(device, BUFFER_SIZE, XCL_BO_FLAGS_NONE, kernel.group_id(0));
    auto col_buffer = xrt::bo(device, BUFFER_SIZE, xrt::bo::flags::p2p, kernel.group_id(0));
    
    int64_t numentry_per_sector = BUFFER_SIZE / sizeof(int64_t);
    int64_t lba_offset, data_offset;

    // ignore "num_neighbors < 0" or "replace" cases
    int64_t r, c, e, rc, offset = 0;
    for (int64_t i = 0; i < idx.size(0); ++i) {
        r = idx_data[i];
        rc = rowcount_data[r];
        // sampling start for i
        std::unordered_set<int64_t> perm;
        if (rc <= num_neighbors) {
            // push all neighbors if not enough neighbors
            for (int64_t x = 0; x < rc; ++x) {
                //perm.insert(x);
                perm.insert(x + ((rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)));
            }
        }
        else {
            // Robert Floyd sampling algorithm
            for (int64_t x = rc - num_neighbors; x < rc; ++x) {
                if (!perm.insert(rand() % x).second) {
                    perm.insert(x + ((rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK) / sizeof(int64_t)));
                }
            }
        }
        // sampling done (for i)
        lba_offset = rowptr_data[r] * sizeof(int64_t) / NUM_SECTOR_PER_BLOCK;
        data_offset = rowptr_data[r] * sizeof(int64_t) % NUM_SECTOR_PER_BLOCK;
        int64_t transfer_size_bytes = ((data_offset + rc * sizeof(int64_t) + NUM_SECTOR_PER_BLOCK) >> NUM_SECTOR_PER_BLOCK_BITS) << NUM_SECTOR_PER_BLOCK_BITS;
        pread(fd_col, col_buffer, transfer_size_bytes, lba_offset * 512);
        // set values with sampled points for i
#ifndef USE_FPGA // this is the part supposed to be done @ FPGA
        for (const int64_t &p : perm) {
            //c = ((int64_t*)col_buffer)[data_offset / sizeof(int64_t) + p];
            c = ((int64_t*)col_buffer)[p];
            if (n_id_map.count(c) == 0) {
                // no duplicate node
                n_id_map[c] = n_ids.size();
                n_ids.push_back(c);
            }
            cols.push_back(n_id_map[c]);
        }
#else // we call the FPGA kernel to calculate all values of c beforehand
        // prepare for kernel call
        // call kernel
        // TODO: kernel call might be seriously wrong
        uint64_t* perm_buffer_host, c_buffer_host;
        // align host memory pointer to 4K boundary
        posix_memalign(&perm_buffer_host, 4096, perm.size() * sizeof(int64_t));
        posix_memalign(&c_buffer_host, 4096, perm.size() * sizeof(int64_t));
        // copy contents into the inputs for kernel
        uint64_t ind = 0;
        for (const int64_t &p : perm) {
            perm_buffer_host[loop_ind] = p;
            ++loop_ind;
        }
        // create buffers for P2P -- check group_id!
        auto perm_buffer = xrt::bo(device,
                                   perm_buffer_host,
                                   perm.size() * sizeof(int64_t),
                                   xrt::bo::flags::p2p,
                                   kernel.group_id(1));
        auto c_buffer = xrt::bo(device,
                                c_buffer_host,
                                perm.size() * sizeof(int64_t),
                                xrt::bo::flags::p2p,
                                kernel.group_id(2));
        auto run = kernel(col_buffer, perm_buffer, c_buffer, (int)perm.size());
        run.wait();
        // after kernel call --> values of c are calculated already
        loop_ind = 0;
        for (const int64_t &p : perm) {
            // fetch c accordingly from the buffer calculated by FPGA
            c = c_buffer_host[loop_ind];
            ++loop_ind;
            if (n_id_map.count(c) == 0) {
                // no duplicate node
                n_id_map[c] = n_ids.size();
                n_ids.push_back(c);
            }
            cols.push_back(n_id_map[c]);
        }
        /*
        uint64_t *perm_buffer_host, cols_result_buffer_host;
        // align host memory pointer to 4K boundary
        posix_memalign(&perm_buffer_host, 4096, perm.size() * sizeof(int64_t));
        posix_memalign(&cols_result_buffer_host, 4096, perm.size() * sizeof(int64_t));
        // copy std::unordered_set perm to int64_t *perm_buffer_host
        uint64_t ind = 0;
        for (const int64_t &p : perm) {
            perm_buffer_host[ind] = p;
            //cols_result_buffer_host[ind] = 0;
            ++ind;
        }
        // create perm_buffer for P2P
        auto perm_buffer = xrt::bo(device, perm_buffer_host, perm.size() * sizeof(int64_t), xrt::bo::flags::p2p, kernel.group_id(1));
        auto cols_result_buffer = xrt::bo(device, cols_result_buffer_host, perm.size() * sizeof(int64_t), xrt::bo::flags::p2p, kernel.group_id(2));
        
        // after kernel execution, move from cols_result_buffer_host to cols
        for (uint64_t x = 0; x < perm.size(); ++x) {
            cols.push_back(cols_result_buffer_host[x]);
        }
        */
#endif
        // done setting values (for i)
        out_rowptr_data[i + 1] = cols.size();
    }

    // return tuple of tensors
    int64_t n_len = n_ids.size();
    int64_t e_len = cols.size();
    auto col = torch::from_blob(cols.data(), {e_len}, rowptr.options()).clone();
    auto n_id = torch::from_blob(n_ids.data(), {n_len}, rowptr.options()).clone();

    // XRT buffer object is freed automatically
    close(fd_col);

    return std::make_tuple(out_rowptr, col, n_id);
}