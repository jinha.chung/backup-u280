def print_average(_type, _size,_status, _key, _list):
    length = len(_list)
    avg = sum(_list) / len(_list)
    print("type: {} & output buffer size: {} [{}] [{}]: average {} -- length: {}".format(_type, _size, _status, _key, avg, length))

def output_buffer_size_sweep_main():
    types = ["bulk", "p2p", "pio"]
    #types = ["bulk", "pio"]
    #types = ["bulk"]
    #types = ["pio"]
    #types = ["p2p"]
    sizes = ["4K", "8K", "16K", "32K"]
    #sizes = ["4K"]
    for _type in types:
        for _size in sizes:
            filename = "out/{}/buffer_{}.log".format(_type, _size)
            print("reaping stats of {}".format(filename))
            f = open(filename, "r")
            lines = f.readlines()

            # let only certain hops pass
            temp_lines = []
            for l in lines:
                #if ("HOP # 1" in l) or ("BREAKDOWN" in l):
                if ("HOP # 2" in l) or ("BREAKDOWN" in l):
                    temp_lines.append(l)
            lines = temp_lines

            f.close()
            stats = {}
            stats["average"] = {}
            stats["total"] = {}
            breakdown_status = None
            for l in lines:
                l = l.strip()
                if l[0] != "[":
                    continue
                l = l.split("]")[1].strip()
                #print(l)
                if "TIME BREAKDOWN" in l:
                    if "AVERAGE" in l:
                        breakdown_status = "average"
                    elif "TOTAL" in l:
                        breakdown_status = "total"
                    else:
                        print("Something wrong... Error TIME BREAKDOWN PARSING")
                        return -1
                    continue
                if breakdown_status != None:
                    if l.endswith("us"):
                        key, value = "".join(l.split(":")[0:-1]).strip(), float(l.split(":")[-1].strip()[0:-2])
                        #print(breakdown_status, key, value)
                        if not key in stats[breakdown_status].keys():
                            stats[breakdown_status][key] = []
                        stats[breakdown_status][key].append(value)
                    else:
                        # do nothing?
                        continue
            # get sum
            for status in stats.keys():
                for key in stats[status].keys():
                    print_average(_type, _size, status, key, stats[status][key])

def bulk_number_sweep_main():
    datasets = ["reddit32x", "reddit160x", "amazon1000x", "movielens160x", "ogbn-papers100M2x", "PPI160x"]
    dataset = datasets[0]
    '''
    ### for pio
    filename = "out/211114_pio_1_1024/{}/pio/pio_1.log".format(dataset)
    print("reaping stats of {}".format(filename))
    f = open(filename, "r")
    lines = f.readlines()

    # let only certain hops pass
    temp_lines = []
    for l in lines:
        if ("HOP #" in l) or ("BREAKDOWN" in l):
        #if ("HOP # 1" in l) or ("BREAKDOWN" in l):
        #if ("HOP # 2" in l) or ("BREAKDOWN" in l):
            temp_lines.append(l)
    lines = temp_lines

    f.close()
    stats = {}
    stats["average"] = {}
    stats["total"] = {}
    breakdown_status = None
    for l in lines:
        l = l.strip()
        if l[0] != "[":
            continue
        l = l.split("]")[1].strip()
        #print(l)
        if "TIME BREAKDOWN" in l:
            if "AVERAGE" in l:
                breakdown_status = "average"
            elif "TOTAL" in l:
                breakdown_status = "total"
            else:
                print("Something wrong... Error TIME BREAKDOWN PARSING")
                return -1
            continue
        if breakdown_status != None:
            if l.endswith("us"):
                key, value = "".join(l.split(":")[0:-1]).strip(), float(l.split(":")[-1].strip()[0:-2])
                #print(breakdown_status, key, value)
                if not key in stats[breakdown_status].keys():
                    stats[breakdown_status][key] = []
                stats[breakdown_status][key].append(value)
            else:
                # do nothing?
                continue
    # get sum
    for status in stats.keys():
        for key in stats[status].keys():
            print_average("pio", 1, status, key, stats[status][key])
    '''
    ### for bulk
    sizes = ["1", "1024"]
    for _size in sizes:
        filename = "out/211114_pio_1_1024/{}/bulk/bulk_{}.log".format(dataset, _size)
        print("reaping stats of {}".format(filename))
        f = open(filename, "r")
        lines = f.readlines()

        # let only certain hops pass
        temp_lines = []
        for l in lines:
            if ("HOP #" in l) or ("BREAKDOWN" in l):
            #if ("HOP # 1" in l) or ("BREAKDOWN" in l):
            #if ("HOP # 2" in l) or ("BREAKDOWN" in l):
                temp_lines.append(l)
        lines = temp_lines

        f.close()
        stats = {}
        stats["average"] = {}
        stats["total"] = {}
        breakdown_status = None
        for l in lines:
            l = l.strip()
            if l[0] != "[":
                continue
            l = l.split("]")[1].strip()
            #print(l)
            if "TIME BREAKDOWN" in l:
                if "AVERAGE" in l:
                    breakdown_status = "average"
                elif "TOTAL" in l:
                    breakdown_status = "total"
                else:
                    print("Something wrong... Error TIME BREAKDOWN PARSING")
                    return -1
                continue
            if breakdown_status != None:
                if l.endswith("us"):
                    key, value = "".join(l.split(":")[0:-1]).strip(), float(l.split(":")[-1].strip()[0:-2])
                    #print(breakdown_status, key, value)
                    if not key in stats[breakdown_status].keys():
                        stats[breakdown_status][key] = []
                    stats[breakdown_status][key].append(value)
                else:
                    # do nothing?
                    continue
        # get sum
        for status in stats.keys():
            for key in stats[status].keys():
                print_average("bulk", _size, status, key, stats[status][key])
    

if __name__ == "__main__":
    #output_buffer_size_sweep_main()
    bulk_number_sweep_main()
