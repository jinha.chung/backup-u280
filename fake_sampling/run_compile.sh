## FOR ALVEO U280
# default:
#g++ -O3 -DVITIS_PLATFORMxilinx_u280_xdma_201920_3  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -o test test.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib
# add argparse:
#g++ -O3 -DVITIS_PLATFORMxilinx_u280_xdma_201920_3  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -I./include/ -o test test.cpp ./src/argparse.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib
# add argparse, no optimizations, add debugger flag (-g)
#g++ -g -DVITIS_PLATFORMxilinx_u280_xdma_201920_3  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -I./include/ -o test test.cpp ./src/argparse.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib
# backup test code for comparison (backup date: before adding num_bulk configurability, 2021.11.09):
#g++ -g -DVITIS_PLATFORMxilinx_u280_xdma_201920_3  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -I./include/ -o original_test original_test.cpp ./src/argparse.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib


## FOR SMARTSSD
# default:
#g++ -O3 -DVITIS_PLATFORMxilinx_u2_gen3x4_xdma_gc_2_202110_1  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -o test test.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib

# add argparse (with optimization):
#g++ -O3 -DVITIS_PLATFORMxilinx_u2_gen3x4_xdma_gc_2_202110_1  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -I./include/ -o test test.cpp ./src/argparse.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib

# add argparse, no optimizations, add debugger flag (-g)
g++ -g -O0 -Wall -DVITIS_PLATFORMxilinx_u2_gen3x4_xdma_gc_2_202110_1  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -I./include/ -o test test.cpp ./src/argparse.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib

# backup test code for comparison (backup date: before adding num_bulk configurability, 2021.11.09):
#g++ -g -DVITIS_PLATFORMxilinx_u2_gen3x4_xdma_gc_2_202110_1  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -I./include/ -o original_test original_test.cpp ./src/argparse.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib

# do TEST for pio_to_bulk.cpp
#g++ -O3 -DVITIS_PLATFORMxilinx_u2_gen3x4_xdma_gc_2_202110_1  -D__USE_XOPEN2K8 -I/opt/xilinx/xrt/include/ -I/tools/Xilinx/Vivado/2021.1/include/ -I./include/ -o supp_test pio_to_bulk.cpp ./src/argparse.cpp -lxrt_coreutil -lxilinxopencl -lpthread -lrt -lstdc++ -L/opt/xilinx/xrt/lib/ -Wl,-rpath-link,/opt/xilinx/xrt/lib

