#!/bin/bash

#dataset="reddit32x"
#dataset="reddit160x"
#dataset="amazon1000x"
#dataset="movielens160x"
#dataset="ogbn-papers100M2x"
#dataset="PPI160x"

# bulk experiments
mkdir -p out/bulk
for bulk_number in 1 1024
do
    for i in {1..100}
    do
        #./test --sampling-type bulk --hop-number 2 --bulk-number $bulk_number --dataset $dataset >> out/bulk/bulk_$bulk_number.log
        ./supp_test --sampling-type bulk --hop-number 2 --bulk-number $bulk_number --dataset $dataset >> out/bulk/bulk_$bulk_number.log
        #./supp_test --sampling-type bulk --hop-number 1 --bulk-number $bulk_number --dataset $dataset >> out/bulk/bulk_$bulk_number.log
        if (($i % 10 == 0));
        then
            echo "Finished test number "$i" for out/bulk/bulk_"$bulk_number".log"
        fi
    done
done

# # pio experiment
# mkdir -p out/pio
# for i in {1..100}
# do
#     ./test --sampling-type pio --hop-number 2 --bulk-number $bulk_number --dataset $dataset >> out/pio/pio_1.log
#     #echo ./test --sampling-type pio --hop-number 2 --bulk-number $bulk_number --dataset $dataset
#     if (($i % 10 == 0));
#     then
#         echo "Finished test number "$i" for out/pio/bulk_1.log"
#     fi
# done
