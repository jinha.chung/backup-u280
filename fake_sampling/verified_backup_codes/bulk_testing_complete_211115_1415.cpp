#include <iostream>
#include <vector>
#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <tuple>
#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <math.h>
#include <fcntl.h>
#include <stdio.h>

#include <xrt/xrt_device.h>
#include <experimental/xrt_xclbin.h>
#include <xrt/xrt_bo.h>
#include <xrt/xrt_kernel.h>
#include <experimental/xrt_ip.h>

// added to make certain parameters be configurable without needing to recompile
#include "argparse.hpp"

/* NOTE: for certain functions (posix_memalign, fread, pread, etc.) gcc wouldn't shut up about ignoring the function return value
 ******* so I decided to add !(void) in front of the function calls to silence the gcc warnings -- see the following link for details:
 ******* https://stackoverflow.com/questions/7271939/warning-ignoring-return-value-of-scanf-declared-with-attribute-warn-unused-r */

using namespace std;

// #define FPGA_WARMUP
#define CONFIGURE_BY_NUM_BULK
#define BUFFER_GRANULARITY (1 << 12)

std::tuple<std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>>
sample_adj_pio_cpu(std::vector<int64_t> rowptr, std::vector<int64_t> rowcount, std::vector<int64_t> idx,
                   int64_t num_neighbors, bool replace, std::string path, int hop_number) {
  auto rowptr_data = rowptr.data();
  auto rowcount_data = rowcount.data();
  auto idx_data = idx.data();
  std::vector<int64_t> out_rowptr(idx.size() + 1, 0);
  auto out_rowptr_data = out_rowptr.data();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> e_ids;
  std::chrono::system_clock::time_point start, end;
  std::chrono::system_clock::time_point t0, t1, t2, t3, t4, t5, t6, t7, t8;

  int64_t duplicate_count = 0;

  std::vector<std::vector<double> > g_perf(5);

  std::cout << "[" << __func__ << "] **************************************************" << std::endl;
  std::cout << "[" << __func__ << "] * num_neighbors: " << num_neighbors
                   //<< ", num_bulk: " << num_bulk
                   //<< ", output_buffer_size: " << output_buffer_size
                   << ", hop_number: " << hop_number
                   << ", and idx.size(): " << idx.size()
                   << " *" << std::endl;

  int64_t i;
  for (int64_t n = 0; n < (int64_t)idx.size(); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }

  start = std::chrono::high_resolution_clock::now();
  int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
  if((fd_col < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }

  void *temp_col_buffer;
  size_t sectorsize = 4096;
  //size_t sector_bitmask = 0xFFFFFFFFFFFFF000;
  size_t buffersize = 4096 * 4 * 256UL;
  (void)!posix_memalign(&(temp_col_buffer), 4096, buffersize);
  int64_t numentry_per_sector = sectorsize / sizeof(int64_t);
  //int64_t numentry_per_buffer = buffersize / sizeof(int64_t);
  int64_t col_data_e;
  int64_t LBA_offset, data_offset;
  end = std::chrono::high_resolution_clock::now();
  g_perf[0].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

  if (num_neighbors < 0) { // No sampling ======================================
    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < (int64_t)idx.size(); i++) {
      r = idx_data[i];

      // torch::Tensor temp_col_buffer, temp_val_buffer;
      int64_t prev = -1;
      for (int64_t j = 0; j < rowcount_data[r]; j++) {
        e = rowptr_data[r] + j;
        // fetch col data
        LBA_offset = e / numentry_per_sector;
        data_offset = e % numentry_per_sector;
        
        if (LBA_offset == prev)
        {
          // get it from buffer
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
        }
        else
        {
          // col
          (void)!pread(fd_col, temp_col_buffer, sectorsize, LBA_offset * sectorsize);
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
          prev = LBA_offset;
        }
        
        c = col_data_e;

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
        e_ids.push_back(e);
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  else if (replace) { // Sample with replacement ===============================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < (int64_t)idx.size(); i++) {
      r = idx_data[i];
      
      int64_t prev = -1;
      for (int64_t j = 0; j < num_neighbors; j++) {
        e = rowptr_data[r] + rand() % rowcount_data[r];
        // fetch col data
        LBA_offset = e / numentry_per_sector;
        data_offset = e % numentry_per_sector;
        if (LBA_offset == prev)
        {
          // get it from buffer
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
        }
        else
        {
          // col
          (void)!pread(fd_col, temp_col_buffer, sectorsize, LBA_offset * sectorsize);
          col_data_e = ((int64_t*)temp_col_buffer)[data_offset];
          prev = LBA_offset;
        }
        c = col_data_e;


        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        c = n_id_map[c];
        if (std::find(cols.begin() + offset, cols.end(), c) == cols.end()) {
          cols.push_back(c);
          e_ids.push_back(e);
        }
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }

  } else { // Sample without replacement via Robert Floyd algorithm ============
    int64_t r, c, rc, offset = 0;
    for (int64_t i = 0; i < (int64_t)idx.size(); i++) {
      r = idx_data[i];
      rc = rowcount_data[r];

      start = std::chrono::high_resolution_clock::now();
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[1].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

      if(rc != 0)
      {
        LBA_offset = rowptr_data[r] * sizeof(int64_t) / sectorsize;
        data_offset = (rowptr_data[r] * sizeof(int64_t) % sectorsize) / sizeof(int64_t);
        size_t nSectors = (data_offset + rc) * sizeof(int64_t) / sectorsize;
        if( ((data_offset + rc) * sizeof(int64_t) % sectorsize) == 0)
          nSectors -= 1;
        
        size_t chunksize = 256;
        size_t loop = nSectors / chunksize;
        size_t counter = 0, chunk_offset = 0;

        int64_t transfer_size_bytes = (nSectors + 1) * sectorsize;
        start = std::chrono::high_resolution_clock::now();
        (void)!pread(fd_col, temp_col_buffer , transfer_size_bytes, LBA_offset * sectorsize);
        end = std::chrono::high_resolution_clock::now();
        g_perf[2].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
        
        while(counter < loop)
        {
          chunk_offset = (counter + 1) * chunksize;
          if(counter == (loop-1))
          {
            transfer_size_bytes = (nSectors - chunk_offset + 1) * sectorsize;
          }
          else
          {
            transfer_size_bytes = chunksize * sectorsize;
          }
          start = std::chrono::high_resolution_clock::now();
          (void)!pread(fd_col, (((char*)temp_col_buffer) + chunk_offset * sectorsize) , transfer_size_bytes, (LBA_offset + chunk_offset) * sectorsize);
          end = std::chrono::high_resolution_clock::now();
          g_perf[2].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
          counter++;
        }
      }
      start = std::chrono::high_resolution_clock::now();
      for (const int64_t &p : perm) {
        c = ((int64_t*)temp_col_buffer)[data_offset + p];

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }
        else {
          ++duplicate_count;
        }
        cols.push_back(n_id_map[c]);
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[3].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  start = std::chrono::high_resolution_clock::now();
  int64_t n_len = n_ids.size(), e_len = cols.size();
  std::vector<int64_t> col(e_len), n_id(n_len);
  std::copy(cols.begin(), cols.end(), col.begin());
  std::copy(n_ids.begin(), n_ids.end(), n_id.begin());

  free(temp_col_buffer);
  close(fd_col);
  end = std::chrono::high_resolution_clock::now();
  g_perf[4].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

  // 0. init
  // 1. perm calculation (Robert Floyd algorithm)
  // 2. pread into col_buffer
  // 3. remaining algorithm
  // 4. close
  // average breakdown
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ************ TIME BREAKDOWN (AVERAGE) ************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                       init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  perm calculation (Robert Floyd algorithm): "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                      pread into col_buffer: "
            << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              remaining algorithm execution: "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                      close: "
            << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size() << "us" << std::endl;
  // total breakdown
  std::cout << "[" << __func__ << "] ************* TIME BREAKDOWN (TOTAL) *************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ******** Duplicate count: " << duplicate_count << " ********" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                       init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  perm calculation (Robert Floyd algorithm): "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                      pread into col_buffer: "
            << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              remaining algorithm execution: "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                      close: "
            << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " **************************************************" << std::endl;
  
  return std::make_tuple(out_rowptr, col, n_id, n_id);
}

std::tuple<std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>>
sample_adj_mmap_cpu(std::vector<int64_t> rowptr, std::vector<int64_t> rowcount, std::vector<int64_t> idx,
                   int64_t num_neighbors, bool replace, std::string path, int hop_number) {
  return sample_adj_pio_cpu(rowptr, rowcount, idx, num_neighbors, replace, path, hop_number);
  /*
  auto rowptr_data = rowptr.data();
  auto rowcount_data = rowcount.data();
  auto idx_data = idx.data();
  std::vector<int64_t> out_rowptr(idx.size() + 1, 0);
  auto out_rowptr_data = out_rowptr.data();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> e_ids;
  std::chrono::system_clock::time_point start, end;

  int64_t duplicate_count = 0;

  std::vector<std::vector<double> > g_perf(5);

  std::cout << "[" << __func__ << "] **************************************************" << std::endl;
  std::cout << "[" << __func__ << "] * num_neighbors: " << num_neighbors
                   //<< ", num_bulk: " << num_bulk
                   //<< ", output_buffer_size: " << output_buffer_size
                   << ", hop_number: " << hop_number
                   << ", and idx.size(): " << idx.size()
                   << " *" << std::endl;
  // ?????????????????????????????????????
  col = ???????????????;
  auto col_data = col.data();

  int64_t i;
  for (int64_t n = 0; n < idx.size(0); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }

  start = std::chrono::high_resolution_clock::now();
  // do mmap here?
  struct stat sb;
  int fd_col;
  fd_col = open((path + "col").c_str(), O_RDONLY);
  fstat(fd_col, &sb);
  size_col = sb.st_size;
  if ((col = (int64_t*)mmap(NULL, size_col, PROT_READ, MAP_SHARED, fd_col, 0)) == MAP_FAILED) {
    std::cout << "mmap error" << std::endl;
    assert(0);
  }
  end = std::chrono::high_resolution_clock::now();
  g_perf[0].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

  if (num_neighbors < 0) { // No sampling ======================================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      for (int64_t j = 0; j < rowcount_data[r]; j++) {
        e = rowptr_data[r] + j;
        c = col_data[e];

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        cols.push_back(n_id_map[c]);
        e_ids.push_back(e);
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }

  else if (replace) { // Sample with replacement ===============================

    int64_t r, c, e, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];

      for (int64_t j = 0; j < num_neighbors; j++) {
        e = rowptr_data[r] + rand() % rowcount_data[r];
        c = col_data[e];

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }

        c = n_id_map[c];
        if (std::find(cols.begin() + offset, cols.end(), c) == cols.end()) {
          cols.push_back(c);
          e_ids.push_back(e);
        }
      }
      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }

  } else { // Sample without replacement via Robert Floyd algorithm ============
    int64_t r, c, e, rc, offset = 0;
    for (int64_t i = 0; i < idx.size(0); i++) {
      r = idx_data[i];
      rc = rowcount_data[r];

      start = std::chrono::high_resolution_clock::now();
      std::unordered_set<int64_t> perm;
      if (rc <= num_neighbors) {
        for (int64_t x = 0; x < rc; x++) {
          perm.insert(x);
        }
      } else {
        for (int64_t x = rc - std::min(rc, num_neighbors); x < rc; x++) {
          if (!perm.insert(rand() % x).second) {
            perm.insert(x);
          }
        }
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[1].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

      start = std::chrono::high_resolution_clock::now();
      for (const int64_t &p : perm) {
        e = rowptr_data[r] + p;
        c = col_data[e];

        if (n_id_map.count(c) == 0) {
          n_id_map[c] = n_ids.size();
          n_ids.push_back(c);
        }
        else {
          ++duplicate_count;
        }
        cols.push_back(n_id_map[c]);
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[3].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
    }
  }
  
  start = std::chrono::high_resolution_clock::now();
  int64_t n_len = n_ids.size(), e_len = cols.size();
  std::vector<int64_t> col(e_len), n_id(n_len);
  std::copy(cols.begin(), cols.end(), col.begin());
  std::copy(n_ids.begin(), n_ids.end(), n_id.begin());

  close(fd_col);
  end = std::chrono::high_resolution_clock::now();
  g_perf[4].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

  // 0. init
  // 1. perm calculation (Robert Floyd algorithm)
  // 2. mmap for col_buffer
  // 3. remaining algorithm
  // 4. close
  // average breakdown
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ************ TIME BREAKDOWN (AVERAGE) ************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                       init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  perm calculation (Robert Floyd algorithm): "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                        mmap for col_buffer: "
            << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              remaining algorithm execution: "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                      close: "
            << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size() << "us" << std::endl;
  // total breakdown
  std::cout << "[" << __func__ << "] ************* TIME BREAKDOWN (TOTAL) *************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ******** Duplicate count: " << duplicate_count << " ********" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                       init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  perm calculation (Robert Floyd algorithm): "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                        mmap for col_buffer: "
            << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              remaining algorithm execution: "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                      close: "
            << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " **************************************************" << std::endl;

  return std::make_tuple(out_rowptr, col, n_id, n_id);
  */
}

std::tuple<std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>>
sample_adj_p2p_cpu(std::vector<int64_t> rowptr, std::vector<int64_t> rowcount, std::vector<int64_t> idx,
                   int64_t num_neighbors, bool replace, std::string path, int64_t maxrc,
                   int64_t output_buffer_size, int hop_number) {
  
  auto rowptr_data = rowptr.data();
  auto rowcount_data = rowcount.data();
  auto idx_data = idx.data();
  std::vector<int64_t> out_rowptr(idx.size() + 1, 0);
  auto out_rowptr_data = out_rowptr.data();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> e_ids;
  std::chrono::system_clock::time_point start, end;

  int64_t duplicate_count = 0;

  // g_perf measurements:
  // 0. init
  // 1. xrt::bo init & allocation
  // 3. P2P read into col_buffer with pread
  // 6. kernel execution
  // 7. sync: from device
  // 8. remaining algorithm
  // 9. close
  std::vector<std::vector<double> > g_perf(10);

  std::cout << "[" << __func__ << "] **************************************************" << std::endl;
  std::cout << "[" << __func__ << "] * num_neighbors: " << num_neighbors
                   //<< ", num_bulk: " << num_bulk
                   << ", output_buffer_size: " << output_buffer_size
                   << ", hop_number: " << hop_number
                   << ", and idx.size(): " << idx.size()
                   << " *" << std::endl;

  start = std::chrono::high_resolution_clock::now();
  int64_t i;
  for (int64_t n = 0; n < (int64_t)idx.size(); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
    
  // get XRT device ready
  auto device = xrt::device(0);
  //auto uuid = device.load_xclbin("/home/via/jinha/backup-u280/xclbins/sample_and_gather_no_perm_dynamic_host_seed_3055317720/bandwidth.xclbin");
  auto uuid = device.load_xclbin("/home/via/jinha/xclbins/smartssd/sample_and_gather_no_perm_dynamic_host_seed_3055317720/bandwidth.xclbin");
  // void bandwidth(intV_t *in_col, intV_t *out_c, int num_neighbor, int rc, int R, int seed)
  auto kernel = xrt::kernel(device, uuid, "bandwidth");

  int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
  if((fd_col < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }

  // allocate buffers using XRT API
  uint64_t BLOCK_SIZE_BITS = 12;
  uint64_t BLOCK_SIZE = 1 << BLOCK_SIZE_BITS;
  uint64_t NUM_NODES_PER_BLOCK = BLOCK_SIZE / sizeof(int64_t);

  end = std::chrono::high_resolution_clock::now();
  g_perf[0].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

  // xrt::bo init & allocation: gperf[1]
  start = std::chrono::high_resolution_clock::now();
  // int64_t max_col_buffer_size = maxrc * sizeof(int64_t);
  int64_t max_col_buffer_size = 1 << 21;
  //int64_t max_c_buffer_size = num_neighbors * sizeof(int64_t);

  auto col_buffer = xrt::bo(device, max_col_buffer_size, xrt::bo::flags::p2p, kernel.group_id(0));
  auto c_buffer = xrt::bo(device, output_buffer_size, kernel.group_id(1));

  auto col_buffer_map = col_buffer.map<int64_t*>();
  auto c_buffer_map = c_buffer.map<int64_t*>();
  end = std::chrono::high_resolution_clock::now();
  g_perf[1].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
#ifdef FPGA_WARMUP
  // FPGA warmup
  std::cout << "FPGA warmup start" << std::endl;
  start = std::chrono::high_resolution_clock::now();
  for (int jerry = 0; jerry < 1024; ++jerry) {
    auto run = kernel(col_buffer, c_buffer, jerry + 1, jerry, 0, 0);
    run.wait();
  }
  std::cout << "FPGA warmup done" << std::endl;
  end = std::chrono::high_resolution_clock::now();
  //std::cout << "[" << __func__ << "]                            FPGA warmup: " << (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << "us" << std::endl;
#endif
  //int64_t col_data_e;
  int64_t LBA_offset, data_offset;

  std::cout << "Sampling start" << std::endl;
  
  if (num_neighbors < 0) { // No sampling ======================================
    std::cout << "num_neighbors < 0" << std::endl;
    assert(0);
  }

  else if (replace) { // Sample with replacement ===============================
    std::cout << "replace is true" << std::endl;
    assert(0);

  } else { // Sample without replacement via Robert Floyd algorithm ============
    int64_t r, c, rc, offset = 0;
    for (int64_t i = 0; i < (int64_t)idx.size(); i++) {
      r = idx_data[i];
      rc = rowcount_data[r];

      if(rc != 0)
      {
        LBA_offset = rowptr_data[r] / NUM_NODES_PER_BLOCK;
        data_offset = (rowptr_data[r] % NUM_NODES_PER_BLOCK);
        size_t nSectors = (data_offset + rc) / NUM_NODES_PER_BLOCK;
        if( ((data_offset + rc) % NUM_NODES_PER_BLOCK) == 0)
          nSectors -= 1;
        
        size_t chunksize = 256;
        size_t loop = nSectors / chunksize;
        size_t counter = 0, chunk_offset = 0;

        int64_t transfer_size_bytes = (nSectors + 1) * BLOCK_SIZE;
        start = std::chrono::high_resolution_clock::now();
        (void)!pread(fd_col, col_buffer_map, transfer_size_bytes, LBA_offset * BLOCK_SIZE);
        end = std::chrono::high_resolution_clock::now();
        g_perf[3].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
        
        while(counter < loop)
        {
          chunk_offset = (counter + 1) * chunksize;
          if(counter == (loop-1))
          {
            transfer_size_bytes = (nSectors - chunk_offset + 1) * BLOCK_SIZE;
          }
          else
          {
            transfer_size_bytes = chunksize * BLOCK_SIZE;
          }
          start = std::chrono::high_resolution_clock::now();
          (void)!pread(fd_col, (col_buffer_map + chunk_offset * BLOCK_SIZE) , transfer_size_bytes, (LBA_offset + chunk_offset) * BLOCK_SIZE);
          end = std::chrono::high_resolution_clock::now();
          g_perf[3].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
          counter++;
        }
      }
      
      srand(time(NULL));
      int seed = rand() % (1 << 16);
      // kernel execution: gperf[6]
      start = std::chrono::high_resolution_clock::now();
      auto run = kernel(col_buffer, c_buffer, num_neighbors, rc, data_offset, seed);
      run.wait();
      end = std::chrono::high_resolution_clock::now();
      g_perf[6].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
      
      // sync, from device: gperf[7]
      start = std::chrono::high_resolution_clock::now();
      c_buffer.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
      end = std::chrono::high_resolution_clock::now();
      g_perf[7].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

      // after kernel call --> values of c are calculated already: gperf[8]
      start = std::chrono::high_resolution_clock::now();
      int64_t loop_num = rc < num_neighbors ? rc : num_neighbors;
      for (int loop_ind = 0; loop_ind < loop_num; loop_ind++) {
          // fetch c accordingly from the buffer calculated by FPGA
          c = c_buffer_map[loop_ind];
          if (n_id_map.count(c) == 0) {
              // no duplicate node
              n_id_map[c] = n_ids.size();
              n_ids.push_back(c);
          }
          else {
            ++duplicate_count;
          }
          cols.push_back(n_id_map[c]);
      }

      offset = cols.size();
      out_rowptr_data[i + 1] = offset;
      end = std::chrono::high_resolution_clock::now();
      g_perf[8].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
    }
  }
  
  // return tuple of tensors: gperf[9]
  start = std::chrono::high_resolution_clock::now();
  int64_t n_len = n_ids.size(), e_len = cols.size();
  std::vector<int64_t> col(e_len), n_id(n_len);
  std::copy(cols.begin(), cols.end(), col.begin());
  std::copy(n_ids.begin(), n_ids.end(), n_id.begin());

  close(fd_col);
  end = std::chrono::high_resolution_clock::now();
  g_perf[9].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
  
  // 0. init
  // 1. xrt::bo init & allocation
  // 2. perm calculation (Robert Floyd algorithm)
  // 3. P2P read into col_buffer with pread
  // 4. perm buffer copy
  // 5. sync: to device
  // 6. kernel execution
  // 7. sync: from device
  // 8. remaining algorithm
  // 9. close
  // average breakdown
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ************ TIME BREAKDOWN (AVERAGE) ************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                   init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              xrt::bo init & allocation: "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size() << "us" << std::endl;
  //std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       perm calculation: "
  //          << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) / g_perf[2].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  P2P read into col_buffer with pread(): "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size() << "us" << std::endl;
  //std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       perm_buffer copy: "
  //          << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) / g_perf[4].size() << "us" << std::endl;
  //std::cout << "[" << __func__ << "] HOP # " << hop_number << "    sync before kernel call (TO DEVICE): "
  //          << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) / g_perf[5].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       kernel execution: "
            << (double)std::accumulate(g_perf[6].begin(), g_perf[6].end(), 0.0) / g_perf[6].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "   sync after kernel call (FROM DEVICE): "
            << (double)std::accumulate(g_perf[7].begin(), g_perf[7].end(), 0.0) / g_perf[7].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "          remaining algorithm execution: "
            << (double)std::accumulate(g_perf[8].begin(), g_perf[8].end(), 0.0) / g_perf[8].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                  close: "
            << (double)std::accumulate(g_perf[9].begin(), g_perf[9].end(), 0.0) / g_perf[9].size() << "us" << std::endl;
  
  // total breakdown
  std::cout << "[" << __func__ << "] ************* TIME BREAKDOWN (TOTAL) *************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ******** Duplicate count: " << duplicate_count << " ********" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                   init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              xrt::bo init & allocation: "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) << "us" << std::endl;
  //std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       perm calculation: "
  //          << (double)std::accumulate(g_perf[2].begin(), g_perf[2].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  P2P read into col_buffer with pread(): "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) << "us" << std::endl;
  //std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       perm_buffer copy: "
  //          << (double)std::accumulate(g_perf[4].begin(), g_perf[4].end(), 0.0) << "us" << std::endl;
  //std::cout << "[" << __func__ << "] HOP # " << hop_number << "    sync before kernel call (TO DEVICE): "
  //          << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       kernel execution: "
            << (double)std::accumulate(g_perf[6].begin(), g_perf[6].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "   sync after kernel call (FROM DEVICE): "
            << (double)std::accumulate(g_perf[7].begin(), g_perf[7].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "          remaining algorithm execution: "
            << (double)std::accumulate(g_perf[8].begin(), g_perf[8].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                  close: "
            << (double)std::accumulate(g_perf[9].begin(), g_perf[9].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " **************************************************" << std::endl;
  
  // return
  return std::make_tuple(out_rowptr, col, n_id, n_id);
}

std::tuple<std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>, std::vector<int64_t>>
sample_adj_p2p_bulk_cpu(std::vector<int64_t> rowptr, std::vector<int64_t> rowcount, std::vector<int64_t> idx,
                        int64_t num_neighbors, int64_t num_bulk, bool replace, std::string path,
                        int64_t maxrc, int64_t output_buffer_size, int hop_number) {
  
  auto rowptr_data = rowptr.data();
  auto rowcount_data = rowcount.data();
  auto idx_data = idx.data();
  std::vector<int64_t> out_rowptr(idx.size() + 1, 0);
  auto out_rowptr_data = out_rowptr.data();
  out_rowptr_data[0] = 0;

  std::vector<int64_t> cols;
  std::vector<int64_t> n_ids;
  std::unordered_map<int64_t, int64_t> n_id_map;
  std::vector<int64_t> e_ids;
  std::chrono::system_clock::time_point start, end;

  int64_t duplicate_count = 0;

  // g_perf measurements:
  // 0. init
  // 1. xrt::bo init & allocation
  // 3. P2P read into col_buffer with pread
  // 6. kernel execution
  // 7. sync: from device
  // 8. remaining algorithm
  // 9. close
  std::vector<std::vector<double> > g_perf(10);

  std::cout << "[" << __func__ << "] **************************************************" << std::endl;
  std::cout << "[" << __func__ << "] * num_neighbors: " << num_neighbors
                   << ", num_bulk: " << num_bulk
                   << ", output_buffer_size: " << output_buffer_size
                   << ", hop_number: " << hop_number
                   << ", and idx.size(): " << idx.size()
                   << " *" << std::endl;

  start = std::chrono::high_resolution_clock::now();
  int64_t i;
  for (int64_t n = 0; n < (int64_t)idx.size(); n++) {
    i = idx_data[n];
    n_id_map[i] = n;
    n_ids.push_back(i);
  }
  // get XRT device ready
  auto device = xrt::device(0);
  //auto uuid = device.load_xclbin("/home/via/jinha/xclbins/smartssd/sample_and_gather_bulk_dynamic_seed_3055317720/bandwidth.xclbin");
  //auto uuid = device.load_xclbin("/home/via/jinha/xclbins/smartssd/sample_and_gather_bulk_dynamic_seed_3055317720_variable_names_change/bandwidth.xclbin");
  //auto uuid = device.load_xclbin("/home/via/jinha/xclbins/smartssd/sample_and_gather_bulk_better_pipelines_dynamic_seed_3055317720/bandwidth.xclbin");
  //auto uuid = device.load_xclbin("/home/via/jinha/xclbins/smartssd/sample_and_gather_bulk_not_random/bandwidth.xclbin");
  auto uuid = device.load_xclbin("/home/via/jinha/xclbins/smartssd/sample_and_gather_bulk_no_pipeline_dynamic_seed_3055317720/bandwidth.xclbin");
  auto kernel = xrt::kernel(device, uuid, "bandwidth");

  int fd_col = open((path+"col").c_str(), O_RDONLY | O_DIRECT);
  if((fd_col < 0))
  {
    std::cout << "can't open " << path+"col" << std::endl;
    exit(1);
  }

  // allocate buffers using XRT API
  uint64_t BLOCK_SIZE_BITS = 12;
  uint64_t BLOCK_SIZE = 1 << BLOCK_SIZE_BITS;
  uint64_t NUM_NODES_PER_BLOCK = BLOCK_SIZE / sizeof(int64_t);

  end = std::chrono::high_resolution_clock::now();
  g_perf[0].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

  // xrt::bo init & allocation: gperf[1]
  start = std::chrono::high_resolution_clock::now();
  int64_t max_col_buffer_size = (1 + (((maxrc * sizeof(int64_t)) + BUFFER_GRANULARITY - 2) / BUFFER_GRANULARITY)) * BUFFER_GRANULARITY * num_bulk;
  int64_t rc_buffer_size = num_bulk * sizeof(int64_t);
  rc_buffer_size = (int64_t)((rc_buffer_size + BUFFER_GRANULARITY - 1) / BUFFER_GRANULARITY) * BUFFER_GRANULARITY;

  auto col_buffer = xrt::bo(device, max_col_buffer_size, xrt::bo::flags::p2p, kernel.group_id(0));
  auto rc_buffer = xrt::bo(device, rc_buffer_size, kernel.group_id(1));
  auto data_offset_buffer = xrt::bo(device, rc_buffer_size, kernel.group_id(2));
  auto c_buffer = xrt::bo(device, output_buffer_size, kernel.group_id(3));

  auto col_buffer_map = col_buffer.map<int64_t*>();
  auto rc_buffer_map = rc_buffer.map<int64_t*>();
  auto data_offset_buffer_map = data_offset_buffer.map<int64_t*>();
  auto c_buffer_map = c_buffer.map<int64_t*>();
  end = std::chrono::high_resolution_clock::now();
  g_perf[1].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

#ifdef FPGA_WARMUP
  // FPGA warmup
  std::cout << "FPGA warmup start" << std::endl;
  start = std::chrono::high_resolution_clock::now();
  for (int jerry = 0; jerry < 1024; ++jerry) {
    auto run = kernel(col_buffer, c_buffer, jerry + 1, jerry, 0, 0);
    run.wait();
  }
  std::cout << "FPGA warmup done" << std::endl;
  end = std::chrono::high_resolution_clock::now();
  //std::cout << "[" << __func__ << "]                            FPGA warmup: " << (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << "us" << std::endl;
#endif
  int64_t LBA_offset, data_offset;

  std::cout << "Sampling start" << std::endl;
  
  if (num_neighbors < 0) { // No sampling ======================================
    std::cout << "num_neighbors < 0" << std::endl;
    assert(0);
  }
  else if (replace) { // Sample with replacement ===============================
    std::cout << "replace is true" << std::endl;
    assert(0);
  }
  else { // Sample without replacement ============
    int64_t r, c, rc, offset, r_idx, col_buffer_offset = 0;
    int64_t r_array[1024] = {0}; // this means num_bulk maximum possible value is 1024
    int64_t num_iteration = (int)((idx.size() + (num_bulk - 1)) / num_bulk);
    for (int64_t i = 0; i < num_iteration; ++i) {
      r_idx = 0;
      for (int64_t j = i * num_bulk; (j < (i + 1) * num_bulk) && (j < (int64_t)idx.size()); ++j) {
        r = idx_data[j];
        rc = rowcount_data[r];
        r_array[r_idx] = r;
        rc_buffer_map[r_idx] = rc;
        ++r_idx;
      }
      r_idx = 0;
      col_buffer_offset = 0;
      data_offset = 0;
      for (int64_t j = i * num_bulk; (j < (i + 1) * num_bulk) && (j < (int64_t)idx.size()); ++j) {
        r = r_array[r_idx];
        rc = rc_buffer_map[r_idx];
        if(rc != 0) {
          LBA_offset = rowptr_data[r] / NUM_NODES_PER_BLOCK;
          data_offset = (rowptr_data[r] % NUM_NODES_PER_BLOCK);
          size_t nSectors = (data_offset + rc) / NUM_NODES_PER_BLOCK;
          if (((data_offset + rc) % NUM_NODES_PER_BLOCK) == 0) {
            nSectors -= 1;
          }
        
          size_t chunksize = 256;
          size_t loop = nSectors / chunksize;
          size_t counter = 0, chunk_offset = 0;
          data_offset_buffer_map[r_idx] = col_buffer_offset / sizeof(int64_t) + data_offset;

          int64_t transfer_size_bytes = (nSectors + 1) * BLOCK_SIZE;
          start = std::chrono::high_resolution_clock::now();
          (void)!pread(fd_col, (char*)col_buffer_map + col_buffer_offset , transfer_size_bytes, LBA_offset * BLOCK_SIZE);
          end = std::chrono::high_resolution_clock::now();
          g_perf[3].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
          col_buffer_offset += transfer_size_bytes;

          while (counter < loop) {
            chunk_offset = (counter + 1) * chunksize;
            if (counter == (loop - 1)) {
              transfer_size_bytes = (nSectors - chunk_offset + 1) * BLOCK_SIZE;
            }
            else {
              transfer_size_bytes = chunksize * BLOCK_SIZE;
            }
            start = std::chrono::high_resolution_clock::now();
            (void)!pread(fd_col, ((char*)col_buffer_map + col_buffer_offset + chunk_offset * BLOCK_SIZE) , transfer_size_bytes, (LBA_offset + chunk_offset) * BLOCK_SIZE);
            end = std::chrono::high_resolution_clock::now();
            g_perf[3].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
            counter++;
            col_buffer_offset += transfer_size_bytes;
          }
        }
        else {
          data_offset_buffer_map[r_idx] = col_buffer_offset / sizeof(int64_t) + data_offset;
        }
        ++r_idx;
      }

      start = std::chrono::high_resolution_clock::now();
      rc_buffer.sync(XCL_BO_SYNC_BO_TO_DEVICE);
      data_offset_buffer.sync(XCL_BO_SYNC_BO_TO_DEVICE);
      end = std::chrono::high_resolution_clock::now();
      g_perf[5].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

      srand(time(NULL));
      int seed = rand() % (1 << 16);
      // kernel execution: gperf[6]
      start = std::chrono::high_resolution_clock::now();
      if ((i == num_iteration - 1) && ((idx.size() % num_bulk) != 0)) {
        int64_t last_num_bulk = idx.size() % num_bulk;
        auto run = kernel(col_buffer, rc_buffer, data_offset_buffer, c_buffer, last_num_bulk, num_neighbors, seed);
        run.wait();
      }
      else {
        auto run = kernel(col_buffer, rc_buffer, data_offset_buffer, c_buffer, num_bulk, num_neighbors, seed);
        run.wait();
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[6].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
      
      // sync, from device: gperf[7]
      start = std::chrono::high_resolution_clock::now();
      c_buffer.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
      rc_buffer.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
      end = std::chrono::high_resolution_clock::now();
      g_perf[7].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());

      // after kernel call --> values of c are calculated already: gperf[8]
      start = std::chrono::high_resolution_clock::now();
      // corner-case handling
      int dup_check_loop_num = num_bulk;
      if ((i == num_iteration - 1) && ((idx.size() % num_bulk) != 0)) {
        dup_check_loop_num = idx.size() % num_bulk;
      }
      for (int bulk_idx = 0; bulk_idx < dup_check_loop_num; ++bulk_idx) {
        int64_t loop_num = rc_buffer_map[bulk_idx] < num_neighbors ? rc_buffer_map[bulk_idx] : num_neighbors;
        for (int loop_ind = 0; loop_ind < loop_num; loop_ind++) {
          // fetch c accordingly from the buffer calculated by FPGA
          c = c_buffer_map[bulk_idx * num_neighbors + loop_ind];
          if (n_id_map.count(c) == 0) {
              // no duplicate node
              n_id_map[c] = n_ids.size();
              n_ids.push_back(c);
          }
          else {
            // duplicate
            ++duplicate_count;
          }
          cols.push_back(n_id_map[c]);
        }

        offset = cols.size();
        out_rowptr_data[i * num_bulk + bulk_idx + 1] = offset;
      }
      end = std::chrono::high_resolution_clock::now();
      g_perf[8].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
    }
  }
  
  // return tuple of tensors: gperf[9]
  start = std::chrono::high_resolution_clock::now();
  int64_t n_len = n_ids.size(), e_len = cols.size();
  std::vector<int64_t> col(e_len), n_id(n_len);
  std::copy(cols.begin(), cols.end(), col.begin());
  std::copy(n_ids.begin(), n_ids.end(), n_id.begin());

  close(fd_col);
  end = std::chrono::high_resolution_clock::now();
  g_perf[9].push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
  
  // 0. init
  // 1. xrt::bo init & allocation
  // 2. perm calculation (Robert Floyd algorithm)
  // 3. P2P read into col_buffer with pread
  // 4. perm buffer copy
  // 5. sync: to device
  // 6. kernel execution
  // 7. sync: from device
  // 8. remaining algorithm
  // 9. close
  // average breakdown
  
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ************ TIME BREAKDOWN (AVERAGE) ************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                   init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) / g_perf[0].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              xrt::bo init & allocation: "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) / g_perf[1].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  P2P read into col_buffer with pread(): "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) / g_perf[3].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "    sync before kernel call (TO DEVICE): "
            << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) / g_perf[5].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       kernel execution: "
            << (double)std::accumulate(g_perf[6].begin(), g_perf[6].end(), 0.0) / g_perf[6].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "   sync after kernel call (FROM DEVICE): "
            << (double)std::accumulate(g_perf[7].begin(), g_perf[7].end(), 0.0) / g_perf[7].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "          remaining algorithm execution: "
            << (double)std::accumulate(g_perf[8].begin(), g_perf[8].end(), 0.0) / g_perf[8].size() << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                  close: "
            << (double)std::accumulate(g_perf[9].begin(), g_perf[9].end(), 0.0) / g_perf[9].size() << "us" << std::endl;
  
  // total breakdown
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ************* TIME BREAKDOWN (TOTAL) *************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " ************* Duplicate count: " << duplicate_count << " *************" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                   init: "
            << (double)std::accumulate(g_perf[0].begin(), g_perf[0].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "              xrt::bo init & allocation: "
            << (double)std::accumulate(g_perf[1].begin(), g_perf[1].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "  P2P read into col_buffer with pread(): "
            << (double)std::accumulate(g_perf[3].begin(), g_perf[3].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "    sync before kernel call (TO DEVICE): "
            << (double)std::accumulate(g_perf[5].begin(), g_perf[5].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                       kernel execution: "
            << (double)std::accumulate(g_perf[6].begin(), g_perf[6].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "   sync after kernel call (FROM DEVICE): "
            << (double)std::accumulate(g_perf[7].begin(), g_perf[7].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "          remaining algorithm execution: "
            << (double)std::accumulate(g_perf[8].begin(), g_perf[8].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << "                                  close: "
            << (double)std::accumulate(g_perf[9].begin(), g_perf[9].end(), 0.0) << "us" << std::endl;
  std::cout << "[" << __func__ << "] HOP # " << hop_number << " **************************************************" << std::endl;
  
  // return
  return std::make_tuple(out_rowptr, col, n_id, n_id);
}


int main(int argc, char *argv[]) {
  // parse command line arguments
  ArgParse parser;
  parser.Parse(argc, argv);
  int64_t hop_number = parser.GetIntFromKey("hop-number", 1);
  std::string dataset = parser.GetStringFromKey("dataset", "reddit32x");
  std::string sampling_type = parser.GetStringFromKey("sampling-type", "pio");
  std::string output_buffer_size_str = parser.GetStringFromKey("output-buffer-size", "4K");
  int64_t neighbor_size_bits = parser.GetIntFromKey("neighbor-size-bits", 5);
  int64_t num_bulk = parser.GetIntFromKey("bulk-number", 1);
  int64_t output_buffer_size, num_neighbors, total_required_buffer_size;

  //dataset-dependent variables
  int64_t maxIdx;
  int64_t maxrc;
  std::string path;
  FILE* fp_rowptr;
  FILE* fp_rowcount;
  if (dataset == "reddit32x") {
    maxIdx = 7454880;
    maxrc = 22656;
    path = "/mnt/smartssd/reddit/32x/";
    fp_rowptr = fopen("/mnt/smartssd/reddit/32x/rowptr", "r");
    fp_rowcount = fopen("/mnt/smartssd/reddit/32x/rowcount", "r");
  }
  else if (dataset == "reddit160x") {
    maxIdx = 37274400;
    maxrc = 25392;
    path = "/mnt/smartssd/reddit/160x/";
    fp_rowptr = fopen("/mnt/smartssd/reddit/160x/rowptr", "r");
    fp_rowcount = fopen("/mnt/smartssd/reddit/160x/rowcount", "r");
  }
  else if (dataset == "amazon1000x") {
    maxIdx = 265933000;
    maxrc = 262;
    path = "/mnt/smartssd/amazon/1000x/";
    fp_rowptr = fopen("/mnt/smartssd/amazon/1000x/rowptr", "r");
    fp_rowcount = fopen("/mnt/smartssd/amazon/1000x/rowcount", "r");
  }
  else if (dataset == "movielens160x") {
    maxIdx = 22158880;
    maxrc = 13787;
    path = "/mnt/smartssd/movielens/160x/";
    fp_rowptr = fopen("/mnt/smartssd/movielens/160x/rowptr", "r");
    fp_rowcount = fopen("/mnt/smartssd/movielens/160x/rowcount", "r");
  }
  else if (dataset == "ogbn-papers100M2x") {
    maxIdx = 179133588;
    maxrc = 251481;
    path = "/mnt/smartssd/ogbn-papers100M/2x/";
    fp_rowptr = fopen("/mnt/smartssd/ogbn-papers100M/2x/rowptr", "r");
    fp_rowcount = fopen("/mnt/smartssd/ogbn-papers100M/2x/rowcount", "r");
  }
  else if (dataset == "PPI160x") {
    maxIdx = 9070080;
    maxrc = 2037;
    path = "/mnt/smartssd/PPI/160x/";
    fp_rowptr = fopen("/mnt/smartssd/PPI/160x/rowptr", "r");
    fp_rowcount = fopen("/mnt/smartssd/PPI/160x/rowcount", "r");
  }
  else {
    std::cout << "Unknown dataset name: " << dataset << std::endl;
    return -1;
  }
  std::cout << "[" << __func__ << "] ********************** DATASET: " << dataset << " **********************" << std::endl;

  std::vector<int64_t> rowptr, rowcount, idx(1024);
  std::unordered_map<int64_t, bool> idx_map;
  std::chrono::system_clock::time_point start, end;
  
  fseek(fp_rowptr, 0, SEEK_END);
  size_t rowptrSize = ftell(fp_rowptr);
  fseek(fp_rowcount, 0, SEEK_END);
  size_t rowcountSize = ftell(fp_rowcount);
  
  std::cout << "rowptrSize: " << rowptrSize << " rowptrCount: " << rowcountSize << std::endl;
  rowptr.resize(rowptrSize / sizeof(int64_t));
  rowcount.resize(rowcountSize / sizeof(int64_t));

  fseek(fp_rowptr, 0, SEEK_SET);
  (void)!fread(rowptr.data(), rowptrSize, 1, fp_rowptr);
  fseek(fp_rowcount, 0, SEEK_SET);
  (void)!fread(rowcount.data(), rowcountSize, 1, fp_rowcount);

  srand(time(NULL));
  auto pidx = idx.data();

  for(int i = 0; i < 1024;) {
    int n = (int64_t)rand() % maxIdx;
    if(idx_map[n])
      continue;
    pidx[i] = n;
    idx_map[n] = true;
    i++;
  }
  // start sampling
  if (hop_number == 1) {
    // determine parameters depending on configuration
    num_neighbors = 1 << neighbor_size_bits;
#ifdef CONFIGURE_BY_NUM_BULK // configure by num_bulk
    // num_bulk was already set correctly at beginning of main function
    total_required_buffer_size = sizeof(int64_t) * num_neighbors * num_bulk;
    output_buffer_size = (int64_t)((total_required_buffer_size + (BUFFER_GRANULARITY - 1)) / BUFFER_GRANULARITY) * BUFFER_GRANULARITY;
#else // configure by output_buffer_size
    output_buffer_size = (output_buffer_size_str == "4K") ? (1 << 12) :
                         (output_buffer_size_str == "8K") ? (1 << 13) :
                         (output_buffer_size_str == "16K") ? (1 << 14) :
                         (output_buffer_size_str == "32K") ? (1 << 15) : -1;
    if (output_buffer_size < 0) {
      std::cout << "Wrong output buffer size" << std::endl;
      return -1;
    }
    num_bulk = (int64_t) (output_buffer_size / (num_neighbors * sizeof(int64_t)));
#endif // CONFIGURE_BY_NUM_BULK
    if (sampling_type == "pio") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_pio_cpu(rowptr, rowcount, idx, num_neighbors, false, path, 1);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "mmap") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_mmap_cpu(rowptr, rowcount, idx, num_neighbors, false, path, 1);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "p2p") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_p2p_cpu(rowptr, rowcount, idx, num_neighbors, false, path, maxrc, output_buffer_size, 1);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "bulk") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_p2p_bulk_cpu(rowptr, rowcount, idx, num_neighbors, num_bulk, false, path, maxrc, output_buffer_size, 1);
      end = std::chrono::high_resolution_clock::now();
    }
    else {
      std::cout << "Wrong sampling type" << std::endl;
      return -1;
    }
    
    std::cout << "[" << __func__ << "] ********** OVERALL FUNCTION EXECUTION TAKEN FOR HOP # 1: "
              << (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << "us **********" << std::endl;
    std::cout << "[" << __func__ << "] **************************************************" << std::endl;
  }
  else if (hop_number == 2) {
    std::vector<int64_t> idx_2hop;
    num_neighbors = 25;
#ifdef CONFIGURE_BY_NUM_BULK // configure by num_bulk
    // num_bulk was already set correctly at beginning of main function
    total_required_buffer_size = sizeof(int64_t) * num_neighbors * num_bulk;
    output_buffer_size = (int64_t)((total_required_buffer_size + (BUFFER_GRANULARITY - 1)) / BUFFER_GRANULARITY) * BUFFER_GRANULARITY;
#else // configure by output_buffer_size
    output_buffer_size = (output_buffer_size_str == "4K") ? (1 << 12) :
                         (output_buffer_size_str == "8K") ? (1 << 13) :
                         (output_buffer_size_str == "16K") ? (1 << 14) :
                         (output_buffer_size_str == "32K") ? (1 << 15) : -1;
    if (output_buffer_size < 0) {
      std::cout << "Wrong output buffer size" << std::endl;
      return -1;
    }
    num_bulk = (int64_t) (output_buffer_size / (num_neighbors * sizeof(int64_t)));
#endif // CONFIGURE_BY_NUM_BULK
    // HOP # 1
    if (sampling_type == "pio") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_pio_cpu(rowptr, rowcount, idx, num_neighbors, false, path, 1);
      idx_2hop = std::get<2>(output);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "mmap") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_mmap_cpu(rowptr, rowcount, idx, num_neighbors, false, path, 1);
      idx_2hop = std::get<2>(output);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "p2p") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_p2p_cpu(rowptr, rowcount, idx, num_neighbors, false, path, maxrc, output_buffer_size, 1);
      idx_2hop = std::get<2>(output);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "bulk") {
      start = std::chrono::high_resolution_clock::now();
      auto output = sample_adj_p2p_bulk_cpu(rowptr, rowcount, idx, num_neighbors, num_bulk, false, path, maxrc, output_buffer_size, 1);
      idx_2hop = std::get<2>(output);
      end = std::chrono::high_resolution_clock::now();
    }
    else {
      std::cout << "Wrong sampling type" << std::endl;
      return -1;
    }

    std::cout << "[" << __func__ << "] ********** OVERALL FUNCTION EXECUTION TAKEN FOR HOP # 1: "
              << (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << "us **********" << std::endl;
    std::cout << "[" << __func__ << "] **************************************************" << std::endl;

    // HOP # 2 function parameter preparation
    num_neighbors = 10;
#ifdef CONFIGURE_BY_NUM_BULK // configure by num_bulk
    // num_bulk was already set correctly at beginning of main function
    total_required_buffer_size = sizeof(int64_t) * num_neighbors * num_bulk;
    output_buffer_size = (int64_t)((total_required_buffer_size + (BUFFER_GRANULARITY - 1)) / BUFFER_GRANULARITY) * BUFFER_GRANULARITY;
#else // configure by output_buffer_size
    num_bulk = (int64_t) (output_buffer_size / (num_neighbors * sizeof(int64_t)));
#endif // CONFIGURE_BY_NUM_BULK
    // HOP # 2
    if (sampling_type == "pio") {
      start = std::chrono::high_resolution_clock::now();
      auto output_2hop = sample_adj_pio_cpu(rowptr, rowcount, idx_2hop, num_neighbors, false, path, 2);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "mmap") {
      start = std::chrono::high_resolution_clock::now();
      auto output_2hop = sample_adj_mmap_cpu(rowptr, rowcount, idx_2hop, num_neighbors, false, path, 2);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "p2p") {
      start = std::chrono::high_resolution_clock::now();
      auto output_2hop = sample_adj_p2p_cpu(rowptr, rowcount, idx_2hop, num_neighbors, false, path, maxrc, output_buffer_size, 2);
      end = std::chrono::high_resolution_clock::now();
    }
    else if (sampling_type == "bulk") {
      start = std::chrono::high_resolution_clock::now();
      auto output_2hop = sample_adj_p2p_bulk_cpu(rowptr, rowcount, idx_2hop, num_neighbors, num_bulk, false, path, maxrc, output_buffer_size, 2);
      end = std::chrono::high_resolution_clock::now();
    }
    else {
      std::cout << "Wrong sampling type" << std::endl;
      return -1;
    }
    
    std::cout << "[" << __func__ << "] ********** OVERALL FUNCTION EXECUTION TAKEN FOR HOP # 2: "
              << (double)std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << "us **********" << std::endl;
    std::cout << "[" << __func__ << "] **************************************************" << std::endl;
  }
  else {
    std::cout << "Wrong number of hops" << std::endl;
  }
  return 0;
}
