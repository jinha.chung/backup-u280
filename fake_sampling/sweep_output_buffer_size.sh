#!/bin/bash

num_hop=2

for sampling_type in "bulk" "pio" "p2p"
#for sampling_type in "bulk" "pio"
#for sampling_type in "bulk"
#for sampling_type in "pio"
#for sampling_type in "p2p"
do
    mkdir -p out/$sampling_type
    for neighbor_size_bits in 5
    do
        for output_buffer_size in "4K" "8K" "16K" "32K"
        #for output_buffer_size in "4K"
        do
            for i in {1..100}
            do
                # e.g., ./test --neighbor-size-bits 5 --output-buffer-size 4K --sampling-type bulk --hop-number 1
                #./test >> out/pio/buffer_32K.log
                #./test --neighbor-size-bits 5 --dataset reddit32x --output-buffer-size $output_buffer_size --sampling-type $sampling_type --hop-number $num_hop >> out/$sampling_type/buffer_$output_buffer_size.log
                ./test --neighbor-size-bits 5 --output-buffer-size $output_buffer_size --dataset reddit32x --sampling-type $sampling_type --hop-number 2 >> out/$sampling_type/buffer_$output_buffer_size.log
                #echo "out/"$sampling_type"/buffer_"$output_buffer_size".log"
                if (($i % 10 == 0));
                then
                    echo "Finished test number "$i" for out/"$sampling_type"/buffer_"$output_buffer_size".log"
                fi
            done
        done
    done
done
